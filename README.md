# VizQGP

VizQGP is a visualization software for high-energy physics processes.
It was born out of the desire to have fun, intuitive and didactic visualization for particle and
nuclear physics, similar to those seen in astrophysics (and in particular general relativity).

## Goals

Some goals for what VizQGP shoule accomplish

- Render simulations of expanding quark-gluon plasmas
- Interface with GEANT4 to bring digital detectors to life
- Fully interactive simulations with a controllable camera
