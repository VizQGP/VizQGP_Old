//
// Author: Kevin Ingles
// File: Camera.cpp
// Description: Implementation of Camera.hpp

#include "Camera.hpp"
#include <Print.hpp>

namespace vizqgp
{
    void Camera::ChangeSensitivity(float sens)
    {
        sensitivity = sens;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void Camera::ProcessInput(CameraDirection dir, float speed)
    {
        if (dir == CameraDirection::FORWARD)
            position += front * speed;
        else if (dir == CameraDirection::BACKWARD)
            position += -front * speed;
        else if (dir == CameraDirection::LEFT)
            position += -right * speed;
        else
            position += right * speed;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....
    
    void Camera::UpdateRotation(float x, float y, float window_width, float window_height)
    {
        glm::vec2 origin    = { 0.5 * window_width, 0.5 * window_height };
        glm::vec2 pos       = glm::vec2{ x, y } - origin;

        if (last_x != x || last_y != y)
        {
            yaw     += sensitivity * pos.x;
            pitch   += sensitivity * pos.y;

            if (pitch > 89.5) pitch = 89;
            if (pitch < -89.5) pitch = -89;

            last_x = x;
            last_y = y;
        }
        if (x <= 0) yaw += -sensitivity * origin.x;
        if (x >= window_width - 1) yaw += sensitivity * origin.x;
        if (y <= 0)
        {
            pitch += -sensitivity * origin.y;
            if (pitch < -89.5) pitch = -89;
        }
        if (y >= window_height - 1)
        {
            pitch += sensitivity * origin.y;
            if (pitch > 89.5) pitch = 89;
        }
        UpdateVectors();
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    glm::mat4 Camera::GetCamera(glm::mat4& model)
    {
        return projection_matrix * view_matrix * model;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void Camera::UpdateVectors()
    {
        glm::vec3 temp{};
        temp.x  = std::cos(glm::radians(yaw)) * std::cos(glm::radians(pitch));
        temp.y  = -std::sin(glm::radians(pitch));
        temp.z  = std::sin(glm::radians(yaw)) * std::cos(glm::radians(pitch));
        front   = glm::normalize(temp);

        right   = glm::normalize(glm::cross(front, world_up));
        up      = glm::normalize(glm::cross(right, front));

        view_matrix = glm::lookAt(position, position + front, up);
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....
}