//
// Author: Kevin Ingles
// File: Structs.hpp
// Description: Implementation for functions in Structs.hpp

#include "VkStructs.hpp"
#include <Log.hpp>
#include <Print.hpp>
#include <string>
#include <TinyObjectLoader.h>


// TODO: Switch struct initialization to braces with '.'-accessors

namespace vizqgp
{
    bool Mesh::LoadFromHDF5(std::filesystem::path file_name)
    {
        tinyobj::attrib_t attribute;                // Contains vertex array from file
        std::vector<tinyobj::shape_t> shapes;       // info on separate object in file
        std::vector<tinyobj::material_t> materials;

        // error handling vars
        std::string warning;
        std::string error;

        std::filesystem::path material_dir = file_name;
        material_dir.remove_filename();

        // load file
        tinyobj::LoadObj(&attribute, &shapes, &materials, &warning, &error, file_name.string().data(), material_dir.string().data());
        if (!warning.empty()) LogWarning(fmt::format("Warning loading mesh: {}", warning).data());
        if (!error.empty())
        {
            LogError(fmt::format("Error loading mesh: {}", error).data());
            return false;
        }

        // loop over shapes
        for (size_t ishape = 0; ishape < shapes.size(); ishape++)
        {
            size_t index_offset = 0;
            // loop over face of polygon
            for (size_t iface = 0; iface < shapes[ishape].mesh.num_face_vertices.size(); iface++)
            {
                int vertices_in_triangle = 3;
                // loop over vertices for face
                for (size_t ivertex = 0; ivertex < vertices_in_triangle; ivertex++)
                {
                    // access vertex
                    tinyobj::index_t idx = shapes[ishape].mesh.indices[index_offset + ivertex];

                    // vertex position
                    tinyobj::real_t vx = attribute.vertices[3 * idx.vertex_index + 0];
                    tinyobj::real_t vy = attribute.vertices[3 * idx.vertex_index + 1];
                    tinyobj::real_t vz = attribute.vertices[3 * idx.vertex_index + 2];

                    // vertex normal
                    tinyobj::real_t nx = attribute.normals[3 * idx.normal_index + 0];
                    tinyobj::real_t ny = attribute.normals[3 * idx.normal_index + 1];
                    tinyobj::real_t nz = attribute.normals[3 * idx.normal_index + 2];


                    // Create vertex with above information
                    Vertex new_vertex;
                    new_vertex.position.x = vx;     new_vertex.normal.x = nx;
                    new_vertex.position.y = vy;     new_vertex.normal.y = ny;
                    new_vertex.position.z = vz;     new_vertex.normal.z = nz;

                    new_vertex.color = new_vertex.normal;
                    vertices.push_back(new_vertex);
                } // end for : ivertex
                index_offset += vertices_in_triangle;
            } // end for : iface
        } // end for : ishape
        return true;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    VkPipeline PipelineBuilder::BuildPipeline(VkDevice device, VkRenderPass pass)
    {
        VkPipelineViewportStateCreateInfo viewport_info{};
        viewport_info.sType         = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
        viewport_info.pNext         = nullptr;
        viewport_info.viewportCount = 1;
        viewport_info.pViewports    = &viewport;
        viewport_info.scissorCount  = 1;
        viewport_info.pScissors     = &scissor;

        VkPipelineColorBlendStateCreateInfo color_blend_info{};
        color_blend_info.sType              = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
        color_blend_info.pNext              = nullptr;
        color_blend_info.logicOpEnable      = VK_FALSE;
        color_blend_info.logicOp            = VK_LOGIC_OP_COPY;
        color_blend_info.attachmentCount    = 1;
        color_blend_info.pAttachments       = &color_blend_attachement;

        // Build actual graphics pipeline
        VkGraphicsPipelineCreateInfo pipeline_info{};
        pipeline_info.sType                 = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
        pipeline_info.pNext                 = nullptr;
        pipeline_info.stageCount            = shader_stages_infos.size();
        pipeline_info.pStages               = shader_stages_infos.data();
        pipeline_info.pVertexInputState     = &vertex_input_info;
        pipeline_info.pInputAssemblyState   = &input_assembly_info;
        pipeline_info.pViewportState        = &viewport_info;
        pipeline_info.pRasterizationState   = &rasterizer_info;
        pipeline_info.pMultisampleState     = &multisampling_info;
        pipeline_info.pColorBlendState      = &color_blend_info;
        pipeline_info.layout                = pipeline_layout;
        pipeline_info.renderPass            = pass;
        pipeline_info.subpass               = 0;
        pipeline_info.basePipelineHandle    = VK_NULL_HANDLE;
        pipeline_info.pDepthStencilState    = &depth_stencil;

        VkPipeline new_pipeline;
        if (vkCreateGraphicsPipelines(device, VK_NULL_HANDLE, 1, &pipeline_info, nullptr, &new_pipeline) != VK_SUCCESS)
        {
            LogWarning("Failed to create pipeline\n");
            return VK_NULL_HANDLE;
        }
        else return new_pipeline;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    VertexInputDescription Vertex::GetVertexDescription()
    {
        VertexInputDescription description;

        VkVertexInputBindingDescription main_binding{};
        main_binding.binding    = 0;
        main_binding.stride     = sizeof(Vertex);
        main_binding.inputRate  = VK_VERTEX_INPUT_RATE_VERTEX;

        description.bindings.push_back(main_binding);

        // positions will be stored at location 0
        VkVertexInputAttributeDescription position_attribute{};
        position_attribute.binding  = 0;
        position_attribute.location = 0;
        position_attribute.format   = VK_FORMAT_R32G32B32_SFLOAT;
        position_attribute.offset   = offsetof(Vertex, position);

        // normal will be stored at location 1
        VkVertexInputAttributeDescription normal_attribute{};
        normal_attribute.binding    = 0;
        normal_attribute.location   = 1;
        normal_attribute.format     = VK_FORMAT_R32G32B32_SFLOAT;
        normal_attribute.offset     = offsetof(Vertex, normal);

        // color will be stored at location 2
        VkVertexInputAttributeDescription color_attribute{};
        color_attribute.binding     = 0;
        color_attribute.location    = 2;
        color_attribute.format      = VK_FORMAT_R32G32B32_SFLOAT;
        color_attribute.offset      = offsetof(Vertex, color);

        LogLog("Pushing VertexAttributes to Vectors:");
        description.attributes.push_back(position_attribute);    LogSuccess("Successfully pushed `position_attribute`"); // , description.attributes[0].format);
        description.attributes.push_back(normal_attribute);      LogSuccess("Successfully pushed `normal_attribute`");   // , description.attributes[1].format);
        description.attributes.push_back(color_attribute);       LogSuccess("Successfully pushed `color_attribute`");    // , description.attributes[2].format);
        return description;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    VkCommandPoolCreateInfo	CommandPoolCreateInfo(uint32_t queue_family_index, VkCommandPoolCreateFlags flags /* = 0 */)
    {
        // Create command pools for submitting to graphics queue
        VkCommandPoolCreateInfo command_pool_info{};	// Very important to initialize with empty initializer_list
        command_pool_info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        command_pool_info.pNext = nullptr;

        // command pool submits to graphics queue
        command_pool_info.queueFamilyIndex  = queue_family_index;
        command_pool_info.flags             = flags;
        return command_pool_info;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    VkCommandBufferAllocateInfo	CommandBufferAllocateInfo(VkCommandPool pool, uint32_t count /* = 1 */, VkCommandBufferLevel level /* = VK_COMMAND_BUFFER_LEVEL_PRIMARY*/)
    {
        // Create command buffers used for rendering
        VkCommandBufferAllocateInfo command_buffer_info{};	// Very important to initialize with empty initializer_list
        command_buffer_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        command_buffer_info.pNext = nullptr;

        command_buffer_info.commandPool         = pool;						// command buffer will be made from our command pool
        command_buffer_info.commandBufferCount  = count;					// will allocate one command buffer (could be more)
        command_buffer_info.level               = level;
        return command_buffer_info;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    VkCommandBufferBeginInfo CommandBufferBeginInfo(VkCommandBufferResetFlags flags, VkCommandBufferInheritanceInfo* inheritance_info /* = nullptr */)
    {
        VkCommandBufferBeginInfo info{};
        info.sType              = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        info.pNext              = nullptr;
        info.flags              = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
        info.pInheritanceInfo   = nullptr;
        return info;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    VkRenderPassCreateInfo RenderPassCreateInfo(VkAttachmentDescription* color_attachment, uint32_t attachment_count, VkSubpassDescription* subpasses, uint32_t subpass_count)
    {
        VkRenderPassCreateInfo render_pass_info{};
        render_pass_info.sType              = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
        render_pass_info.attachmentCount    = attachment_count;
        render_pass_info.pAttachments       = color_attachment;
        render_pass_info.subpassCount       = subpass_count;
        render_pass_info.pSubpasses         = subpasses;
        return render_pass_info;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    VkRenderPassBeginInfo RenderPassBeginInfo(VkRenderPass render_pass, VkExtent2D extent, VkFramebuffer framebuffer, VkClearValue* clear_values, int count /* = 0 */)
    {
        VkRenderPassBeginInfo info{};
        info.sType                  = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        info.pNext                  = nullptr;
        info.renderPass             = render_pass;
        info.renderArea.offset.x    = 0;
        info.renderArea.offset.y    = 0;
        info.renderArea.extent      = extent;
        info.framebuffer            = framebuffer;
        info.clearValueCount        = count;
        info.pClearValues           = clear_values;
        return info;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    VkSubmitInfo SubmitInfo(VkCommandBuffer* command_buffer)
    {
        VkPipelineStageFlags wait_stage = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        VkSubmitInfo info{};
        info.sType                  = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        info.pNext                  = nullptr;
        info.pWaitDstStageMask      = 0;
        info.waitSemaphoreCount     = 0;
        info.pWaitSemaphores        = nullptr;
        info.signalSemaphoreCount   = 0;
        info.pSignalSemaphores      = nullptr;
        info.commandBufferCount     = 1;
        info.pCommandBuffers        = command_buffer;
        return info;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    VkPipelineShaderStageCreateInfo	PipelineShaderStageCreateInfo(VkShaderStageFlagBits stage, VkShaderModule shader_module)
    {
        VkPipelineShaderStageCreateInfo info{};
        info.sType  = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        info.pNext  = nullptr;
        info.stage  = stage;
        info.module = shader_module;
        info.pName  = "main";
        return info;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    VkPipelineVertexInputStateCreateInfo VertexInputStateCreateInfo()
    {
        VkPipelineVertexInputStateCreateInfo info{};
        info.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
        info.pNext = nullptr;

        // no vertex bindings and attributes
        info.vertexBindingDescriptionCount      = 0;
        info.vertexAttributeDescriptionCount    = 0;
        return info;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    VkPipelineInputAssemblyStateCreateInfo	InputAssemblyCreateInfo(VkPrimitiveTopology topology)
    {
        VkPipelineInputAssemblyStateCreateInfo info{};
        info.sType                  = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
        info.pNext                  = 0;
        info.topology               = topology;
        info.primitiveRestartEnable = VK_FALSE;
        return info;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    VkPipelineRasterizationStateCreateInfo RasterizationStateCreateInfo(VkPolygonMode polygon_mode)
    {
        VkPipelineRasterizationStateCreateInfo info{};
        info.sType                      = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
        info.pNext                      = nullptr;
        info.depthClampEnable           = VK_FALSE;
        info.rasterizerDiscardEnable    = VK_FALSE;                 // Discards all primitives before rasterization when true
        info.polygonMode                = polygon_mode;
        info.lineWidth                  = 1.0f;
        info.cullMode                   = VK_CULL_MODE_NONE;        // No backface cull
        info.frontFace                  = VK_FRONT_FACE_CLOCKWISE;
        info.depthBiasEnable            = VK_FALSE;                 // No depth bias
        info.depthBiasConstantFactor    = 0.0f;
        info.depthBiasClamp             = 0.0f;
        info.depthBiasSlopeFactor       = 0.0f;
        return info;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    VkPipelineColorBlendAttachmentState	ColorBlendAttachmentState()
    {
        VkPipelineColorBlendAttachmentState color_blend_attachment{};
        color_blend_attachment.colorWriteMask   = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
        color_blend_attachment.blendEnable      = VK_FALSE;
        return color_blend_attachment;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    VkPipelineMultisampleStateCreateInfo MultisamplingStateCreateInfo()
    {
        VkPipelineMultisampleStateCreateInfo info{};
        info.sType                  = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
        info.pNext                  = 0;
        info.sampleShadingEnable    = VK_FALSE;
        info.rasterizationSamples   = VK_SAMPLE_COUNT_1_BIT;  // One sample per pixel
        info.minSampleShading       = 1.0f;
        info.pSampleMask            = nullptr;
        info.alphaToCoverageEnable  = VK_FALSE;
        info.alphaToOneEnable       = VK_FALSE;
        return info;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    VkPipelineLayoutCreateInfo PipelineLayoutCreateInfo()
    {
        VkPipelineLayoutCreateInfo info{};
        info.sType                  = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        info.pNext                  = nullptr;
        info.flags                  = 0;
        info.setLayoutCount         = 0;
        info.pSetLayouts            = nullptr;
        info.pushConstantRangeCount = 0;
        info.pPushConstantRanges    = nullptr;
        return info;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    VkImageCreateInfo ImageCreatInfo(VkFormat format, VkImageUsageFlags usage_flags, VkExtent3D extent)
    {
        VkImageCreateInfo info{};
        info.sType          = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
        info.pNext          = nullptr;
        info.imageType      = VK_IMAGE_TYPE_2D;
        info.format         = format;
        info.extent         = extent;
        info.mipLevels      = 1;
        info.arrayLayers    = 1;
        info.samples        = VK_SAMPLE_COUNT_1_BIT;
        info.tiling         = VK_IMAGE_TILING_OPTIMAL;
        info.usage          = usage_flags;
        return info;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    VkImageViewCreateInfo ImageViewCreateInfo(VkFormat format, VkImage image, VkImageAspectFlags aspect_flags)
    {
        VkImageViewCreateInfo info{};
        info.sType                              = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        info.pNext                              = nullptr;
        info.viewType                           = VK_IMAGE_VIEW_TYPE_2D;
        info.image                              = image;
        info.format                             = format;
        info.subresourceRange.baseMipLevel      = 0;
        info.subresourceRange.levelCount        = 1;
        info.subresourceRange.baseArrayLayer    = 0;
        info.subresourceRange.layerCount        = 1;
        info.subresourceRange.aspectMask        = aspect_flags;
        return info;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    VkPipelineDepthStencilStateCreateInfo DepthStencilCreateInfo(bool b_depth_test, bool b_depth_write, VkCompareOp compare_op)
    {
        VkPipelineDepthStencilStateCreateInfo info{};
        info.sType                  = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
        info.pNext                  = nullptr;
        info.depthTestEnable        = b_depth_test ? VK_TRUE : VK_FALSE;
        info.depthWriteEnable       = b_depth_write ? VK_TRUE : VK_FALSE;
        info.depthCompareOp         = b_depth_test ? compare_op : VK_COMPARE_OP_ALWAYS;
        info.depthBoundsTestEnable  = VK_FALSE;
        info.minDepthBounds         = 0.0f;
        info.maxDepthBounds         = 1.0f;
        info.stencilTestEnable      = VK_FALSE;
        return info;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    VkSemaphoreCreateInfo SemaphoreCreateInfo(VkSemaphoreCreateFlags flags /* = 0 */)
    {
        VkSemaphoreCreateInfo info{};
        info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
        info.pNext = nullptr;
        info.flags = flags;
        return info;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    VkFenceCreateInfo FenceCreateInfo(VkFenceCreateFlags flags /* = 0 */)
    {
        VkFenceCreateInfo info{};
        info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
        info.pNext = nullptr;
        info.flags = flags;
        return info;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    VkDescriptorSetLayoutBinding DescriptorSetLayoutBinding(VkDescriptorType type, VkShaderStageFlags stage_flags, uint32_t binding)
    {
        VkDescriptorSetLayoutBinding set_binding{};
        set_binding.binding             = binding;
        set_binding.descriptorCount     = 1;
        set_binding.descriptorType      = type;
        set_binding.pImmutableSamplers  = nullptr;
        set_binding.stageFlags          = stage_flags;
        return set_binding;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    VkWriteDescriptorSet WriteDescriptorSet(VkDescriptorType type, VkDescriptorSet descriptor_set, VkDescriptorBufferInfo* buffer_info, uint32_t binding)
    {
        VkWriteDescriptorSet set{};
        set.sType           = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        set.pNext           = nullptr;
        set.dstBinding      = binding;
        set.dstSet          = descriptor_set;
        set.descriptorCount = 1;
        set.descriptorType  = type;
        set.pBufferInfo     = buffer_info;
        return set;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    VkWriteDescriptorSet WriteDescriptorSet(VkDescriptorType type, VkDescriptorSet descriptor_set, VkDescriptorImageInfo* image_info, uint32_t binding)
    {
        VkWriteDescriptorSet set{};
        set.sType           = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        set.pNext           = nullptr;
        set.dstBinding      = binding;
        set.dstSet          = descriptor_set;
        set.descriptorCount = 1;
        set.descriptorType  = type;
        set.pImageInfo      = image_info;
        return set;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    VkDescriptorBufferInfo DescriptorBufferInfo(VkBuffer buffer, VkDeviceSize offset, VkDeviceSize range)
    {
        VkDescriptorBufferInfo info{};
        info.buffer = buffer;
        info.offset = offset;
        info.range  = range;
        return info;
    }

    VkFramebufferCreateInfo FrameBufferCreateInfo(VkRenderPass render_pass, VkExtent2D window_extent, uint32_t layers)
    {
        VkFramebufferCreateInfo info{};
        info.sType              = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        info.pNext              = nullptr;
        info.renderPass         = render_pass;
        info.attachmentCount    = 0;
        info.width              = window_extent.width;
        info.height             = window_extent.height;
        info.layers             = 1;
        return info;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....
}