//
// Author: Kevin Ingles
// File: Structs.h
// Description: This file contains many of the ad hoc structs used in the RenderEngine.hpp file.
//              Haveing them centralized, allows us to make one include file and avoid confusing searches

#ifndef VK_STRUCTS_H
#define VK_STRUCTS_H
// Our includes
#include "Camera.hpp"

#include <vector>
#include <filesystem>

// Other includes
#include <vulkan/vulkan.h>
#include <VkMemoryAlloc.h>

// STL includes

namespace vizqgp
{
    // Forward declarations just in case they need to see each other,
    // because I want to keep them alphabetically organized
    struct AllocatedBuffer;
    struct AllocatedImage;
    struct FrameData;
    struct GPUCameraData;
    struct GPUObjectData;
    struct Material;
    struct Mesh;
    struct PiplelineBuilder;
    struct RenderObject;
    struct UploadContext;
    struct Vertex;
    struct VertexInputDescription;
    
    ///////////////////////////////////////////////
    // Functions for initializing vulkan structs //
    ///////////////////////////////////////////////

    VkCommandPoolCreateInfo		CommandPoolCreateInfo(uint32_t queue_family_index, VkCommandPoolCreateFlags flags = 0);
    VkCommandBufferAllocateInfo	CommandBufferAllocateInfo(VkCommandPool pool, uint32_t count = 1, VkCommandBufferLevel level = VK_COMMAND_BUFFER_LEVEL_PRIMARY);
    VkCommandBufferBeginInfo    CommandBufferBeginInfo(VkCommandBufferResetFlags flags, VkCommandBufferInheritanceInfo* inheritance_info = nullptr);
    VkRenderPassCreateInfo		RenderPassCreateInfo(VkAttachmentDescription* color_attachment, uint32_t attachment_count, VkSubpassDescription* subpasses, uint32_t subpass_count);
    VkRenderPassBeginInfo       RenderPassBeginInfo(VkRenderPass render_pass, VkExtent2D extent, VkFramebuffer framebuffer, VkClearValue* clear_values, int count=0);
    VkSubmitInfo                SubmitInfo(VkCommandBuffer* command_buffer);

    // Pipeline struct initializations
    VkPipelineShaderStageCreateInfo			PipelineShaderStageCreateInfo(VkShaderStageFlagBits stage, VkShaderModule shader_module);
    VkPipelineVertexInputStateCreateInfo	VertexInputStateCreateInfo();
    VkPipelineInputAssemblyStateCreateInfo	InputAssemblyCreateInfo(VkPrimitiveTopology topology);
    VkPipelineRasterizationStateCreateInfo	RasterizationStateCreateInfo(VkPolygonMode polygon_mode);
    VkPipelineColorBlendAttachmentState		ColorBlendAttachmentState();
    VkPipelineMultisampleStateCreateInfo	MultisamplingStateCreateInfo();

    // Pipeline layouts need to be created separately
    VkPipelineLayoutCreateInfo PipelineLayoutCreateInfo();

    // Depth buffer initializations
    VkImageCreateInfo                       ImageCreatInfo(VkFormat format, VkImageUsageFlags usage_flags, VkExtent3D extent);
    VkImageViewCreateInfo                   ImageViewCreateInfo(VkFormat format, VkImage image, VkImageAspectFlags aspect_flags);
    VkPipelineDepthStencilStateCreateInfo   DepthStencilCreateInfo(bool b_depth_test, bool b_depth_write, VkCompareOp compare_op);

    // Initialize synchronoization structures
    VkSemaphoreCreateInfo   SemaphoreCreateInfo(VkSemaphoreCreateFlags flags = 0);
    VkFenceCreateInfo       FenceCreateInfo(VkFenceCreateFlags flags = 0);

    // Descriptor set initializations
    VkDescriptorSetLayoutBinding    DescriptorSetLayoutBinding(VkDescriptorType type, VkShaderStageFlags stage_flags, uint32_t binding);
    VkWriteDescriptorSet            WriteDescriptorSet(VkDescriptorType type, VkDescriptorSet descriptor_set, VkDescriptorBufferInfo* buffer_info, uint32_t binding);
    VkWriteDescriptorSet            WriteDescriptorSet(VkDescriptorType type, VkDescriptorSet descriptor_set, VkDescriptorImageInfo* image_info, uint32_t binding);
    VkDescriptorBufferInfo          DescriptorBufferInfo(VkBuffer buffer, VkDeviceSize offset, VkDeviceSize range);

    // Frame buffers
    VkFramebufferCreateInfo FrameBufferCreateInfo(VkRenderPass render_pass, VkExtent2D window_extent, uint32_t layers);



    //////////////////////
    /// Struct details ///
    //////////////////////
    struct AllocatedBuffer
    {
        VkBuffer        buffer;
        VmaAllocation   allocation;
    };

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    struct AllocatedImage
    {
        VkImage         image;
        VmaAllocation   allocation;
    };

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    struct FrameData
    {
        VkSemaphore present_semaphore;
        VkSemaphore render_semaphore;
        VkFence     render_fence;

        VkCommandPool   command_pool;
        VkCommandBuffer command_buffer;

        AllocatedBuffer camera_buffer;
        AllocatedBuffer object_buffer;

        VkDescriptorSet global_descriptor_set;
        VkDescriptorSet object_descriptor_set;
    };

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    struct GPUCameraData
    {
        glm::mat4 view_matrix;
        glm::mat4 projection_matrix;
        glm::mat4 view_projection;      // Product of view and projection matrices
    };

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    struct GPUObjectData
    {
        glm::mat4 model_matrix;
    };

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    struct Material
    {
        VkPipeline          pipeline;
        VkPipelineLayout    pipeline_layout;

        VkDescriptorSet texture_descriptor_set{ VK_NULL_HANDLE }; // We don't support textures, but may in the future
    };

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    struct Mesh
    {
        std::vector<Vertex> vertices;
        AllocatedBuffer     vertex_buffer;

        bool LoadFromHDF5(std::filesystem::path file_name);
    };

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    struct PipelineBuilder
    {
        std::vector<VkPipelineShaderStageCreateInfo> shader_stages_infos;
        VkPipelineVertexInputStateCreateInfo	vertex_input_info;
        VkPipelineInputAssemblyStateCreateInfo	input_assembly_info;
        VkViewport								viewport;
        VkRect2D								scissor;
        VkPipelineRasterizationStateCreateInfo	rasterizer_info;
        VkPipelineColorBlendAttachmentState		color_blend_attachement;
        VkPipelineMultisampleStateCreateInfo	multisampling_info;
        VkPipelineLayout						pipeline_layout;
        VkPipelineDepthStencilStateCreateInfo   depth_stencil;

        VkPipeline BuildPipeline(VkDevice device, VkRenderPass pass);
    };

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    struct RenderObject
    {
        Mesh*       mesh;
        Material*   material;
        glm::mat4   transform_matrix;
    };

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    struct UploadContext
    {
        VkFence         upload_fence;
        VkCommandPool   command_pool;
    };

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    struct VertexInputDescription
    {
        std::vector<VkVertexInputBindingDescription>     bindings;
        std::vector<VkVertexInputAttributeDescription>   attributes;

        VkPipelineVertexInputStateCreateFlags flags{ 0 };

        VertexInputDescription() = default;
    };

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....


    struct Vertex
    {
        glm::vec3 position;
        glm::vec3 normal;
        glm::vec3 color;

        static VertexInputDescription GetVertexDescription();
    };

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....
}

#endif