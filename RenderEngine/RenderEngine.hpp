
// Author: Kevin Ingles
// File: RenderEngine.hpp
// Description: Main work horse in rendering the visualizations. Interfaces with Vulakn library and
//              SDL libraries

// Thoughts: Does it make sense to couple the SDL and Vulkan like this?
//           Want to add state machines in some capacity

#ifndef RENDER_ENGINE_HPP
#define RENDER_ENGINE_HPP

#include <Print.hpp>
#include <DeletionQueue.hpp>
#include <ImGuiLayer.hpp>
#include <SDLWindow.hpp>

#include "VkStructs.hpp"

#include <functional>
#include <filesystem>
#include <unordered_map>
#include <Scene.hpp>
#include <string>

namespace vizqgp
{
    constexpr unsigned int FRAME_OVERLAP = 2;

    class RenderEngine
    {
    public:
        // Simple RAII Interface
        RenderEngine();
        ~RenderEngine();


        // Core functionality
        void Cleanup();	// shuts down the engine
        void Draw();	// draw loop
        void Init();	// initializes everything in the engine
        void Run();		// run main loop

        AllocatedBuffer CreateBuffer(size_t allocation_size, VkBufferUsageFlags usage, VmaMemoryUsage memory_usage);
        size_t          PadUniformBufferSize(size_t original_size);

        FrameData&      GetCurrentFrame();
        ui::ImGuiLayer& GetGui();
        void            ImmediateSubmit(std::function<void(VkCommandBuffer)>&& func);

        //void CreateMaterial(VkPipeline pipeline, VkPipelineLayout pipeline_layout, const char* material_name);
        bool LoadShaderModule(std::filesystem::path file_path, VkShaderModule* shader);


    private:
        // Initialization of various engine components
        void InitCamera();
        void InitCommands();
        void InitDefaultRenderPass();
        void InitDescriptors();
        void InitFrameBuffers();
        void InitPipelines();
        void InitScene();
        void InitSwapchain();
        void InitSyncStructures();
        void InitVulkan();

        void OnWindowResize(uint32_t width, uint32_t height);
        void ReInitCommandBuffers();                            // If resize window event occurs, free current command buffers and 
        void UploadSceneMeshes();


        // Deletion queues (this needs a better work around?)
        DeletionQueue m_main_deletion_queue;    // deletion queue to help keep track of what order to delete things
        DeletionQueue m_resize_window_queue;    // Queue of objects that need to be deleted when recreating swap chains

        // For creating a window
        VkExtent2D  m_window_extent;
        Window*     m_window;

        // Bool for initializing engine
        bool m_is_initialized{ false };

        VmaAllocator m_vma_allocator;

        // For initializing Vulkan API
        VkInstance                  m_vulkan_instance;
        VkDebugUtilsMessengerEXT    m_vulkan_debug_messenger;
        VkPhysicalDevice            m_vulkan_physical_device;
        VkPhysicalDeviceProperties  m_vulkan_physical_device_properties;
        VkDevice                    m_vulkan_device;
        VkSurfaceKHR                m_vulkan_surface{};

        FrameData   m_frame_data[FRAME_OVERLAP];
        int         m_frame_number{ 0 };

        Scene m_scene;

        VkSwapchainKHR		        m_swapchain;				
        VkFormat			        m_swapchain_imagine_format;
        std::vector<VkImage>		m_swapchain_images;		
        std::vector<VkImageView>    m_swapchain_image_views;

        // Depth buffering 
        VkImageView     m_depth_image_view;
        AllocatedImage  m_depth_image;
        VkFormat        m_depth_format;

        // GPU Queues
        VkQueue		m_graphics_queue;		    // Queue to submit rendering calls to
        uint32_t	m_graphics_queue_family;	// Keeps track which family of queues we are submitting to

        // VkRenderPass 
        VkRenderPass			m_render_pass;	    // For organizing function calls and image data passed to graphics queue
        std::vector<VkFramebuffer>	m_frame_buffers;	// Contains organized images

        VkDescriptorPool        m_descriptor_pool;
        VkDescriptorSetLayout   m_global_descriptor_set_layout;
        VkDescriptorSetLayout   m_object_descriptor_set_layout;

        // For allocating buffers in the ImmediateSubmit function call
        UploadContext m_upload_context;

        Camera m_camera;

        // Interface for ImGuiLayout
        ui::ImGuiLayer* m_gui{};

        // Window size information
        float       m_aspect_ratio;
    };
}

#endif