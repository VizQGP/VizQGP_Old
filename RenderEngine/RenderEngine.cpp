
// Author: Kevin Ingles
// File: RenderEngine.cpp
// Description: Main work horse in rendering the visualizations. Interfaces with Vulakn library and
//              SDLWindow libraries

// Thoughts: Do we want to eventually implement our own memory allocation for Vulkan
//           Will need to work to multi-thread this eventually

#include "RenderEngine.hpp"
#include <Log.hpp>
#include <ProjectPaths.h>

// Vulkan Bootstrap library to take care of initialization
#include <VkBootstrap.h>

// Vulkan Memory Allocation takes care for the behind the scene memory application
#define VMA_IMPLEMENTATION
#include <VkMemoryAlloc.h>

// GLM Librbaries
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// IMGUI libraries
#include <ImGuiLayer.hpp>

// STL Libraries
#include <fstream>
#include <filesystem>

//#define NDEBUG
#include <cassert>

// For error tracking
static void VkCheck(VkResult err)
{
    if (err)
        LogError(fmt::format("Render engine detected Vulkan Error: {}", err));
}

namespace vizqgp
{
    ///////////////////////////////////////
    ///         Public Interface        ///
    ///////////////////////////////////////
    RenderEngine::RenderEngine()
    {
        Init();
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    RenderEngine::~RenderEngine()
    {
        Cleanup();
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void RenderEngine::Cleanup()
    {
        if (m_is_initialized)
        {
            vkDeviceWaitIdle(m_vulkan_device);
            delete m_gui;
            m_scene.DestroyMeshAllocations(m_vma_allocator); // FIXME: Scene should take care of its own garabage collection
            m_resize_window_queue.Flush();
            m_main_deletion_queue.Flush();

            vkDestroySurfaceKHR(m_vulkan_instance, m_vulkan_surface, nullptr);
            vkDestroyDevice(m_vulkan_device, nullptr);
            vkb::destroy_debug_utils_messenger(m_vulkan_instance, m_vulkan_debug_messenger);
            vkDestroyInstance(m_vulkan_instance, nullptr);
            m_window->~Window();
        }
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void RenderEngine::Draw()
    {
        constexpr uint64_t one_second = 1'000'000'000;

        uint32_t swapchain_image_index;

        VkCheck(vkWaitForFences(m_vulkan_device, 1, &GetCurrentFrame().render_fence, true, one_second));
        VkCheck(vkResetFences(m_vulkan_device, 1, &GetCurrentFrame().render_fence));
        VkCheck(vkAcquireNextImageKHR(m_vulkan_device, m_swapchain, one_second, GetCurrentFrame().present_semaphore, nullptr, &swapchain_image_index));
        VkCheck(vkResetCommandBuffer(GetCurrentFrame().command_buffer, 0));

        VkCommandBufferBeginInfo command_buffer_begin_info = CommandBufferBeginInfo(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);
        VkCheck(vkBeginCommandBuffer(GetCurrentFrame().command_buffer, &command_buffer_begin_info));
        

        // Render pass that flashes a clear-color over time
        VkClearValue clear_value;
        //float flash =  // std::abs(std::sin(m_frame_number / 60.0f));
        // clear_value.color = { { 0.0f, 0.0f, flash, 1.0f } };
        clear_value.color = { {0.150f, 0.0, 0.0, 1.0f} };

        VkClearValue depth_clear_value;
        depth_clear_value.depthStencil.depth = 1.0f;

        // Combine clear colors
        VkClearValue clear_values[2] = { clear_value, depth_clear_value };

        // Start main render pass
        VkRenderPassBeginInfo render_pass_begin_info = RenderPassBeginInfo(m_render_pass, m_window_extent, m_frame_buffers[swapchain_image_index], clear_values, 2);
        vkCmdBeginRenderPass(GetCurrentFrame().command_buffer, &render_pass_begin_info, VK_SUBPASS_CONTENTS_INLINE);

        m_scene.DrawObjects(GetCurrentFrame().command_buffer, m_vma_allocator, m_camera, GetCurrentFrame());

        // Render additional view ports
        GetGui().BeginFrame();
        GetGui().DrawLoggingConsole();
        GetGui().RenderFrame(GetCurrentFrame().command_buffer);
        GetGui().EndFrame();

        vkCmdEndRenderPass(GetCurrentFrame().command_buffer);           // End RenderPass
        VkCheck(vkEndCommandBuffer(GetCurrentFrame().command_buffer));  // End CommandBuffer

        VkSubmitInfo submit_info{};
        submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submit_info.pNext = nullptr;

        VkPipelineStageFlags wait_stage = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

        submit_info.pWaitDstStageMask   = &wait_stage;
        submit_info.waitSemaphoreCount  = 1;
        submit_info.pWaitSemaphores     = &GetCurrentFrame().present_semaphore;

        submit_info.signalSemaphoreCount    = 1;
        submit_info.pSignalSemaphores       = &GetCurrentFrame().render_semaphore;

        submit_info.commandBufferCount  = 1;
        submit_info.pCommandBuffers     = &GetCurrentFrame().command_buffer;

        VkCheck(vkQueueSubmit(m_graphics_queue, 1, &submit_info, GetCurrentFrame().render_fence));

        VkPresentInfoKHR present_info{};
        present_info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
        present_info.pNext = nullptr;

        present_info.swapchainCount     = 1;
        present_info.pSwapchains        = &m_swapchain;
        present_info.waitSemaphoreCount = 1;
        present_info.pWaitSemaphores    = &GetCurrentFrame().render_semaphore;
        present_info.pImageIndices      = &swapchain_image_index;

        auto result = vkQueuePresentKHR(m_graphics_queue, &present_info);
        VkCheck(result);

        if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR) 
            OnWindowResize(m_window->width, m_window->height);

        m_frame_number++;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void RenderEngine::Init()
    {
        LogLog("Initializing VizQGPApp::RenderEngine");
        SDL_WindowFlags window_flags = static_cast<SDL_WindowFlags>(SDL_WINDOW_VULKAN); // | SDL_WINDOW_RESIZABLE);

        m_window = new Window("VizQGP", 1600, 900, window_flags);
        m_aspect_ratio = static_cast<float>(m_window->width) / static_cast<float>(m_window->height);

        m_window_extent.width = m_window->width;
        m_window_extent.height = m_window->height;

        InitVulkan();
        InitSwapchain();
        InitCommands();
        InitDefaultRenderPass();
        InitFrameBuffers();
        InitCamera();
        InitSyncStructures();
        InitDescriptors();
        InitScene();
        InitPipelines();


        ui::VulkanInitInfo vulkan_init_info{};
        vulkan_init_info.instance           = m_vulkan_instance;
        vulkan_init_info.physical_device    = m_vulkan_physical_device;
        vulkan_init_info.device             = m_vulkan_device;
        vulkan_init_info.queue              = m_graphics_queue;
        vulkan_init_info.queue_family       = m_graphics_queue_family;
        vulkan_init_info.min_image_count    = 3;
        vulkan_init_info.image_count        = 3;
        vulkan_init_info.MSAA_samples       = VK_SAMPLE_COUNT_1_BIT;
        vulkan_init_info.VkCheckResults     = VkCheck;

        m_gui = new ui::ImGuiLayer(vulkan_init_info, *m_window, m_render_pass);

        ImmediateSubmit([&](VkCommandBuffer command_buffer)
            {
                GetGui().UploadFontTextures(command_buffer);
            });
        GetGui().DestroyFontTexturesObjects();

        m_is_initialized = true;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void RenderEngine::Run()
    {
        SDL_Event sdl_event;
        bool b_quit = false;

        while (!b_quit)
        {
            while (SDL_PollEvent(&sdl_event))
            {
                GetGui().ProcessEvents(&sdl_event);
                m_window->ProcessEvent(&sdl_event);
                switch(sdl_event.type)
                {
                case SDL_QUIT:
                    {
                        b_quit = true;
                        break;
                    }
                case SDL_WINDOWEVENT:
                    {
                        switch (sdl_event.window.event)
                        {
                        case SDL_WINDOWEVENT_RESIZED:
                            OnWindowResize(sdl_event.window.data1, sdl_event.window.data2); 
                            break;
                        //case SDL_WINDOWEVENT_SIZE_CHANGED:
                        //    OnWindowResize(sdl_event.window.data1, sdl_event.window.data2); break;
                        default:
                            break;
                        } // End switch (sdl_event.window.event)
                    }// End case SDL_WINDOWEVENT:
                case SDL_KEYDOWN:
                    {
                        float speed = 0.1f;
                        // Convert to switch statement
                        if (sdl_event.key.keysym.sym == SDLK_a)
                        {
                            m_camera.ProcessInput(CameraDirection::LEFT, speed);
                        }
                        if (sdl_event.key.keysym.sym == SDLK_d)
                        {
                            m_camera.ProcessInput(CameraDirection::RIGHT, speed);
                        }
                        if (sdl_event.key.keysym.sym == SDLK_s)
                        {
                            m_camera.ProcessInput(CameraDirection::BACKWARD, speed);
                        }
                        if (sdl_event.key.keysym.sym == SDLK_w)
                        {
                            m_camera.ProcessInput(CameraDirection::FORWARD, speed);
                        }

                        // Rest camera
                        if (sdl_event.key.keysym.sym == SDLK_SPACE)
                            InitCamera();
                    } // End case SDL_KEYDOWN
                }// End switch (sdl_event.type
            }// End while (SDL_PollEvent(&sdl_event))

            {   // Update mouse position
                int x, y;
                SDL_PumpEvents();
                uint32_t button = SDL_GetMouseState(&x, &y);
                // Mouse should only care about area of screen where nothing else is drawn
                // Need to hide these details in the a function
                VkExtent2D mouse_accessible_window_size{ ImGui::GetIO().DisplaySize.x, ImGui::GetIO().DisplaySize.y };
                for (const auto viewport_size : GetGui().GetImGuiViewportRectangles())
                {
                    if (mouse_accessible_window_size.width != viewport_size.width) 
                        mouse_accessible_window_size.width -= viewport_size.width;

                    if (mouse_accessible_window_size.height != viewport_size.height)
                        mouse_accessible_window_size.height -= viewport_size.height;
                }
                m_camera.UpdateRotation(x, y, mouse_accessible_window_size.width, mouse_accessible_window_size.height);
            }

            Draw();
        }
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    AllocatedBuffer RenderEngine::CreateBuffer(size_t allocation_size, VkBufferUsageFlags usage, VmaMemoryUsage memory_usage)
    {
        VkBufferCreateInfo buffer_info{};
        buffer_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        buffer_info.size = allocation_size;
        buffer_info.usage = usage;

        VmaAllocationCreateInfo vmaalloc_info{};
        vmaalloc_info.usage = memory_usage;

        AllocatedBuffer new_buffer{};

        VkCheck(vmaCreateBuffer(m_vma_allocator, &buffer_info, &vmaalloc_info, &new_buffer.buffer, &new_buffer.allocation, nullptr));

        return new_buffer;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    size_t RenderEngine::PadUniformBufferSize(size_t original_size)
    {
        size_t minimum_uniform_buffer_alignment = m_vulkan_physical_device_properties.limits.minUniformBufferOffsetAlignment;
        size_t aligned_size = original_size;

        if (minimum_uniform_buffer_alignment > 0)
            aligned_size = (aligned_size + minimum_uniform_buffer_alignment - 1) & ~(minimum_uniform_buffer_alignment - 1);

        return aligned_size;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    FrameData& RenderEngine::GetCurrentFrame()
    {
        return m_frame_data[m_frame_number % FRAME_OVERLAP];
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    ui::ImGuiLayer& RenderEngine::GetGui()
    {
        if (!m_gui)
            std::runtime_error("RenderEngine GUI has not been instantiated");
        return *m_gui;
    }

    void RenderEngine::ImmediateSubmit(std::function<void(VkCommandBuffer)>&& func)
    {
        VkCommandBufferAllocateInfo command_buffer_allocation_info = CommandBufferAllocateInfo(m_upload_context.command_pool, 1);
        VkCommandBuffer             command_buffer;
        VkCheck(vkAllocateCommandBuffers(m_vulkan_device, &command_buffer_allocation_info, &command_buffer));

        VkCommandBufferBeginInfo command_buffer_begin_info = CommandBufferBeginInfo(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);
        VkCheck(vkBeginCommandBuffer(command_buffer, &command_buffer_begin_info));

        func(command_buffer);

        VkCheck(vkEndCommandBuffer(command_buffer));

        VkSubmitInfo submit_info = SubmitInfo(&command_buffer);
        VkCheck(vkQueueSubmit(m_graphics_queue, 1, &submit_info, m_upload_context.upload_fence));

        vkWaitForFences(m_vulkan_device, 1, &m_upload_context.upload_fence, true, 9999999999);
        vkResetFences(m_vulkan_device, 1, &m_upload_context.upload_fence);
        vkResetCommandPool(m_vulkan_device, m_upload_context.command_pool, 0);
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    bool RenderEngine::LoadShaderModule(std::filesystem::path file_path, VkShaderModule* shader)
    {
        std::ifstream file(file_path, std::ios::ate | std::ios::binary);
        if (!file.is_open())
            return false;

        size_t file_size = (size_t)file.tellg();					// Gives size of file
        std::vector<uint32_t> buffer(file_size / sizeof(uint32_t));

        // Place cursor at begin of file and read in file
        file.seekg(0);
        file.read(reinterpret_cast<char*>(buffer.data()), file_size);
        file.close();

        // Create shader module 
        VkShaderModuleCreateInfo shader_module_info{};
        shader_module_info.sType    = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
        shader_module_info.pNext    = nullptr;
        shader_module_info.codeSize = buffer.size() * sizeof(uint32_t);
        shader_module_info.pCode    = buffer.data();

        VkShaderModule shader_module;
        if (vkCreateShaderModule(m_vulkan_device, &shader_module_info, nullptr, &shader_module) != VK_SUCCESS)
            return false;

        *shader = shader_module;
        return true;
    }

    ///////////////////////////////////////
    ///        Private Interface        ///
    ///////////////////////////////////////
    void RenderEngine::InitCamera()
    {
        m_camera.yaw                 = 0.0;
        m_camera.pitch               = 0.0;
        m_camera.position            = glm::vec3{ 0.0, 1.0, 0.0 };
        m_camera.front               = glm::vec3{ 0.0, 0.0, 1.0 };
        m_camera.right               = glm::vec3{ 1.0, 0.0, 0.0 };
        m_camera.up                  = glm::cross(m_camera.right, m_camera.up);
        m_camera.view_matrix         = glm::lookAt(m_camera.position, m_camera.position + m_camera.front, m_camera.up);
        m_camera.field_of_view       = 70.0f;
        m_camera.projection_matrix   = glm::perspective(glm::radians(m_camera.field_of_view), m_aspect_ratio, 0.1f, 200.f);
        m_camera.projection_matrix[1][1] *= -1.0;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void RenderEngine::InitCommands()
    {
        LogLog("\tInitializing command buffers");
        VkCommandPoolCreateInfo command_pool_info = CommandPoolCreateInfo(m_graphics_queue_family, VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT);
        for (int i = 0; i < FRAME_OVERLAP; i++)
        {
            VkCheck(vkCreateCommandPool(m_vulkan_device, &command_pool_info, nullptr, &m_frame_data[i].command_pool));
            
            VkCommandBufferAllocateInfo command_buffer_info = CommandBufferAllocateInfo(m_frame_data[i].command_pool, 1);
            VkCheck(vkAllocateCommandBuffers(m_vulkan_device, &command_buffer_info, &m_frame_data[i].command_buffer));
            m_main_deletion_queue.PushFunction([=]()
                {
                    Print("De-allocating Frame data's command pool from RenderEngine::InitCommands()");
                    vkDestroyCommandPool(m_vulkan_device, m_frame_data[i].command_pool, nullptr);
                });

            m_resize_window_queue.PushFunction([=]()
                {
                    Print("Freeing command buffer from command pool from RenderEngine::InitCommands()");
                    vkFreeCommandBuffers(m_vulkan_device, m_frame_data[i].command_pool, 1, &m_frame_data[i].command_buffer);
                });
        }

        VkCommandPoolCreateInfo upload_context_command_pool_info = CommandPoolCreateInfo(m_graphics_queue_family);
        VkCheck(vkCreateCommandPool(m_vulkan_device, &upload_context_command_pool_info, nullptr, &m_upload_context.command_pool));
        m_main_deletion_queue.PushFunction([=]()
            {
                Print("De-allocating Upload Context command pool from RenderEngine::InitCommands()");
                vkDestroyCommandPool(m_vulkan_device, m_upload_context.command_pool, nullptr);
            });
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void RenderEngine::InitDefaultRenderPass()
    {
        LogLog("\tBuilding default render pass");
        VkAttachmentDescription color_attachment{};	
        color_attachment.format			= m_swapchain_imagine_format;		// set format to that of swapchain
        color_attachment.samples		= VK_SAMPLE_COUNT_1_BIT;			// what is MSAA?
        color_attachment.loadOp			= VK_ATTACHMENT_LOAD_OP_CLEAR;		// clear when attachment is loaded
        color_attachment.storeOp		= VK_ATTACHMENT_STORE_OP_STORE;		// keep attachment stored when render pass ends
        color_attachment.stencilLoadOp	= VK_ATTACHMENT_LOAD_OP_DONT_CARE;	// don't care about stencils
        color_attachment.stencilStoreOp	= VK_ATTACHMENT_STORE_OP_DONT_CARE;	// don't care about stencils
        color_attachment.initialLayout	= VK_IMAGE_LAYOUT_UNDEFINED;		// for not we don't care
        color_attachment.finalLayout	= VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;	// image has to be ready for display

        VkAttachmentReference color_attachment_ref{};
        color_attachment_ref.attachment = 0;										// attachment number indexed by render pass itself
        color_attachment_ref.layout		= VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;


        // Depth buffer code
        VkAttachmentDescription depth_attachment{};
        depth_attachment.flags          = 0;
        depth_attachment.format         = m_depth_format;
        depth_attachment.samples        = VK_SAMPLE_COUNT_1_BIT;
        depth_attachment.loadOp         = VK_ATTACHMENT_LOAD_OP_CLEAR;
        depth_attachment.storeOp        = VK_ATTACHMENT_STORE_OP_STORE;
        depth_attachment.stencilLoadOp  = VK_ATTACHMENT_LOAD_OP_CLEAR;
        depth_attachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        depth_attachment.initialLayout  = VK_IMAGE_LAYOUT_UNDEFINED;
        depth_attachment.finalLayout    = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

        VkAttachmentReference depth_attachment_ref{};
        depth_attachment_ref.attachment = 1;
        depth_attachment_ref.layout     = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
   
        VkSubpassDescription subpass{};
        subpass.pipelineBindPoint		= VK_PIPELINE_BIND_POINT_GRAPHICS;
        subpass.colorAttachmentCount	= 1;
        subpass.pColorAttachments		= &color_attachment_ref;
        subpass.pDepthStencilAttachment = &depth_attachment_ref;

        // Image life so far: 
        // UNDEFINED -> RenderpPass begins 
        //				-> Subpass 0 begins (Transition to Attachment Optimal)
        //				-> Subpass 0 renders
        //				-> Subpass 0 ends		
        //			 -> RenderPass ends (Transition to Present Source)

        VkAttachmentDescription attachments[2]{ color_attachment, depth_attachment };
        VkRenderPassCreateInfo render_pass_info{};
        render_pass_info.sType              = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
        render_pass_info.attachmentCount    = 2;
        render_pass_info.pAttachments       = attachments; 
        render_pass_info.subpassCount       = 1;
        render_pass_info.pSubpasses         = &subpass;

        VkCheck(vkCreateRenderPass(m_vulkan_device, &render_pass_info, nullptr, &m_render_pass));
        m_resize_window_queue.PushFunction([=]() 
            {
                Print("Destroying Default RenderPass from RenderEngine::InitDefaultRenderPass()");
                vkDestroyRenderPass(m_vulkan_device, m_render_pass, nullptr); 
            });
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void RenderEngine::InitDescriptors()
    {
        LogLog("\tBuidling descriptor sets and layouts");
        std::vector<VkDescriptorPoolSize> descriptor_pool_sizes =
        {
            { VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 10 },
            { VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, 10 },
            { VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 10 },
            { VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 10}
        };

        VkDescriptorPoolCreateInfo pool_info{};
        pool_info.sType         = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
        pool_info.flags         = 0;
        pool_info.maxSets       = 10;
        pool_info.poolSizeCount = static_cast<uint32_t>(descriptor_pool_sizes.size());
        pool_info.pPoolSizes    = descriptor_pool_sizes.data();

        VkCheck(vkCreateDescriptorPool(m_vulkan_device, &pool_info, nullptr, &m_descriptor_pool));

        m_main_deletion_queue.PushFunction([=]() 
            { 
                Print("Destroying descriptor pool from RenderEngine::InitDescriptor()");
                vkDestroyDescriptorPool(m_vulkan_device, m_descriptor_pool, nullptr); 
            });

        // Descriptor sets layout binding for set 0
        VkDescriptorSetLayoutBinding camera_buffer_binding  = DescriptorSetLayoutBinding(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_VERTEX_BIT, 0);
        VkDescriptorSetLayoutBinding bindings[]             = { camera_buffer_binding };

        // Descriptor sets layout binding for set 1
        VkDescriptorSetLayoutBinding object_bind = DescriptorSetLayoutBinding(VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, VK_SHADER_STAGE_VERTEX_BIT, 0);

        // Descriptor set layout info for set 0
        VkDescriptorSetLayoutCreateInfo set_info_0{};
        set_info_0.sType          = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
        set_info_0.pNext          = nullptr;
        set_info_0.bindingCount   = 1;
        set_info_0.flags          = 0;
        set_info_0.pBindings      = bindings;

        // Descriptor set layout info for set 1
        VkDescriptorSetLayoutCreateInfo set_info_1{};
        set_info_1.sType        = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
        set_info_1.pNext        = nullptr;
        set_info_1.bindingCount = 1;
        set_info_1.flags        = 0;
        set_info_1.pBindings    = &object_bind;

        VkCheck(vkCreateDescriptorSetLayout(m_vulkan_device, &set_info_0, nullptr, &m_global_descriptor_set_layout));
        VkCheck(vkCreateDescriptorSetLayout(m_vulkan_device, &set_info_1, nullptr, &m_object_descriptor_set_layout));

        m_main_deletion_queue.PushFunction([=]() 
            { 
                Print("Deleting descriptor set layouts from RenderEngine::InitDescriptors()");
                vkDestroyDescriptorSetLayout(m_vulkan_device, m_global_descriptor_set_layout, nullptr);
                vkDestroyDescriptorSetLayout(m_vulkan_device, m_object_descriptor_set_layout, nullptr);
            });

        VkDescriptorSetLayout set_layouts[] = { m_global_descriptor_set_layout, m_object_descriptor_set_layout };
        for (int i = 0; i < FRAME_OVERLAP; i++)
        {
            const int MAX_OBJECTS = 10000;
            m_frame_data[i].camera_buffer = CreateBuffer(sizeof(GPUCameraData), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU);
            m_frame_data[i].object_buffer = CreateBuffer(sizeof(GPUObjectData) * MAX_OBJECTS, VK_BUFFER_USAGE_STORAGE_BUFFER_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU);

            // allocate descriptor sets
            VkDescriptorSetAllocateInfo descriptor_set_allocation_info_0{};
            descriptor_set_allocation_info_0.sType                = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
            descriptor_set_allocation_info_0.pNext                = nullptr;
            descriptor_set_allocation_info_0.descriptorPool       = m_descriptor_pool;
            descriptor_set_allocation_info_0.descriptorSetCount   = 1;
            descriptor_set_allocation_info_0.pSetLayouts          = &m_global_descriptor_set_layout;

            VkCheck(vkAllocateDescriptorSets(m_vulkan_device, &descriptor_set_allocation_info_0, &m_frame_data[i].global_descriptor_set));

            VkDescriptorSetAllocateInfo descriptor_set_allocation_info_1{};
            descriptor_set_allocation_info_1.sType               = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
            descriptor_set_allocation_info_1.pNext               = nullptr;
            descriptor_set_allocation_info_1.descriptorPool      = m_descriptor_pool;
            descriptor_set_allocation_info_1.descriptorSetCount  = 1;
            descriptor_set_allocation_info_1.pSetLayouts         = &m_object_descriptor_set_layout;

            VkCheck(vkAllocateDescriptorSets(m_vulkan_device, &descriptor_set_allocation_info_1, &m_frame_data[i].object_descriptor_set));


            // Set up descriptor set 0
            VkDescriptorBufferInfo camera_buffer_info   = DescriptorBufferInfo(m_frame_data[i].camera_buffer.buffer, 0, sizeof(GPUCameraData));
            VkDescriptorBufferInfo object_buffer_info   = DescriptorBufferInfo(m_frame_data[i].object_buffer.buffer, 0, sizeof(GPUObjectData) * MAX_OBJECTS);

            VkWriteDescriptorSet write_camera   = WriteDescriptorSet(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, m_frame_data[i].global_descriptor_set, &camera_buffer_info, 0);
            VkWriteDescriptorSet write_object   = WriteDescriptorSet(VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, m_frame_data[i].object_descriptor_set, &object_buffer_info, 0);
            VkWriteDescriptorSet write_sets[]   = { write_camera, write_object };
        
            vkUpdateDescriptorSets(m_vulkan_device, 2, write_sets, 0, nullptr);     // Upload descriptor set zeor

            m_main_deletion_queue.PushFunction([=]()
                {
                    Print("Deleting object and camera buffers from RenderEngine::InitDescriptors()");
                    vmaDestroyBuffer(m_vma_allocator, m_frame_data[i].camera_buffer.buffer, m_frame_data[i].camera_buffer.allocation);
                    vmaDestroyBuffer(m_vma_allocator, m_frame_data[i].object_buffer.buffer, m_frame_data[i].object_buffer.allocation);
                });
        }
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void RenderEngine::InitFrameBuffers()
    {
        VkFramebufferCreateInfo frame_buffer_info = FrameBufferCreateInfo(m_render_pass, m_window_extent, 1);

        uint32_t swapchain_image_count = m_swapchain_images.size();
        m_frame_buffers = std::vector<VkFramebuffer>(swapchain_image_count);

        // create frame buffers for each of the swapchain images
        for (int i = 0; i < swapchain_image_count; i++)
        {
            VkImageView image_view_attachments[2]{ m_swapchain_image_views[i], m_depth_image_view };

            frame_buffer_info.pAttachments = image_view_attachments;
            frame_buffer_info.attachmentCount = 2;

            VkCheck(vkCreateFramebuffer(m_vulkan_device, &frame_buffer_info, nullptr, &m_frame_buffers[i]));

            m_resize_window_queue.PushFunction([=]()
                {
                    Print("Deleting frame buffers from RenderEngine::InitFrameBuffers()");
                    vkDestroyFramebuffer(m_vulkan_device, m_frame_buffers[i], nullptr);
                    vkDestroyImageView(m_vulkan_device, m_swapchain_image_views[i], nullptr);
                });
        }
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void RenderEngine::InitPipelines()
    {
        LogLog("\tSetting up render pipelines");
        VkShaderModule color_mesh_shader;
        if (!LoadShaderModule(shaders_dir / "default_lit.frag.spv", &color_mesh_shader))
            LogError("Error when building the triangle fragment shader module");
        else
            LogSuccess("Triangle fragment shader successfully loaded");

        VkShaderModule mesh_vertex_shader;
        if (!LoadShaderModule(shaders_dir / "tri_mesh.vert.spv", &mesh_vertex_shader)) 
            LogError("Error when building the triangle vertex shader module");
        else
            LogSuccess("Triangle vertex shader successfully loaded");

        // Pipeline layout for just triangles and monkey
        VkDescriptorSetLayout set_layouts[]                     = { m_global_descriptor_set_layout, m_object_descriptor_set_layout };
        VkPipelineLayoutCreateInfo mesh_pipeline_layout_info    = PipelineLayoutCreateInfo();
        mesh_pipeline_layout_info.pPushConstantRanges           = nullptr;
        mesh_pipeline_layout_info.pushConstantRangeCount        = 0;
        mesh_pipeline_layout_info.setLayoutCount                = 2;
        mesh_pipeline_layout_info.pSetLayouts                   = set_layouts;
        VkPipelineLayout mesh_pipeline_layout{};
        VkCheck(vkCreatePipelineLayout(m_vulkan_device, &mesh_pipeline_layout_info, nullptr, &mesh_pipeline_layout));

        VertexInputDescription vertex_description = Vertex::GetVertexDescription();

        PipelineBuilder pipeline_builder{};
        pipeline_builder.shader_stages_infos.push_back(PipelineShaderStageCreateInfo(VK_SHADER_STAGE_VERTEX_BIT, mesh_vertex_shader));
        pipeline_builder.shader_stages_infos.push_back(PipelineShaderStageCreateInfo(VK_SHADER_STAGE_FRAGMENT_BIT, color_mesh_shader));
        pipeline_builder.vertex_input_info         = VertexInputStateCreateInfo();
        pipeline_builder.input_assembly_info       = InputAssemblyCreateInfo(VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST);
        pipeline_builder.viewport.x                = 0.0f;
        pipeline_builder.viewport.y                = 0.0f;
        pipeline_builder.viewport.width            = (float)m_window_extent.width;
        pipeline_builder.viewport.height           = (float)m_window_extent.height;
        pipeline_builder.viewport.minDepth         = 0.0f;
        pipeline_builder.viewport.maxDepth         = 1.0f;
        pipeline_builder.scissor.offset            = { 0, 0 };
        pipeline_builder.scissor.extent            = m_window_extent;
        pipeline_builder.rasterizer_info           = RasterizationStateCreateInfo(VK_POLYGON_MODE_FILL); // Draw filled triangles
        pipeline_builder.multisampling_info        = MultisamplingStateCreateInfo();
        pipeline_builder.color_blend_attachement   = ColorBlendAttachmentState();
        pipeline_builder.pipeline_layout           = mesh_pipeline_layout;
        pipeline_builder.depth_stencil             = DepthStencilCreateInfo(true, true, VK_COMPARE_OP_LESS_OR_EQUAL);

        // Connect the pipeline builder to vertex input info
        pipeline_builder.vertex_input_info.pVertexAttributeDescriptions    = vertex_description.attributes.data();
        pipeline_builder.vertex_input_info.vertexAttributeDescriptionCount = vertex_description.attributes.size();
        pipeline_builder.vertex_input_info.pVertexBindingDescriptions      = vertex_description.bindings.data();
        pipeline_builder.vertex_input_info.vertexBindingDescriptionCount   = vertex_description.bindings.size();

        VkPipeline mesh_pipeline = pipeline_builder.BuildPipeline(m_vulkan_device, m_render_pass);


        // FIXME: This could should not remain here. We eventually want to be able to dynamically load meshes
        //        For now we keep it here to debug rendering pipelines...
        m_scene.CreateMaterial(mesh_pipeline, mesh_pipeline_layout, "monkey_smooth");

        if (m_scene.AddRenderObjects())
            m_scene.AllowDrawObjects();
        UploadSceneMeshes();
    

        // Destroy modules after used
        vkDestroyShaderModule(m_vulkan_device, mesh_vertex_shader, nullptr);
        vkDestroyShaderModule(m_vulkan_device, color_mesh_shader, nullptr);
     
        m_resize_window_queue.PushFunction([=]()
            {
                Print("Deleting pipeline and pipeline layouts from RenderEngine::InitPipelines()");
                vkDestroyPipeline(m_vulkan_device, mesh_pipeline, nullptr);
                vkDestroyPipelineLayout(m_vulkan_device, mesh_pipeline_layout, nullptr);
            });
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void RenderEngine::InitScene()
    {
        // TODO: Allow scenes to be introduces at runtime
        //       Before that we have to specify a format with which to save file
        m_scene = std::move(Scene());
        m_scene.LoadMesh(default_mesh_dir / "monkey_smooth.obj", "monkey_smooth");
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void RenderEngine::InitSwapchain()
    {
        LogLog("\tInitializing swap chains");
        vkb::SwapchainBuilder swapchain_builder{ m_vulkan_physical_device, m_vulkan_device, m_vulkan_surface };
        vkb::Swapchain vkb_swapchain = swapchain_builder
            .use_default_format_selection()
            .set_desired_present_mode(VK_PRESENT_MODE_FIFO_KHR) // use vsync present mode??
            .set_desired_extent(m_window_extent.width, m_window_extent.height)
            .build()
            .value();
        
        // store swap chain
        m_swapchain                 = vkb_swapchain.swapchain;
        m_swapchain_images          = vkb_swapchain.get_images().value();
        m_swapchain_image_views     = vkb_swapchain.get_image_views().value();
        m_swapchain_imagine_format  = vkb_swapchain.image_format;

        m_resize_window_queue.PushFunction([=]()
            {
                Print("Delete swapchain from RenderEngine::InitSwapchain()");
                vkDestroySwapchainKHR(m_vulkan_device, m_swapchain, nullptr);
            });
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void RenderEngine::InitSyncStructures()
    {
        LogLog("\tInitializing Vulkan sync structures");
        VkFenceCreateInfo       fence_info      = FenceCreateInfo(VK_FENCE_CREATE_SIGNALED_BIT);
        VkSemaphoreCreateInfo   semaphore_info  = SemaphoreCreateInfo();


        for (int i = 0; i < FRAME_OVERLAP; i++)
        {
            VkCheck(vkCreateFence(m_vulkan_device, &fence_info, nullptr, &m_frame_data[i].render_fence));

            m_main_deletion_queue.PushFunction([=]() 
                {
                    Print("Deleting rendering fences from RenderEngine::InitSyncStructure()");
                    vkDestroyFence(m_vulkan_device, m_frame_data[i].render_fence, nullptr); 
                });


            VkCheck(vkCreateSemaphore(m_vulkan_device, &semaphore_info, nullptr, &m_frame_data[i].present_semaphore));
            VkCheck(vkCreateSemaphore(m_vulkan_device, &semaphore_info, nullptr, &m_frame_data[i].render_semaphore));

            m_main_deletion_queue.PushFunction([=]
                {
                    Print("Deleting rendering semaphores from RenderEngine::InitSyncStructure()");
                    vkDestroySemaphore(m_vulkan_device, m_frame_data[i].present_semaphore, nullptr);
                    vkDestroySemaphore(m_vulkan_device, m_frame_data[i].render_semaphore, nullptr);
                });
        }

        VkFenceCreateInfo uplaod_context_fence_info = FenceCreateInfo();
        VkCheck(vkCreateFence(m_vulkan_device, &uplaod_context_fence_info, nullptr, &m_upload_context.upload_fence));
        m_main_deletion_queue.PushFunction([=]() 
            { 
                Print("Deleting upload fence from RenderEgine::InitSyncStructures()");
                vkDestroyFence(m_vulkan_device, m_upload_context.upload_fence, nullptr); 
            });
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void RenderEngine::InitVulkan()
    {
        LogLog("\tInitializing Vulkan API");
        vkb::InstanceBuilder builder;

        uint32_t extensions_count = 0;
        SDL_Vulkan_GetInstanceExtensions(m_window->window, &extensions_count, nullptr);
        std::vector<const char*> extensions(extensions_count);
        SDL_Vulkan_GetInstanceExtensions(m_window->window, &extensions_count, extensions.data());

        // Create Vulkan instance with basic debug features
        builder.set_app_name("VizQGPApp")
            .request_validation_layers(true)
            .require_api_version(1, 1, 0)
            .use_default_debug_messenger();

        for (uint32_t i = 0; i < extensions_count; i++)
            builder.enable_extension(extensions[i]);
        
        auto inst_ret = builder.build();

        for (uint32_t n = 0; n < extensions_count; n++)
            break;// delete[] extensions[n];

        // Create and store instance
        vkb::Instance vkb_inst      = inst_ret.value();
        m_vulkan_instance			= vkb_inst.instance;
        m_vulkan_debug_messenger	= vkb_inst.debug_messenger;


        // Get surface of window created with SDL
        bool created_surface = SDL_Vulkan_CreateSurface(m_window->window, m_vulkan_instance, &m_vulkan_surface);
        assert(created_surface && "Failed to create `m_vulkan_surface`");

        // Use vkb to select GPU
        vkb::PhysicalDeviceSelector selector{ vkb_inst };
        vkb::PhysicalDevice physical_device = selector
            .set_minimum_version(1, 1)
            .set_surface(m_vulkan_surface)
            .select()
            .value();

        // Create final Vulkan device
        vkb::DeviceBuilder device_builder{ physical_device };
        vkb::Device vkb_device = device_builder.build().value();

        m_vulkan_device             = vkb_device.device;
        m_vulkan_physical_device    = physical_device.physical_device;

        // use vkb to get graphics queue(s)
        m_graphics_queue        = vkb_device.get_queue(vkb::QueueType::graphics).value();
        m_graphics_queue_family	= vkb_device.get_queue_index(vkb::QueueType::graphics).value();

        // Initialize allocation buffers
        VmaAllocatorCreateInfo allocator_info{};
        allocator_info.physicalDevice   = m_vulkan_physical_device;
        allocator_info.device           = m_vulkan_device;
        allocator_info.instance         = m_vulkan_instance;
        vmaCreateAllocator(&allocator_info, &m_vma_allocator);

        m_main_deletion_queue.PushFunction([&]() 
            {
                Print("Destroying the VMA Allocator from RenderEngine::InitVulkan()");
                vmaDestroyAllocator(m_vma_allocator); 
            });

        // Depth buffer components
        VkExtent3D depth_image_extent{ m_window_extent.width, m_window_extent.height, 1 };

        // hardcoding depth format to be 32 bit float;
        m_depth_format = VK_FORMAT_D32_SFLOAT;   

        VkImageCreateInfo depth_image_info = ImageCreatInfo(m_depth_format, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, depth_image_extent);

        // Allocate GPU memory for depth image
        VmaAllocationCreateInfo depth_image_allocation_info{};
        depth_image_allocation_info.usage           = VMA_MEMORY_USAGE_GPU_ONLY;
        depth_image_allocation_info.requiredFlags   = VkMemoryPropertyFlags(VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
        vmaCreateImage(m_vma_allocator, &depth_image_info, &depth_image_allocation_info, &m_depth_image.image, &m_depth_image.allocation, nullptr);

        // build image-view from the depth image
        VkImageViewCreateInfo depth_image_view_info = ImageViewCreateInfo(m_depth_format, m_depth_image.image, VK_IMAGE_ASPECT_DEPTH_BIT);

        VkCheck(vkCreateImageView(m_vulkan_device, &depth_image_view_info, nullptr, &m_depth_image_view));

        m_main_deletion_queue.PushFunction([=]()
            {
                Print("Destroying depth image from RenderEngine::InitVulkan()");
                vkDestroyImageView(m_vulkan_device, m_depth_image_view, nullptr);
                vmaDestroyImage(m_vma_allocator, m_depth_image.image, m_depth_image.allocation);
            });

        vkGetPhysicalDeviceProperties(m_vulkan_physical_device, &m_vulkan_physical_device_properties);
        Print("The GPU has a minimum buffer alignement of", m_vulkan_physical_device_properties.limits.minUniformBufferOffsetAlignment);
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void RenderEngine::OnWindowResize(uint32_t width, uint32_t height)
    {
        LogLog("Resizing window!");
        vkDeviceWaitIdle(m_vulkan_device);
        // Note(Kevin): The SDL documentation is not clear on this. However,
        //              comparing the SDL_WindowEvent and SDL_SetWindowSize 
        //              documentation leades me to belief this is true
        m_window->width  = width;
        m_window->height = height;
        m_aspect_ratio  = static_cast<float>(width) / static_cast<float>(height);

        m_window_extent.width   = width;
        m_window_extent.height  = height;

        // Seems that calling this function only works when SDL does the rendering
        // SDL_SetWindowSize(m_window, m_width, m_height);


        // TODO: After resizing the window, we have to recreate the swapchains and frame_buffers.
        //       This requires some additional functions, and moving the automatic destructions of
        //       swapchains and frame buffers out of the main deletion queue
        //      DONE!
        m_resize_window_queue.Flush();

        InitSwapchain();
        ReInitCommandBuffers();
        InitDefaultRenderPass();
        InitFrameBuffers();
        InitPipelines();
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void RenderEngine::ReInitCommandBuffers()
    {
        LogLog("Recreating command buffers");
        for (int i = 0; i < FRAME_OVERLAP; i++)
        {
            VkCommandBufferAllocateInfo command_buffer_info = CommandBufferAllocateInfo(m_frame_data[i].command_pool, 1);
            VkCheck(vkAllocateCommandBuffers(m_vulkan_device, &command_buffer_info, &m_frame_data[i].command_buffer));

            m_resize_window_queue.PushFunction([=]()
                {
                    Print("Freeing command buffer from command pool from RenderEngine::ReInitCommandBuffers()");
                    vkFreeCommandBuffers(m_vulkan_device, m_frame_data[i].command_pool, 1, &m_frame_data[i].command_buffer);
                });
        }
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void RenderEngine::UploadSceneMeshes()
    {
        for (auto& [key, mesh] : m_scene.meshes)
        {
            const size_t buffer_size = mesh.vertices.size() * sizeof(Vertex);
            VkBufferCreateInfo buffer_info{};
            buffer_info.sType   = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
            buffer_info.size    = buffer_size;
            buffer_info.usage   = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;

            VmaAllocationCreateInfo vmaalloc_info{};
            vmaalloc_info.usage = VMA_MEMORY_USAGE_CPU_ONLY;

            AllocatedBuffer staging_buffer;

            VkCheck(vmaCreateBuffer(m_vma_allocator, &buffer_info, &vmaalloc_info, &staging_buffer.buffer, &staging_buffer.allocation, nullptr));

            // Map data to VkBuffer
            void* data;
            vmaMapMemory(m_vma_allocator, staging_buffer.allocation, &data);
            memcpy(data, mesh.vertices.data(), buffer_size);
            vmaUnmapMemory(m_vma_allocator, staging_buffer.allocation);

            // Make GPU-side buffer
            VkBufferCreateInfo vertex_buffer_info{};
            vertex_buffer_info.sType    = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
            vertex_buffer_info.pNext    = nullptr;
            vertex_buffer_info.size     = buffer_size;
            vertex_buffer_info.usage    = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;

            vmaalloc_info.usage = VMA_MEMORY_USAGE_GPU_ONLY;

            VkCheck(vmaCreateBuffer(m_vma_allocator, &vertex_buffer_info, &vmaalloc_info, &mesh.vertex_buffer.buffer, &mesh.vertex_buffer.allocation, nullptr));

            ImmediateSubmit([=](VkCommandBuffer command_buffer)
                {
                    VkBufferCopy copy;
                    copy.dstOffset = 0;
                    copy.srcOffset = 0;
                    copy.size      = buffer_size;
                    vkCmdCopyBuffer(command_buffer, staging_buffer.buffer, mesh.vertex_buffer.buffer, 1, &copy);
                });

            vmaDestroyBuffer(m_vma_allocator, staging_buffer.buffer, staging_buffer.allocation);
        }
    }
}