//
// Author: Kevin Ingles
// File: Scene.hpp
// Description: Meant to describe all the data that might be drawn in a single draw call
//              Should be be able to add scenes at runtime, which requires a way to store them

#ifndef VIZQGP_SCENE_HPP
#define VIZQGP_SCENE_HPP

#include <Camera.hpp>
#include <filesystem>
#include <Log.hpp>
#include <string_view>
#include <vector>
#include <VkStructs.hpp>
#include <unordered_map>

namespace vizqgp
{
    class Scene
    {
    public:
        Scene() = default;
        ~Scene();

        Scene& operator=(Scene&& other) noexcept;
        
        bool        AddRenderObjects();
        void        AllowDrawObjects();
        Material*   CreateMaterial(VkPipeline pipeline, VkPipelineLayout layout, std::string_view name);
        void        DestroyMeshAllocations(VmaAllocator& allocator);
        void        DrawObjects(VkCommandBuffer comman_buffer, VmaAllocator& allocator, Camera& scene_camera, FrameData& current_frame_data);
        Material*   GetMaterial(const std::string_view& name);
        Mesh*       GetMesh(const std::string_view& name);
        Mesh*       LoadMesh(std::filesystem::path file_name, std::string_view name);
        // void UploadMesh(Mesh& mesh, VmaAllocator& allocator); // Needs to be implemented 
        
        std::unordered_map<std::string_view, Mesh>      meshes;
        std::unordered_map<std::string_view, Material>  materials;
    private:
        bool                        b_draw_objects = false;
        std::vector<RenderObject>   m_render_objects;
    };
}

#endif