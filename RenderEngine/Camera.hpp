//
// Author: Kevin Ingles
// File: Camera.h
// Description: Projective camera class

#ifndef CAMERA_HPP
#define CAMERA_HPP

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace vizqgp
{
    enum class CameraDirection { FORWARD = 0, BACKWARD, LEFT, RIGHT };

    class Camera
    {
    public:
        void        ChangeSensitivity(float sens);
        glm::mat4   GetCamera(glm::mat4& model);
        void        ProcessInput(CameraDirection dir, float speed);
        void        UpdateRotation(float x, float y, float window_width, float window_height);

        float field_of_view;
        float last_x{ 0 };          // Keep track of last mouse position, and only update if it changes
        float last_y{ 0 };          // Keep track of last mouse position, and only update if it changes
        float sensitivity{ 1e-3 };  // How quickly mouse rotation is
        float pitch;
        float yaw;

        glm::vec3 front;                        // the direction the camera is pointing
        glm::vec3 position;                     // position of camera
        glm::mat4 projection_matrix;
        glm::vec3 up;                           // y direction is camera frame
        glm::vec3 right;                        // used to calculate front up
        glm::mat4 view_matrix;                  // transforms world object onto 2d screen
        glm::vec3 world_up{ 0.0, 1.0, 0.0 };    // used to calculate right vector
    private:
        void UpdateVectors();
    };
}

#endif