//
// Author: Kevin Ingles
// File: Scene.cpp
// Description: Implementraion of Scene.hpp functions

#include <Print.hpp>
#include <Scene.hpp>

namespace vizqgp
{
    Scene::~Scene()
    {
        m_render_objects.clear();
        meshes.clear();
        materials.clear();
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    Scene& Scene::operator=(Scene&& other) noexcept
    {
        m_render_objects    = std::move(other.m_render_objects);
        meshes              = std::move(other.meshes);
        materials           = std::move(other.materials);

        return *this;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    bool Scene::AddRenderObjects()
    {
        if (meshes.size() != materials.size())
        {
            LogError("The number of meshes does not equal the number of materials. Not drawing data.");
            LogError(fmt::format("\tmeshes.size() = {}, and materials.size() = {}", meshes.size(), materials.size()).data());
            return false;
        }
        else if (meshes.size() == 0)
        {
            LogWarning("Number of meshes is 0. No data to draw.");
            return false;
        }

        // Function assumes that each mesh has a material with the corresponding name
        for (auto& [key, mesh] : meshes)
        {
            RenderObject render_object{
                .mesh               = &mesh,
                .material           = &materials[key],
                .transform_matrix   = glm::mat4{ 1.0f }
            };

            m_render_objects.push_back(render_object);
        }
        return true;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void Scene::AllowDrawObjects()
    {
        b_draw_objects = true;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    Material* Scene::CreateMaterial(VkPipeline pipeline, VkPipelineLayout layout, std::string_view name)
    {
        Material material {
            .pipeline           = pipeline,
            .pipeline_layout    = layout
        };
        materials[name] = material;
        return &materials[name];
    }

    void Scene::DestroyMeshAllocations(VmaAllocator& allocator)
    {
        Print("Deleteing vertex buffers in LoadMeshes()");
        for (auto& [key, mesh] : meshes)
            vmaDestroyBuffer(allocator, mesh.vertex_buffer.buffer, mesh.vertex_buffer.allocation);
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....
    
    void Scene::DrawObjects(VkCommandBuffer command_buffer, VmaAllocator& allocator, Camera& scene_camera, FrameData& current_frame_data)
    {
        if (b_draw_objects)
        {
            // send camera data to GPU
            GPUCameraData gpu_camera_data{
                .view_matrix = scene_camera.view_matrix,
                .projection_matrix = scene_camera.projection_matrix,
                .view_projection = scene_camera.projection_matrix * scene_camera.view_matrix
            };
            void* p_camera_data;
            vmaMapMemory(allocator, current_frame_data.camera_buffer.allocation, &p_camera_data);
            memcpy(p_camera_data, &gpu_camera_data, sizeof(GPUCameraData));
            vmaUnmapMemory(allocator, current_frame_data.camera_buffer.allocation);

            // send data for descriptor set 1
            void* p_object_data;
            vmaMapMemory(allocator, current_frame_data.object_buffer.allocation, &p_object_data);
            GPUObjectData* object_storage_buffer = static_cast<GPUObjectData*>(p_object_data);
            for (size_t n = 0; n < m_render_objects.size(); n++)
                object_storage_buffer[n].model_matrix = m_render_objects[n].transform_matrix;
            vmaUnmapMemory(allocator, current_frame_data.object_buffer.allocation);

            // Add draw information to command buffer
            Mesh* last_mesh = nullptr;
            Material* last_material = nullptr;
            size_t index = 0;
            for (const auto& object : m_render_objects)
            {
                // and recall that we have no texture support
                if (object.material != last_material)
                {
                    last_material = object.material;
                    
                    vkCmdBindPipeline(command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, object.material->pipeline);
                    // For descriptor set 0
                    vkCmdBindDescriptorSets(command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, object.material->pipeline_layout, 0, 1, &current_frame_data.global_descriptor_set, 0, nullptr);
                    vkCmdBindDescriptorSets(command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, object.material->pipeline_layout, 1, 1, &current_frame_data.object_descriptor_set, 0, nullptr); 
                    // For descriptor set 1
                }

                if (object.mesh != last_mesh)
                {
                    VkDeviceSize offset = 0;
                    vkCmdBindVertexBuffers(command_buffer, 0, 1, &object.mesh->vertex_buffer.buffer, &offset);
                }
                
                // Draw to command buffer
                vkCmdDraw(command_buffer, object.mesh->vertices.size(), 1, 0, index++);
            }

            return;
        }
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....
    
    Material* Scene::GetMaterial(const std::string_view& name)
    {
        auto loc = materials.find(name);
        return loc != materials.end() ? &(*loc).second : nullptr;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....
    
    Mesh* Scene::GetMesh(const std::string_view& name)
    {
        auto loc = meshes.find(name);
        return loc != meshes.end() ? &(*loc).second : nullptr;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....
        
    Mesh* Scene::LoadMesh(std::filesystem::path file_name, std::string_view name)
    {
        Mesh local_mesh;
        local_mesh.LoadFromHDF5(file_name);
        meshes[name] = local_mesh;
        return &meshes[name];
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....
}