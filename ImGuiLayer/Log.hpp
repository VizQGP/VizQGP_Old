//
// Author: Kevin Ingles
// File: Log.hpp
// Description: Interface to store the all the log messages from instantiating our application.
//              We store them in a vector of string views. May add a max buffer size

// TODO: Colorize messages

#ifndef VIZQGP_LOG_HPP
#define VIZQGP_LOG_HPP

#include <vector>
#include <string>

// TODO: Upgrade Log to an ImGuiTextBuffer and draw using clipping to ensure preformance

// A quicker way to do the log is to create buffer and copy the temporary data from a log 
// message into that string, like that we do not need to take ownership of a char*, because
// the string class is simply too slow and bulky
namespace vizqgp::ui
{
    static const char* log_prefix       = "Log: ";
    static const char* debug_prefix     = "Debug: ";
    static const char* error_prefix     = "Error: ";
    static const char* success_prefix   = "Success: ";
    static const char* warning_prefix   = "Warning: ";

    enum class LogMessageType { Debug, Error, Log, Success, Warning };
    struct LogMessage
    {
        char*           message;
        LogMessageType  type;

        LogMessage() = default;
        LogMessage(const std::string& message_, LogMessageType type_);
        LogMessage(const char* message_, LogMessageType type_);
        ~LogMessage() {} // Seems C++ is smart enough to know how to free builtin types

        const char* Message() const { return const_cast<const char*>(message); }

        template<typename Ostream>
        friend Ostream& operator<<(Ostream& ostream, LogMessage message)
        {
            switch (message.type)
            {
                case LogMessageType::Debug:     { ostream << debug_prefix   << message.message; break; }
                case LogMessageType::Error:     { ostream << error_prefix   << message.message; break; }
                case LogMessageType::Log:       { ostream << log_prefix     << message.message; break; }
                case LogMessageType::Success:   { ostream << success_prefix << message.message; break; }
                case LogMessageType::Warning:   { ostream << warning_prefix << message.message; break; }
            }
            return ostream;
        } 
    };

    void CreateLog();
    [[nodiscard]] std::vector<LogMessage>& GetLog();
    void DestroyLog();

    // TODO: Make variadic functions to interface with fmt library
    //      This is very memory expensive
    //      Current problem with using const char is that the vector is non-owning, i.e.
    //          pointers get deleted before we print them to the Log and we get garbage
    void LogDebug(const std::string& message);
    void LogDebug(const char* message);
    void LogError(const std::string& message);
    void LogError(const char* message);
    void LogLog(const std::string& message);
    void LogLog(const char* message);
    void LogSuccess(const std::string& message);
    void LogSuccess(const char* message);
    void LogWarning(const std::string& message);
    void LogWarning(const char* message);

    void PrintLog(const char* file_name);
}

using vizqgp::ui::LogDebug;
using vizqgp::ui::LogError;
using vizqgp::ui::LogLog;
using vizqgp::ui::LogWarning;
using vizqgp::ui::LogSuccess;

#endif