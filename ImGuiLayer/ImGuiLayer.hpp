//
// Author: Kevin Ingles
// File: ImGuiLayer.hpp
// Description: Header file that contains the primary interface for the UI of the VizQGP application

#ifndef VIZQGP_IMGUI_LAYER_HPP
#define VIZQGP_IMGUI_LAYER_HPP

#include <imgui.h>
#include <SDL_vulkan.h>
#include <SDL_events.h>
#include <SDLWindow.hpp>
#include <vector>
#include <vulkan/vulkan.h>

#include <ImGuiImpl.hpp>


namespace vizqgp::ui
{
    class ImGuiLayer
    {
    public:
        ImGuiLayer() = default;
        ImGuiLayer(VulkanInitInfo& vulkan_init_info, Window& window, VkRenderPass& render_pass);
        ~ImGuiLayer();

        void BeginFrame();
        void DestroyFontTexturesObjects();
        void DrawLoggingConsole();
        std::vector<VkExtent2D> GetImGuiViewportRectangles();
        void EndFrame();
        void ProcessEvents(const SDL_Event* event);
        void RenderFrame(VkCommandBuffer command_buffer);
        void SetImGuiIOConfigFlags(ImGuiConfigFlags flags);
        void UploadFontTextures(VkCommandBuffer command_buffer);

        // enum class ImGuiWindowModificationFlag { Add, Subtract, Set };
        // void ModifyImGuiWindowFlags(ImGuiWindowFlags imgui_flags, ImGuiWindowModificationFlag modification_flag);

    private:
        VulkanInitInfo m_vulkan_init_info;
        // ImGuiWindowFlags m_imgui_window_flags;
    };
}
#endif