//
// Author: Kevin Ingles
// File: ImGuiImpl.cpp
// Description: ImGui backend translation unit. See header file for more detail
//              Most of the credit for this code goes to ocornut (original dear imgui dev)
//
// Note: All of this should ultimate be absorbed into the SDLWindow.xxx and VkStructs.xxx files

#include "ImGuiImpl.hpp"

#include <Log.hpp>
#include <Print.hpp>
#include <SDL_syswm.h>
#include <SDL_vulkan.h>

#if defined(__APPLE__)
#include <TargetConditionals.h>
#endif

// TODO: Make sure equal signs for struct modifications are all lined up (conforms to coding standards)

// version of SDL this code uses is 2.0.16
// SDL backend starts at line 29
// Vulkan backend starts at line 687
//
// Note: I like the functions to be organized alphabetically, hence there will be a lot
//       of forward declaration

namespace vizqgp::ui
{
    // forward declare SDL structures
    struct SDLData;
    struct SDLViewportData;

    //forward declare SDL functions
    static int          SDLCreateVkSurface(ImGuiViewport* viewport, ImU64 vk_instance, const void* vk_allocator, ImU64* out_vk_surface);
    static void         SDLCreateWindow(ImGuiViewport* viewport);
    static void         SDLDestroyWindow(ImGuiViewport* viewport);
    static SDLData*     SDLGetBackendData();
    static const char*  SDLGetClipboardText(void*);
    static bool         SDLGetWindowFocus(ImGuiViewport* viewport);
    static bool         SDLGetWindowMinimized(ImGuiViewport* viewport);
    static ImVec2       SDLGetWindowPosition(ImGuiViewport* viewport);
    static ImVec2       SDLGetWindowSize(ImGuiViewport* viewport);
    static bool         SDLInitBackend(SDL_Window* window, void* sdl_gl_context);
    static void         SDLInitPlatformInterface(SDL_Window* window, void* sdl_gl_context);
    static void         SDLRenderWindow(ImGuiViewport* viewport, void* data);
    static void         SDLSetClipboardText(void*, const char* text);
    static void         SDLSetWindowAlpha(ImGuiViewport* viewport, float alpha);
    static void         SDLSetWindowFocus(ImGuiViewport* viewport);
    static void         SDLSetWindowPosition(ImGuiViewport* viewport, ImVec2 position);
    static void         SDLSetWindowSize(ImGuiViewport* viewport, ImVec2 size);
    static void         SDLSetWindowTitle(ImGuiViewport* viewport, const char* title);
    static void         SDLShowWindow(ImGuiViewport* viewport);
    static void         SDLShutdownPlatformInterface();
    static void         SDLSwapBuffers(ImGuiViewport* viewport, void* buffer);
    static void         SDLUpdateMonitors();
    static void         SDLUpdateMouseCursor();
    static void         SDLUpdateMousePositionAndButtons();


    // SDL backend struct implmentation
    struct SDLData
    {
        SDL_Window* window;
        Uint64      time;
        bool        b_mouse_pressed[3];
        SDL_Cursor* mouse_cursor[ImGuiMouseCursor_COUNT];
        char*       clipboard_text_data;
        bool        b_mouse_can_use_global_state;
        bool        b_use_vulkan;

        SDLData()
        {
            memset(this, 0, sizeof(*this));
        }
    };

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    // This will defintiely need to be edited
    struct SDLViewportData
    {
        SDL_Window*     window;
        Uint32          window_id;
        bool            b_window_owned;
        SDL_GLContext   gl_context;

        SDLViewportData()
            : window{ nullptr }, window_id{ 0 }, b_window_owned{ false }, gl_context{ NULL } {};
        ~SDLViewportData() { IM_ASSERT(window == nullptr && gl_context == NULL); }
    };

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    ////////////////////////////////////////////////////////////////
    //             Begin SDL function implementations             //
    ////////////////////////////////////////////////////////////////

    // Implementation of functions form header file
    bool SDLInit(SDL_Window* window)
    {
        Print("Initializing SDL backend");
        if (!SDLInitBackend(window, NULL))
            return false;
        SDLData* backend_data = SDLGetBackendData();
        backend_data->b_use_vulkan = true;
        return true;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void SDLNewFrame()
    {
        SDLData* backend_data = SDLGetBackendData();
        IM_ASSERT(backend_data != NULL && "Have you called SDLInit()");
        ImGuiIO& io = ImGui::GetIO();

        int width{ 0 };
        int height{ 0 };
        int display_width{ 0 };
        int display_height{ 0 };

        SDL_GetWindowSize(backend_data->window, &width, &height);
        if (SDL_GetWindowFlags(backend_data->window) & SDL_WINDOW_MINIMIZED)
            width = height = 0;

        SDL_GL_GetDrawableSize(backend_data->window, &display_width, &display_height);
        io.DisplaySize = ImVec2(static_cast<float>(width), static_cast<float>(height));
        if ((width > 0) && (height > 0))
            io.DisplayFramebufferScale = ImVec2(static_cast<float>(display_width) / width, static_cast<float>(display_height) / height);

        static Uint64 frequency = SDL_GetPerformanceFrequency();
        Uint64 current_time = SDL_GetPerformanceCounter();
        io.DeltaTime = backend_data->time > 0 ? static_cast<float>(static_cast<double>(current_time - backend_data->time) / frequency) : static_cast<float>(1.0f / 60.0f);
        backend_data->time = current_time;

        SDLUpdateMousePositionAndButtons();
        SDLUpdateMouseCursor();
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    bool SDLProcessEvent(const SDL_Event* event)
    {
        ImGuiIO& io = ImGui::GetIO();
        SDLData* backend_data = SDLGetBackendData();

        switch (event->type)
        {
        case SDL_MOUSEWHEEL:
        {
            if (event->wheel.x > 0) io.MouseWheelH += 1;
            if (event->wheel.x < 0) io.MouseWheelH -= 1;
            if (event->wheel.y > 0) io.MouseWheel += 1;
            if (event->wheel.y < 0) io.MouseWheel -= 1;
            return true;
        }
        case SDL_MOUSEBUTTONDOWN:
        {
            if (event->button.button == SDL_BUTTON_LEFT) { backend_data->b_mouse_pressed[0] = true; }
            if (event->button.button == SDL_BUTTON_RIGHT) { backend_data->b_mouse_pressed[1] = true; }
            if (event->button.button == SDL_BUTTON_MIDDLE) { backend_data->b_mouse_pressed[2] = true; }
            return true;
        }
        case SDL_TEXTINPUT:
        {
            io.AddInputCharactersUTF8(event->text.text);
            return true;
        }
        case SDL_KEYDOWN:
        case SDL_KEYUP:
        {
            int key = event->key.keysym.scancode;
            IM_ASSERT(key >= 0 && key < IM_ARRAYSIZE(io.KeysDown));
            io.KeysDown[key]    = (event->type == SDL_KEYDOWN);
            io.KeyShift         = ((SDL_GetModState() & KMOD_SHIFT) != 0);
            io.KeyCtrl          = ((SDL_GetModState() & KMOD_CTRL) != 0);
            io.KeyAlt           = ((SDL_GetModState() & KMOD_ALT) != 0);
#ifdef _WIN32
            io.KeySuper = false;
#else
            io.KeySuper = ((SDL_GetModState() & KMOD_GUI) != 0);
#endif
            return true;
        }
        // Multi-viewport support
        case SDL_WINDOWEVENT:
            Uint8 window_event = event->window.event;
            if (window_event == SDL_WINDOWEVENT_CLOSE || window_event == SDL_WINDOWEVENT_MOVED || window_event == SDL_WINDOWEVENT_RESIZED || window_event == SDL_WINDOWEVENT_FOCUS_GAINED || window_event == SDL_WINDOWEVENT_FOCUS_LOST)
                if (ImGuiViewport* viewport = ImGui::FindViewportByPlatformHandle(static_cast<void*>(SDL_GetWindowFromID(event->window.windowID))))
                {
                    if (window_event == SDL_WINDOWEVENT_CLOSE)
                        viewport->PlatformRequestClose = true;
                    if (window_event == SDL_WINDOWEVENT_MOVED)
                        viewport->PlatformRequestMove = true;
                    if (window_event == SDL_WINDOWEVENT_RESIZED)
                        viewport->PlatformRequestResize = true;
                    if (window_event == SDL_WINDOWEVENT_FOCUS_GAINED)
                        io.AddFocusEvent(true);
                    else if (window_event == SDL_WINDOWEVENT_FOCUS_LOST)
                        io.AddFocusEvent(false);
                    return true;
                }
            break;
        }
        return false;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void SDLShutdown()
    {
        SDLData* backend_data = SDLGetBackendData();
        IM_ASSERT(backend_data != NULL && "Did you initialize backend?");
        ImGuiIO& io = ImGui::GetIO();

        SDLShutdownPlatformInterface();

        if (backend_data->clipboard_text_data)
            SDL_free(backend_data->clipboard_text_data);

        for (ImGuiMouseCursor cursor = 0; cursor < ImGuiMouseCursor_COUNT; cursor++)
            SDL_FreeCursor(backend_data->mouse_cursor[cursor]);

        io.BackendPlatformName = nullptr;
        io.BackendLanguageUserData = nullptr;
        IM_DELETE(backend_data);
    }


    // The implementation for the SDL static functions declared in this translation unit

    static int SDLCreateVkSurface(ImGuiViewport* viewport, ImU64 vk_instance, const void* vk_allocator, ImU64* out_vk_surface)
    {
        SDLViewportData* viewport_data = static_cast<SDLViewportData*>(viewport->PlatformUserData);
        (void)vk_allocator;
        SDL_bool return_value = SDL_Vulkan_CreateSurface(viewport_data->window, reinterpret_cast<VkInstance>(vk_instance), reinterpret_cast<VkSurfaceKHR*>(out_vk_surface));

        return return_value ? 0 : 1;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static void SDLCreateWindow(ImGuiViewport* viewport)
    {
        SDLData* backend_data = SDLGetBackendData();
        SDLViewportData* viewport_data = IM_NEW(SDLViewportData)();
        viewport->PlatformUserData = viewport_data;

        ImGuiViewport* main_viewport = ImGui::GetMainViewport();
        SDLViewportData* main_viewport_data = static_cast<SDLViewportData*>(main_viewport->PlatformUserData);

        // GL resources
        bool b_use_opengl = (main_viewport_data->gl_context != NULL);
        SDL_GLContext backup_context = NULL;
        if (b_use_opengl)
        {
            backup_context = SDL_GL_GetCurrentContext();
            SDL_GL_SetAttribute(SDL_GL_SHARE_WITH_CURRENT_CONTEXT, 1);
            SDL_GL_MakeCurrent(main_viewport_data->window, main_viewport_data->gl_context);
        }

        Uint32 sdl_flags = 0;
        sdl_flags |= b_use_opengl ? SDL_WINDOW_OPENGL : (backend_data->b_use_vulkan ? SDL_WINDOW_VULKAN : 0);
        sdl_flags |= SDL_GetWindowFlags(backend_data->window) & SDL_WINDOW_ALLOW_HIGHDPI;
        sdl_flags |= SDL_WINDOW_HIDDEN;
        sdl_flags |= (viewport->Flags & ImGuiWindowFlags_NoDecoration) ? SDL_WINDOW_BORDERLESS : 0;
        sdl_flags |= (viewport->Flags & ImGuiWindowFlags_NoDecoration) ? 0 : SDL_WINDOW_RESIZABLE;
#if !defined(_WIN32)
        sdl_flags |= (viewport->Flags & ImGuiViewportFlags_NoTaskBarIcon) ? SDL_WINDOW_SKIP_TASKBAR : 0;
#endif
        sdl_flags |= (viewport->Flags & ImGuiViewportFlags_TopMost) ? SDL_WINDOW_ALWAYS_ON_TOP : 0;
        viewport_data->window = SDL_CreateWindow("ImGui SDL Setup", static_cast<int>(viewport->Pos.x), static_cast<int>(viewport->Pos.y), static_cast<int>(viewport->Size.x), static_cast<int>(viewport->Size.y), sdl_flags);
        viewport_data->b_window_owned = true;
        if (b_use_opengl)
        {
            viewport_data->gl_context = SDL_GL_CreateContext(viewport_data->window);
            SDL_GL_SetSwapInterval(0);
        }
        if (b_use_opengl && backup_context)
            SDL_GL_MakeCurrent(viewport_data->window, backup_context);

        viewport->PlatformHandle = static_cast<void*>(viewport_data->window);
#if defined(_WIN32)
        SDL_SysWMinfo info;
        SDL_VERSION(&info.version);
        if (SDL_GetWindowWMInfo(viewport_data->window, &info))
            viewport->PlatformHandleRaw = info.info.win.window;
#endif
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static void SDLDestroyWindow(ImGuiViewport* viewport)
    {
        SDLViewportData* viewport_data = static_cast<SDLViewportData*>(viewport->PlatformUserData);
        if (viewport_data)
        {
            if (viewport_data->gl_context && viewport_data->b_window_owned)
                SDL_GL_DeleteContext(viewport_data->gl_context);
            if (viewport_data->window && viewport_data->b_window_owned)
                SDL_DestroyWindow(viewport_data->window);
            viewport_data->gl_context   = NULL;
            viewport_data->window       = nullptr;
            IM_DELETE(viewport_data);
        }
        viewport->PlatformUserData = viewport->PlatformHandle = nullptr;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....


    static SDLData* SDLGetBackendData()
    {
        return ImGui::GetCurrentContext() ? static_cast<SDLData*>(ImGui::GetIO().BackendPlatformUserData) : NULL;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static const char* SDLGetClipboardText(void*)
    {
        SDLData* backend_data = SDLGetBackendData();
        if (backend_data->clipboard_text_data)
            SDL_free(backend_data->clipboard_text_data);
        backend_data->clipboard_text_data = SDL_GetClipboardText();
        return backend_data->clipboard_text_data;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static bool SDLGetWindowFocus(ImGuiViewport* viewport)
    {
        return (SDL_GetWindowFlags(static_cast<SDLViewportData*>(viewport->PlatformUserData)->window) & SDL_WINDOW_INPUT_FOCUS) != 0;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static bool SDLGetWindowMinimized(ImGuiViewport* viewport)
    {
        return (SDL_GetWindowFlags(static_cast<SDLViewportData*>(viewport->PlatformUserData)->window) & SDL_WINDOW_MINIMIZED) != 0;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static ImVec2 SDLGetWindowPosition(ImGuiViewport* viewport)
    {
        SDLViewportData* viewport_data = static_cast<SDLViewportData*>(viewport->PlatformUserData);
        int x{ 0 };
        int y{ 0 };
        SDL_GetWindowPosition(viewport_data->window, &x, &y);
        return ImVec2(static_cast<float>(x), static_cast<float>(y));
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static ImVec2 SDLGetWindowSize(ImGuiViewport* viewport)
    {
        SDLViewportData* viewport_data = static_cast<SDLViewportData*>(viewport->PlatformUserData);
        int width{ 0 };
        int height{ 0 };
        SDL_GetWindowSize(viewport_data->window, &width, &height);
        return ImVec2(static_cast<float>(width), static_cast<float>(height));
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static bool SDLInitBackend(SDL_Window* window, void* sdl_gl_context)
    {
        ImGuiIO& io = ImGui::GetIO();
        LogLog(fmt::format("ImGuiIO address from SDLInitBackend(): {}", static_cast<void*>(&io)).data());
        IM_ASSERT(io.BackendPlatformUserData == NULL && "Already initialized a platform backend!");

        bool mouse_can_use_global_state = false;
        const char* sdl_backend = SDL_GetCurrentVideoDriver();
        const char* global_mouse_whitelist[] = { "windows", "cocoa", "x11", "DIVE", "VMAN" };
        for (int n = 0; n < IM_ARRAYSIZE(global_mouse_whitelist); n++)
            if (strncmp(sdl_backend, global_mouse_whitelist[n], strlen(global_mouse_whitelist[n])) == 0)
                mouse_can_use_global_state = true;

        SDLData* backend_data = IM_NEW(SDLData)();
        io.BackendPlatformUserData  = static_cast<void*>(backend_data);
        io.BackendPlatformName      = "ImGui_SDL_Backend";
        io.BackendFlags             |= ImGuiBackendFlags_HasMouseCursors;
        io.BackendFlags             |= ImGuiBackendFlags_HasSetMousePos;
        if (mouse_can_use_global_state)
            io.BackendFlags |= ImGuiBackendFlags_PlatformHasViewports;

        backend_data->window = window;
        backend_data->b_mouse_can_use_global_state = mouse_can_use_global_state;

        // Keyboard mapping. Dear ImGui will use those indices to peek into the io.KeysDown[] array.
        io.KeyMap[ImGuiKey_Tab]         = SDL_SCANCODE_TAB;
        io.KeyMap[ImGuiKey_LeftArrow]   = SDL_SCANCODE_LEFT;
        io.KeyMap[ImGuiKey_RightArrow]  = SDL_SCANCODE_RIGHT;
        io.KeyMap[ImGuiKey_UpArrow]     = SDL_SCANCODE_UP;
        io.KeyMap[ImGuiKey_DownArrow]   = SDL_SCANCODE_DOWN;
        io.KeyMap[ImGuiKey_PageUp]      = SDL_SCANCODE_PAGEUP;
        io.KeyMap[ImGuiKey_PageDown]    = SDL_SCANCODE_PAGEDOWN;
        io.KeyMap[ImGuiKey_Home]        = SDL_SCANCODE_HOME;
        io.KeyMap[ImGuiKey_End]         = SDL_SCANCODE_END;
        io.KeyMap[ImGuiKey_Insert]      = SDL_SCANCODE_INSERT;
        io.KeyMap[ImGuiKey_Delete]      = SDL_SCANCODE_DELETE;
        io.KeyMap[ImGuiKey_Backspace]   = SDL_SCANCODE_BACKSPACE;
        io.KeyMap[ImGuiKey_Space]       = SDL_SCANCODE_SPACE;
        io.KeyMap[ImGuiKey_Enter]       = SDL_SCANCODE_RETURN;
        io.KeyMap[ImGuiKey_Escape]      = SDL_SCANCODE_ESCAPE;
        io.KeyMap[ImGuiKey_KeyPadEnter] = SDL_SCANCODE_KP_ENTER;
        io.KeyMap[ImGuiKey_A]           = SDL_SCANCODE_A;
        io.KeyMap[ImGuiKey_C]           = SDL_SCANCODE_C;
        io.KeyMap[ImGuiKey_V]           = SDL_SCANCODE_V;
        io.KeyMap[ImGuiKey_X]           = SDL_SCANCODE_X;
        io.KeyMap[ImGuiKey_Y]           = SDL_SCANCODE_Y;
        io.KeyMap[ImGuiKey_Z]           = SDL_SCANCODE_Z;

        io.SetClipboardTextFn   = SDLSetClipboardText;
        io.GetClipboardTextFn   = SDLGetClipboardText;
        io.ClipboardUserData    = NULL;

        // Load mouse cursors
        backend_data->mouse_cursor[ImGuiMouseCursor_Arrow]      = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_ARROW);
        backend_data->mouse_cursor[ImGuiMouseCursor_TextInput]  = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_IBEAM);
        backend_data->mouse_cursor[ImGuiMouseCursor_ResizeAll]  = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_SIZEALL);
        backend_data->mouse_cursor[ImGuiMouseCursor_ResizeNS]   = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_SIZENS);
        backend_data->mouse_cursor[ImGuiMouseCursor_ResizeEW]   = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_SIZEWE);
        backend_data->mouse_cursor[ImGuiMouseCursor_ResizeNESW] = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_SIZENESW);
        backend_data->mouse_cursor[ImGuiMouseCursor_ResizeNWSE] = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_SIZENWSE);
        backend_data->mouse_cursor[ImGuiMouseCursor_Hand]       = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_HAND);
        backend_data->mouse_cursor[ImGuiMouseCursor_NotAllowed] = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_NO);

        // Our mouse update function expect PlatformHandle to be filled for the main viewport
        ImGuiViewport* main_viewport = ImGui::GetMainViewport();
        main_viewport->PlatformHandle = static_cast<void*>(window);
#if defined(_WIN32)
        SDL_SysWMinfo info;
        SDL_VERSION(&info.version);
        if (SDL_GetWindowWMInfo(window, &info))
            main_viewport->PlatformHandleRaw = info.info.win.window;
#endif
        SDL_SetHint(SDL_HINT_MOUSE_FOCUS_CLICKTHROUGH, "1");

        // Update monitors
        Print("Before SDLUpdateMonitors()");
        SDLUpdateMonitors();

        if ((io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable) && (io.BackendFlags & ImGuiBackendFlags_PlatformHasViewports))
            SDLInitPlatformInterface(window, sdl_gl_context);

        return true;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static void SDLInitPlatformInterface(SDL_Window* window, void* sdl_gl_context)
    {
        ImGuiPlatformIO& platform_io = ImGui::GetPlatformIO();
        platform_io.Platform_CreateWindow       = SDLCreateWindow;
        platform_io.Platform_DestroyWindow      = SDLDestroyWindow;
        platform_io.Platform_ShowWindow         = SDLShowWindow;
        platform_io.Platform_SetWindowPos       = SDLSetWindowPosition;
        platform_io.Platform_GetWindowPos       = SDLGetWindowPosition;
        platform_io.Platform_SetWindowSize      = SDLSetWindowSize;
        platform_io.Platform_GetWindowSize      = SDLGetWindowSize;
        platform_io.Platform_SetWindowFocus     = SDLSetWindowFocus;
        platform_io.Platform_GetWindowFocus     = SDLGetWindowFocus;
        platform_io.Platform_GetWindowMinimized = SDLGetWindowMinimized;
        platform_io.Platform_SetWindowTitle     = SDLSetWindowTitle;
        platform_io.Platform_RenderWindow       = SDLRenderWindow;
        platform_io.Platform_SwapBuffers        = SDLSwapBuffers;
        platform_io.Platform_SetWindowAlpha     = SDLSetWindowAlpha;
        platform_io.Platform_CreateVkSurface    = SDLCreateVkSurface;

        ImGuiViewport* main_viewport = ImGui::GetMainViewport();
        SDLViewportData* viewport_data = IM_NEW(SDLViewportData)();
        viewport_data->window           = window;
        viewport_data->window_id        = SDL_GetWindowID(window);
        viewport_data->b_window_owned   = false;
        viewport_data->gl_context       = sdl_gl_context;
        main_viewport->PlatformUserData = viewport_data;
        main_viewport->PlatformHandle   = viewport_data->window;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static void SDLRenderWindow(ImGuiViewport* viewport, void*)
    {
        SDLViewportData* viewport_data = static_cast<SDLViewportData*>(viewport->PlatformUserData);
        if (viewport_data->gl_context)
        {
            SDL_GL_MakeCurrent(viewport_data->window, viewport_data->gl_context);
            SDL_GL_SwapWindow(viewport_data->window);
        }
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static void SDLSetClipboardText(void*, const char* text)
    {
        SDL_SetClipboardText(text);
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static void SDLSetWindowAlpha(ImGuiViewport* viewport, float alpha)
    {
        SDL_SetWindowOpacity(static_cast<SDLViewportData*>(viewport->PlatformUserData)->window, alpha);
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static void SDLSetWindowFocus(ImGuiViewport* viewport)
    {
        SDL_RaiseWindow(static_cast<SDLViewportData*>(viewport->PlatformUserData)->window);
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static void SDLSetWindowPosition(ImGuiViewport* viewport, ImVec2 position)
    {
        SDL_SetWindowPosition(static_cast<SDLViewportData*>(viewport->PlatformUserData)->window, static_cast<int>(position.x), static_cast<int>(position.y));
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static void SDLSetWindowSize(ImGuiViewport* viewport, ImVec2 position)
    {
        SDL_SetWindowSize(static_cast<SDLViewportData*>(viewport->PlatformUserData)->window, static_cast<int>(position.x), static_cast<int>(position.y));
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static void SDLSetWindowTitle(ImGuiViewport* viewport, const char* title)
    {
        SDL_SetWindowTitle(static_cast<SDLViewportData*>(viewport->PlatformUserData)->window, title);
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static void SDLShowWindow(ImGuiViewport* viewport)
    {
        SDLViewportData* viewport_data = static_cast<SDLViewportData*>(viewport->PlatformUserData);
#if defined(_WIN32)
        // from windef.h
        HWND windowsOS_handle = static_cast<HWND>(viewport->PlatformHandleRaw);
        if (viewport->Flags & ImGuiViewportFlags_NoTaskBarIcon)
        {
            LONG style = GetWindowLong(windowsOS_handle, GWL_EXSTYLE);
            style &= ~WS_EX_APPWINDOW;
            style |= WS_EX_TOOLWINDOW;
            SetWindowLong(windowsOS_handle, GWL_EXSTYLE, style);
        }

        if (viewport->Flags & ImGuiViewportFlags_NoTaskBarIcon)
        {
            ShowWindow(windowsOS_handle, SW_SHOWNA);
            return;
        }
#endif
        SDL_ShowWindow(viewport_data->window);
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static void SDLShutdownPlatformInterface()
    {
        ImGui::DestroyPlatformWindows();
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static void SDLSwapBuffers(ImGuiViewport* viewport, void*)
    {
        SDLViewportData* viewport_data = static_cast<SDLViewportData*>(viewport->PlatformUserData);
        if (viewport_data->gl_context)
        {
            SDL_GL_MakeCurrent(viewport_data->window, viewport_data->gl_context);
            SDL_GL_SwapWindow(viewport_data->window);
        }
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static void SDLUpdateMonitors()
    {
        ImGuiPlatformIO& platform_io = ImGui::GetPlatformIO();
        platform_io.Monitors.resize(0);
        uint32_t display_count = SDL_GetNumVideoDisplays();
        LogLog(fmt::format("From SDLUpdateMonitors: display_cout = {}\n", display_count));
        for (uint32_t n = 0; n < display_count; n++)
        {
            // Support for multiple displays
            ImGuiPlatformMonitor monitor;
            SDL_Rect rect;
            SDL_GetDisplayUsableBounds(n, &rect);
            monitor.MainPos     = monitor.WorkPos = ImVec2(static_cast<float>(rect.x), static_cast<float>(rect.y));
            monitor.MainSize    = monitor.WorkSize = ImVec2(static_cast<float>(rect.w), static_cast<float>(rect.h));

            // Get application DPI
            float dpi = 0.0f;
            if (!SDL_GetDisplayDPI(n, &dpi, NULL, NULL))
                monitor.DpiScale = dpi / 96.0f;

            platform_io.Monitors.push_back(monitor);
        }
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static void SDLUpdateMouseCursor()
    {
        ImGuiIO& io = ImGui::GetIO();
        if (io.ConfigFlags & ImGuiConfigFlags_NoMouseCursorChange)
            return;
        SDLData* backend_data = SDLGetBackendData();

        ImGuiMouseCursor imgui_cursor = ImGui::GetMouseCursor();
        if (io.MouseDrawCursor || imgui_cursor == ImGuiMouseCursor_None)
        {
            SDL_ShowCursor(SDL_FALSE);
        }
        else
        {
            SDL_SetCursor(backend_data->mouse_cursor[imgui_cursor] ? backend_data->mouse_cursor[imgui_cursor] : backend_data->mouse_cursor[ImGuiMouseCursor_Arrow]);
            SDL_ShowCursor(SDL_TRUE);
        }
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static void SDLUpdateMousePositionAndButtons()
    {
        SDLData* backend_data = SDLGetBackendData();
        ImGuiIO& io = ImGui::GetIO();

        ImVec2 mouse_previous_position = io.MousePos;
        io.MousePos = ImVec2(-FLT_MAX, -FLT_MAX);
        io.MouseHoveredViewport = 0;

        // Update mouse buttons
        int mouse_x_local, mouse_y_local;
        Uint32 mouse_buttons = SDL_GetMouseState(&mouse_x_local, &mouse_y_local);
        io.MouseDown[0] = backend_data->b_mouse_pressed[0] || (mouse_buttons & SDL_BUTTON(SDL_BUTTON_LEFT)) != 0;
        io.MouseDown[1] = backend_data->b_mouse_pressed[1] || (mouse_buttons & SDL_BUTTON(SDL_BUTTON_RIGHT)) != 0;
        io.MouseDown[2] = backend_data->b_mouse_pressed[2] || (mouse_buttons & SDL_BUTTON(SDL_BUTTON_MIDDLE)) != 0;
        backend_data->b_mouse_pressed[0] = backend_data->b_mouse_pressed[1] = backend_data->b_mouse_pressed[2] = false;

        // Get focused window
        SDL_Window* focused_window = SDL_GetKeyboardFocus();
        SDL_Window* hovered_window = SDL_GetMouseFocus();
        SDL_Window* mouse_window = NULL;
        if (hovered_window && (backend_data->window == hovered_window || ImGui::FindViewportByPlatformHandle(static_cast<void*>(hovered_window))))
            mouse_window = hovered_window;
        else if (focused_window && (backend_data->window == focused_window || ImGui::FindViewportByPlatformHandle(static_cast<void*>(focused_window))))
            mouse_window = focused_window;

        // SDL_CaptureMouse() lets the OS know e.g. that our imgui drag outside the SDL window boundaries shouldn't e.g. trigger other operations outside
        SDL_CaptureMouse(ImGui::IsAnyMouseDown() ? SDL_TRUE : SDL_FALSE);

        if (mouse_window == NULL)
            return;

        // Set OS mouse position from Dear ImGui if requested (rarely used, only when ImGuiConfigFlags_NavEnableSetMousePos is enabled by user)
        if (io.WantSetMousePos)
        {
            if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
                SDL_WarpMouseGlobal(static_cast<int>(mouse_previous_position.x), static_cast<int>(mouse_previous_position.y));
            else
                SDL_WarpMouseInWindow(backend_data->window, static_cast<int>(mouse_previous_position.x), static_cast<int>(mouse_previous_position.y));
        }


        if (backend_data->b_mouse_can_use_global_state)
        {
            int mouse_x_global, mouse_y_global;
            SDL_GetGlobalMouseState(&mouse_x_global, &mouse_y_global);
            if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
                io.MousePos = ImVec2(static_cast<float>(mouse_x_global), static_cast<float>(mouse_y_global));

            else
            {
                int window_x, window_y;
                SDL_GetWindowPosition(mouse_window, &window_x, &window_y);
                io.MousePos = ImVec2(static_cast<float>(mouse_x_global - window_x), static_cast<float>(mouse_y_global - window_y));
            }
        }
        else
        {
            io.MousePos = ImVec2(static_cast<float>(mouse_x_local), static_cast<float>(mouse_y_local));
        }
    }


    ///////////////////////////////////////////////////////////
    ///             Vulkan Backend Implementation            //
    ///////////////////////////////////////////////////////////

    // Binary for vertex shader and fragment shader
    // These correspond to the following shader modules

    // Vertex Shader
    // #version 450 core
    // layout(location = 0) in vec2 aPos;
    // layout(location = 1) in vec2 aUV;
    // layout(location = 2) in vec4 aColor;
    // layout(push_constant) uniform uPushConstant { vec2 uScale; vec2 uTranslate; } pc;
    // 
    // out gl_PerVertex{ vec4 gl_Position; };
    // layout(location = 0) out struct { vec4 Color; vec2 UV; } Out;
    // 
    // void main()
    // {
    //     Out.Color    = aColor;
    //     Out.UV       = aUV;
    //     gl_Position  = vec4(aPos * pc.uScale + pc.uTranslate, 0, 1);
    // }
    static constexpr uint32_t __glsl_vertex_shader_spirv[] =
    {
        0x07230203,0x00010000,0x00080001,0x0000002e,0x00000000,0x00020011,0x00000001,0x0006000b,
        0x00000001,0x4c534c47,0x6474732e,0x3035342e,0x00000000,0x0003000e,0x00000000,0x00000001,
        0x000a000f,0x00000000,0x00000004,0x6e69616d,0x00000000,0x0000000b,0x0000000f,0x00000015,
        0x0000001b,0x0000001c,0x00030003,0x00000002,0x000001c2,0x00040005,0x00000004,0x6e69616d,
        0x00000000,0x00030005,0x00000009,0x00000000,0x00050006,0x00000009,0x00000000,0x6f6c6f43,
        0x00000072,0x00040006,0x00000009,0x00000001,0x00005655,0x00030005,0x0000000b,0x0074754f,
        0x00040005,0x0000000f,0x6c6f4361,0x0000726f,0x00030005,0x00000015,0x00565561,0x00060005,
        0x00000019,0x505f6c67,0x65567265,0x78657472,0x00000000,0x00060006,0x00000019,0x00000000,
        0x505f6c67,0x7469736f,0x006e6f69,0x00030005,0x0000001b,0x00000000,0x00040005,0x0000001c,
        0x736f5061,0x00000000,0x00060005,0x0000001e,0x73755075,0x6e6f4368,0x6e617473,0x00000074,
        0x00050006,0x0000001e,0x00000000,0x61635375,0x0000656c,0x00060006,0x0000001e,0x00000001,
        0x61725475,0x616c736e,0x00006574,0x00030005,0x00000020,0x00006370,0x00040047,0x0000000b,
        0x0000001e,0x00000000,0x00040047,0x0000000f,0x0000001e,0x00000002,0x00040047,0x00000015,
        0x0000001e,0x00000001,0x00050048,0x00000019,0x00000000,0x0000000b,0x00000000,0x00030047,
        0x00000019,0x00000002,0x00040047,0x0000001c,0x0000001e,0x00000000,0x00050048,0x0000001e,
        0x00000000,0x00000023,0x00000000,0x00050048,0x0000001e,0x00000001,0x00000023,0x00000008,
        0x00030047,0x0000001e,0x00000002,0x00020013,0x00000002,0x00030021,0x00000003,0x00000002,
        0x00030016,0x00000006,0x00000020,0x00040017,0x00000007,0x00000006,0x00000004,0x00040017,
        0x00000008,0x00000006,0x00000002,0x0004001e,0x00000009,0x00000007,0x00000008,0x00040020,
        0x0000000a,0x00000003,0x00000009,0x0004003b,0x0000000a,0x0000000b,0x00000003,0x00040015,
        0x0000000c,0x00000020,0x00000001,0x0004002b,0x0000000c,0x0000000d,0x00000000,0x00040020,
        0x0000000e,0x00000001,0x00000007,0x0004003b,0x0000000e,0x0000000f,0x00000001,0x00040020,
        0x00000011,0x00000003,0x00000007,0x0004002b,0x0000000c,0x00000013,0x00000001,0x00040020,
        0x00000014,0x00000001,0x00000008,0x0004003b,0x00000014,0x00000015,0x00000001,0x00040020,
        0x00000017,0x00000003,0x00000008,0x0003001e,0x00000019,0x00000007,0x00040020,0x0000001a,
        0x00000003,0x00000019,0x0004003b,0x0000001a,0x0000001b,0x00000003,0x0004003b,0x00000014,
        0x0000001c,0x00000001,0x0004001e,0x0000001e,0x00000008,0x00000008,0x00040020,0x0000001f,
        0x00000009,0x0000001e,0x0004003b,0x0000001f,0x00000020,0x00000009,0x00040020,0x00000021,
        0x00000009,0x00000008,0x0004002b,0x00000006,0x00000028,0x00000000,0x0004002b,0x00000006,
        0x00000029,0x3f800000,0x00050036,0x00000002,0x00000004,0x00000000,0x00000003,0x000200f8,
        0x00000005,0x0004003d,0x00000007,0x00000010,0x0000000f,0x00050041,0x00000011,0x00000012,
        0x0000000b,0x0000000d,0x0003003e,0x00000012,0x00000010,0x0004003d,0x00000008,0x00000016,
        0x00000015,0x00050041,0x00000017,0x00000018,0x0000000b,0x00000013,0x0003003e,0x00000018,
        0x00000016,0x0004003d,0x00000008,0x0000001d,0x0000001c,0x00050041,0x00000021,0x00000022,
        0x00000020,0x0000000d,0x0004003d,0x00000008,0x00000023,0x00000022,0x00050085,0x00000008,
        0x00000024,0x0000001d,0x00000023,0x00050041,0x00000021,0x00000025,0x00000020,0x00000013,
        0x0004003d,0x00000008,0x00000026,0x00000025,0x00050081,0x00000008,0x00000027,0x00000024,
        0x00000026,0x00050051,0x00000006,0x0000002a,0x00000027,0x00000000,0x00050051,0x00000006,
        0x0000002b,0x00000027,0x00000001,0x00070050,0x00000007,0x0000002c,0x0000002a,0x0000002b,
        0x00000028,0x00000029,0x00050041,0x00000011,0x0000002d,0x0000001b,0x0000000d,0x0003003e,
        0x0000002d,0x0000002c,0x000100fd,0x00010038
    };


    // Fragment Shader
    // #version 450 core
    // layout(location = 0) out vec4 fColor;
    // layout(set = 0, binding = 0) uniform sampler2D sTexture;
    // layout(location = 0) in struct { vec4 Color; vec2 UV; } In;
    // void main()
    // {
    //     fColor = In.Color * texture(sTexture, In.UV.st);
    // }
    static constexpr uint32_t __glsl_fragment_shader_spirv[] =
    {
        0x07230203,0x00010000,0x00080001,0x0000001e,0x00000000,0x00020011,0x00000001,0x0006000b,
        0x00000001,0x4c534c47,0x6474732e,0x3035342e,0x00000000,0x0003000e,0x00000000,0x00000001,
        0x0007000f,0x00000004,0x00000004,0x6e69616d,0x00000000,0x00000009,0x0000000d,0x00030010,
        0x00000004,0x00000007,0x00030003,0x00000002,0x000001c2,0x00040005,0x00000004,0x6e69616d,
        0x00000000,0x00040005,0x00000009,0x6c6f4366,0x0000726f,0x00030005,0x0000000b,0x00000000,
        0x00050006,0x0000000b,0x00000000,0x6f6c6f43,0x00000072,0x00040006,0x0000000b,0x00000001,
        0x00005655,0x00030005,0x0000000d,0x00006e49,0x00050005,0x00000016,0x78655473,0x65727574,
        0x00000000,0x00040047,0x00000009,0x0000001e,0x00000000,0x00040047,0x0000000d,0x0000001e,
        0x00000000,0x00040047,0x00000016,0x00000022,0x00000000,0x00040047,0x00000016,0x00000021,
        0x00000000,0x00020013,0x00000002,0x00030021,0x00000003,0x00000002,0x00030016,0x00000006,
        0x00000020,0x00040017,0x00000007,0x00000006,0x00000004,0x00040020,0x00000008,0x00000003,
        0x00000007,0x0004003b,0x00000008,0x00000009,0x00000003,0x00040017,0x0000000a,0x00000006,
        0x00000002,0x0004001e,0x0000000b,0x00000007,0x0000000a,0x00040020,0x0000000c,0x00000001,
        0x0000000b,0x0004003b,0x0000000c,0x0000000d,0x00000001,0x00040015,0x0000000e,0x00000020,
        0x00000001,0x0004002b,0x0000000e,0x0000000f,0x00000000,0x00040020,0x00000010,0x00000001,
        0x00000007,0x00090019,0x00000013,0x00000006,0x00000001,0x00000000,0x00000000,0x00000000,
        0x00000001,0x00000000,0x0003001b,0x00000014,0x00000013,0x00040020,0x00000015,0x00000000,
        0x00000014,0x0004003b,0x00000015,0x00000016,0x00000000,0x0004002b,0x0000000e,0x00000018,
        0x00000001,0x00040020,0x00000019,0x00000001,0x0000000a,0x00050036,0x00000002,0x00000004,
        0x00000000,0x00000003,0x000200f8,0x00000005,0x00050041,0x00000010,0x00000011,0x0000000d,
        0x0000000f,0x0004003d,0x00000007,0x00000012,0x00000011,0x0004003d,0x00000014,0x00000017,
        0x00000016,0x00050041,0x00000019,0x0000001a,0x0000000d,0x00000018,0x0004003d,0x0000000a,
        0x0000001b,0x0000001a,0x00050057,0x00000007,0x0000001c,0x00000017,0x0000001b,0x00050085,
        0x00000007,0x0000001d,0x00000012,0x0000001c,0x0003003e,0x00000009,0x0000001d,0x000100fd,
        0x00010038
    };

    // struct forward declarations
    struct VulkanData;
    struct VulkanViewportData;


    // function forward declarations
    static void         VulkanCreateOrResizeBuffer(VkBuffer& buffer, VkDeviceMemory& buffer_memory, VkDeviceSize& buffer_size, size_t new_size, VkBufferUsageFlagBits usage_flag_bits);
    static void         VulkanCreateDescriptorSetLayout(VkDevice device, const VkAllocationCallbacks* allocator);
    static bool         VulkanCreateDeviceObjects();
    static void         VulkanCreateFontSampler(VkDevice device, const VkAllocationCallbacks* allocator);
    static void         VulkanCreatePipeline(VkDevice device, const VkAllocationCallbacks* allocator, VkPipelineCache pipeline_cache, VkRenderPass render_pass, VkSampleCountFlagBits msaa_samples, VkPipeline* pipeline, uint32_t subpass);
    static void         VulkanCreatePipelineLayout(VkDevice device, const VkAllocationCallbacks* allocator);
    static void         VulkanCreateShaderModule(VkDevice device, const VkAllocationCallbacks* allocator);
    static void         VulkanCreateWindow(ImGuiViewport* viewport);
    static void         VulkanCreateWindowCommandBuffers(VkPhysicalDevice physical_device, VkDevice device, VulkanWindow* window, uint32_t queue_family, const VkAllocationCallbacks* allocator);
    static void         VulkanCreateWindowSwapchain(VkPhysicalDevice physical_device, VkDevice device, VulkanWindow* window, const VkAllocationCallbacks* allocator, int window_height, int window_width, uint32_t min_image_count);
    static void         VulkanDestroyAllViewportsRenderBuffers(VkDevice device, const VkAllocationCallbacks* allocator);
    static void         VulkanDestroyDeviceObjects();
    static void         VulkanDestroyFrame(VkDevice device, VulkanFrame* frame, const VkAllocationCallbacks* allocator);
    static void         VulkanDestroyFrameRenderBuffers(VkDevice device, VulkanFrameRenderBuffers* framer_render_buffers, const VkAllocationCallbacks* allocator);
    static void         VulkanDestroyFrameSemaphores(VkDevice device, VulkanFrameSemaphores* frame_render_semaphore, const VkAllocationCallbacks* allocator);
    static void         VulkanDestroyWindow(ImGuiViewport* viewport);
    static void         VulkanDestroyWindow(VkInstance instance, VkDevice device, VulkanWindow* window, const VkAllocationCallbacks* allocator);
    static void         VulkanDestroyWindowRenderBuffers(VkDevice device, VulkanWindowRenderBuffers* window_render_buffers, const VkAllocationCallbacks* allocator);
    static VulkanData* VulkanGetBackendData();
    static void         VulkanInitPlatformInterface();
    static uint32_t     VulkanMemoryType(VkMemoryPropertyFlags property_flags, uint32_t type_bits);
    static void         VulkanRenderWindow(ImGuiViewport* viewport, void*);
    static void         VulkanSetWindowSize(ImGuiViewport* viewport, ImVec2 size);
    static void         VulkanSetupRenderState(ImDrawData* draw_data, VkPipeline pipeline, VkCommandBuffer command_buffer, VulkanFrameRenderBuffers* frame_render_buffers, int viewport_width, int viewport_height);
    static void         VulkanShutdownPlatformInterface();
    static void         VulkanSwapBuffers(ImGuiViewport* viewport, void*);

    // Vulkan struct implementations
    struct VulkanData
    {
        VulkanInitInfo          vulkan_init_info;
        VkRenderPass            render_pass;
        VkDeviceSize            buffer_memory_alignment;
        VkPipelineCreateFlags   pipeline_create_flags;
        VkDescriptorSetLayout   descriptor_set_layout;
        VkPipelineLayout        pipeline_layout;
        VkDescriptorSet         descriptor_set;
        VkPipeline              pipeline;
        uint32_t                subpass;
        VkShaderModule          vertex_shader_module;
        VkShaderModule          fragment_shader_module;

        VkSampler               font_sampler;
        VkDeviceMemory          font_memory;
        VkImage                 font_image;
        VkImageView             font_view;
        VkDeviceMemory          upload_buffer_memory;
        VkBuffer                upload_buffer;

        VulkanWindowRenderBuffers main_window_render_buffer;

        VulkanData()
        {
            memset(this, 0, sizeof(*this));
            buffer_memory_alignment = 256;
        }
    };

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    struct VulkanViewportData
    {
        bool                        b_window_owned;
        VulkanWindow                window;
        VulkanWindowRenderBuffers   window_render_buffers;

        VulkanViewportData()
            : b_window_owned{ false }, window{}
        {
            memset(&window_render_buffers, 0, sizeof(window_render_buffers));
        }

        ~VulkanViewportData() {}
    };

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....


    static void VkCheck(VkResult err)
    {
        VulkanData* backend_data = VulkanGetBackendData();
        if (!backend_data)
            return;
        VulkanInitInfo* vulkan_init_info = &backend_data->vulkan_init_info;
        if (vulkan_init_info->VkCheckResults)
            vulkan_init_info->VkCheckResults(err);
    };

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....


    // Implementation of header file functions
    bool VulkanCreateFontsTexture(VkCommandBuffer command_buffer)
    {
        ImGuiIO& io = ImGui::GetIO();
        VulkanData* backend_data = VulkanGetBackendData();
        VulkanInitInfo* vulkan_init_info = &backend_data->vulkan_init_info;

        unsigned char* pixels;
        int width{ 0 };
        int height{ 0 };
        io.Fonts->GetTexDataAsRGBA32(&pixels, &width, &height);
        size_t upload_size = width * height * 4 * sizeof(char);

        // Create the Image:
        {
            VkImageCreateInfo info = {};
            info.sType          = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
            info.imageType      = VK_IMAGE_TYPE_2D;
            info.format         = VK_FORMAT_R8G8B8A8_UNORM;
            info.extent.width   = width;
            info.extent.height  = height;
            info.extent.depth   = 1;
            info.mipLevels      = 1;
            info.arrayLayers    = 1;
            info.samples        = VK_SAMPLE_COUNT_1_BIT;
            info.tiling         = VK_IMAGE_TILING_OPTIMAL;
            info.usage          = VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;
            info.sharingMode    = VK_SHARING_MODE_EXCLUSIVE;
            info.initialLayout  = VK_IMAGE_LAYOUT_UNDEFINED;
            VkCheck(vkCreateImage(vulkan_init_info->device, &info, vulkan_init_info->allocator, &backend_data->font_image));
            
            VkMemoryRequirements req;
            vkGetImageMemoryRequirements(vulkan_init_info->device, backend_data->font_image, &req);
            
            VkMemoryAllocateInfo alloc_info = {};
            alloc_info.sType            = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
            alloc_info.allocationSize   = req.size;
            alloc_info.memoryTypeIndex  = VulkanMemoryType(VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, req.memoryTypeBits);
            VkCheck(vkAllocateMemory(vulkan_init_info->device, &alloc_info, vulkan_init_info->allocator, &backend_data->font_memory));
            VkCheck(vkBindImageMemory(vulkan_init_info->device, backend_data->font_image, backend_data->font_memory, 0));
        }

        // Create the Image View:
        {
            VkImageViewCreateInfo info = {};
            info.sType                          = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
            info.image                          = backend_data->font_image;
            info.viewType                       = VK_IMAGE_VIEW_TYPE_2D;
            info.format                         = VK_FORMAT_R8G8B8A8_UNORM;
            info.subresourceRange.aspectMask    = VK_IMAGE_ASPECT_COLOR_BIT;
            info.subresourceRange.levelCount    = 1;
            info.subresourceRange.layerCount    = 1;
            VkCheck(vkCreateImageView(vulkan_init_info->device, &info, vulkan_init_info->allocator, &backend_data->font_view));
        }

        // Update the Descriptor Set:
        {
            VkDescriptorImageInfo desc_image[1] = {};
            desc_image[0].sampler       = backend_data->font_sampler;
            desc_image[0].imageView     = backend_data->font_view;
            desc_image[0].imageLayout   = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
            
            VkWriteDescriptorSet write_desc[1] = {};
            write_desc[0].sType             = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
            write_desc[0].dstSet            = backend_data->descriptor_set;
            write_desc[0].descriptorCount   = 1;
            write_desc[0].descriptorType    = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
            write_desc[0].pImageInfo        = desc_image;
            vkUpdateDescriptorSets(vulkan_init_info->device, 1, write_desc, 0, NULL);
        }

        // Create the Upload Buffer:
        {
            VkBufferCreateInfo buffer_info = {};
            buffer_info.sType       = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
            buffer_info.size        = upload_size;
            buffer_info.usage       = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
            buffer_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
            VkCheck(vkCreateBuffer(vulkan_init_info->device, &buffer_info, vulkan_init_info->allocator, &backend_data->upload_buffer));
            
            VkMemoryRequirements req;
            vkGetBufferMemoryRequirements(vulkan_init_info->device, backend_data->upload_buffer, &req);
            backend_data->buffer_memory_alignment = (backend_data->buffer_memory_alignment > req.alignment) ? backend_data->buffer_memory_alignment : req.alignment;
            
            VkMemoryAllocateInfo alloc_info = {};
            alloc_info.sType            = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
            alloc_info.allocationSize   = req.size;
            alloc_info.memoryTypeIndex  = VulkanMemoryType(VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, req.memoryTypeBits);
            VkCheck(vkAllocateMemory(vulkan_init_info->device, &alloc_info, vulkan_init_info->allocator, &backend_data->upload_buffer_memory));
            VkCheck(vkBindBufferMemory(vulkan_init_info->device, backend_data->upload_buffer, backend_data->upload_buffer_memory, 0));
        }

        // Upload to Buffer:
        {
            char* map = NULL;
            VkCheck(vkMapMemory(vulkan_init_info->device, backend_data->upload_buffer_memory, 0, upload_size, 0, reinterpret_cast<void**>(&map)));
            memcpy(map, pixels, upload_size);
            VkMappedMemoryRange range[1] = {};
            range[0].sType  = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
            range[0].memory = backend_data->upload_buffer_memory;
            range[0].size   = upload_size;
            VkCheck(vkFlushMappedMemoryRanges(vulkan_init_info->device, 1, range));
            vkUnmapMemory(vulkan_init_info->device, backend_data->upload_buffer_memory);
        }

        // Copy to Image:
        {
            VkImageMemoryBarrier copy_barrier[1] = {};
            copy_barrier[0].sType                       = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
            copy_barrier[0].dstAccessMask               = VK_ACCESS_TRANSFER_WRITE_BIT;
            copy_barrier[0].oldLayout                   = VK_IMAGE_LAYOUT_UNDEFINED;
            copy_barrier[0].newLayout                   = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
            copy_barrier[0].srcQueueFamilyIndex         = VK_QUEUE_FAMILY_IGNORED;
            copy_barrier[0].dstQueueFamilyIndex         = VK_QUEUE_FAMILY_IGNORED;
            copy_barrier[0].image                       = backend_data->font_image;
            copy_barrier[0].subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            copy_barrier[0].subresourceRange.levelCount = 1;
            copy_barrier[0].subresourceRange.layerCount = 1;
            vkCmdPipelineBarrier(command_buffer, VK_PIPELINE_STAGE_HOST_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, NULL, 0, NULL, 1, copy_barrier);

            VkBufferImageCopy region = {};
            region.imageSubresource.aspectMask  = VK_IMAGE_ASPECT_COLOR_BIT;
            region.imageSubresource.layerCount  = 1;
            region.imageExtent.width            = width;
            region.imageExtent.height           = height;
            region.imageExtent.depth            = 1;
            vkCmdCopyBufferToImage(command_buffer, backend_data->upload_buffer, backend_data->font_image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);

            VkImageMemoryBarrier use_barrier[1] = {};
            use_barrier[0].sType                        = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
            use_barrier[0].srcAccessMask                = VK_ACCESS_TRANSFER_WRITE_BIT;
            use_barrier[0].dstAccessMask                = VK_ACCESS_SHADER_READ_BIT;
            use_barrier[0].oldLayout                    = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
            use_barrier[0].newLayout                    = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
            use_barrier[0].srcQueueFamilyIndex          = VK_QUEUE_FAMILY_IGNORED;
            use_barrier[0].dstQueueFamilyIndex          = VK_QUEUE_FAMILY_IGNORED;
            use_barrier[0].image                        = backend_data->font_image;
            use_barrier[0].subresourceRange.aspectMask  = VK_IMAGE_ASPECT_COLOR_BIT;
            use_barrier[0].subresourceRange.levelCount  = 1;
            use_barrier[0].subresourceRange.layerCount  = 1;
            vkCmdPipelineBarrier(command_buffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0, 0, NULL, 0, NULL, 1, use_barrier);
        }

        // Store our identifier
        io.Fonts->SetTexID(reinterpret_cast<ImTextureID>(reinterpret_cast<intptr_t>(backend_data->font_image)));

        return true;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void VulkanCreateOrResizeWindow(VulkanInitInfo* info, VulkanWindow* window, int width, int height)
    {
        VulkanCreateWindowSwapchain(info->physical_device, info->device, window, info->allocator, width, height, info->min_image_count);
        VulkanCreateWindowCommandBuffers(info->physical_device, info->device, window, info->queue_family, info->allocator);
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void VulkanDestroyFontUploadObjects()
    {
        VulkanData* backend_data = VulkanGetBackendData();
        VulkanInitInfo* info = &backend_data->vulkan_init_info;
        if (backend_data->upload_buffer)
        {
            vkDestroyBuffer(info->device, backend_data->upload_buffer, info->allocator);
            backend_data->upload_buffer = VK_NULL_HANDLE;
        }
        if (backend_data->upload_buffer_memory)
        {
            vkFreeMemory(info->device, backend_data->upload_buffer_memory, info->allocator);
            backend_data->upload_buffer_memory = VK_NULL_HANDLE;
        }
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void VulkanDestroyWindow(VulkanInitInfo* info, VulkanWindow* window)
    {
        vkDeviceWaitIdle(info->device);
        for (uint32_t i = 0; i < window->image_count; i++)
        {
            VulkanDestroyFrame(info->device, &window->frames[i], info->allocator);
            VulkanDestroyFrameSemaphores(info->device, &window->frame_semaphores[i], info->allocator);
        }
        IM_FREE(window->frames);
        IM_FREE(window->frame_semaphores);
        window->frames = NULL;
        window->frame_semaphores = NULL;
        vkDestroyPipeline(info->device, window->pipeline, info->allocator);
        vkDestroyRenderPass(info->device, window->render_pass, info->allocator);
        vkDestroySwapchainKHR(info->device, window->swap_chain, info->allocator);
        vkDestroySurfaceKHR(info->instance, window->surface, info->allocator);

        *window = VulkanWindow();
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    int VulkanGetMinImageCountFromPresentMode(VkPresentModeKHR present_mode)
    {
        if (present_mode == VK_PRESENT_MODE_MAILBOX_KHR)
            return 3;
        if (present_mode == VK_PRESENT_MODE_FIFO_KHR || present_mode == VK_PRESENT_MODE_FIFO_RELAXED_KHR)
            return 2;
        if (present_mode == VK_PRESENT_MODE_IMMEDIATE_KHR)
            return 1;
        IM_ASSERT(0);
        return 1;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    bool VulkanInit(VulkanInitInfo& info, VkRenderPass& render_pass)
    {
        ImGuiIO& io = ImGui::GetIO();
        IM_ASSERT(io.BackendRendererUserData == NULL && "Already initialized Vulkan backend!");

        VulkanData* backend_data = IM_NEW(VulkanData)();
        io.BackendRendererUserData = static_cast<void*>(backend_data);
        io.BackendRendererName = "ImGui_Vulkan_Backend";
        io.BackendFlags |= ImGuiBackendFlags_RendererHasVtxOffset;
        io.BackendFlags |= ImGuiBackendFlags_RendererHasViewports;

        IM_ASSERT(info.instance         != VK_NULL_HANDLE);
        IM_ASSERT(info.physical_device  != VK_NULL_HANDLE);
        IM_ASSERT(info.device           != VK_NULL_HANDLE);
        IM_ASSERT(info.queue            != VK_NULL_HANDLE);
        IM_ASSERT(info.descriptor_pool  != VK_NULL_HANDLE);
        IM_ASSERT(info.min_image_count  >= 2);
        IM_ASSERT(info.image_count      >= info.min_image_count);
        IM_ASSERT(render_pass           != VK_NULL_HANDLE);

        backend_data->vulkan_init_info  = info;
        backend_data->render_pass       = render_pass;
        backend_data->subpass           = info.subpass;

        VulkanCreateDeviceObjects();

        ImGuiViewport* main_viewport = ImGui::GetMainViewport();
        main_viewport->RendererUserData = IM_NEW(VulkanViewportData)();

        if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
            VulkanInitPlatformInterface();

        return true;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void VulkanNewFrame()
    {
        VulkanData* backend_data = VulkanGetBackendData();
        IM_ASSERT(backend_data != NULL && "Have you initiated ImGui Vulkan backend");
        IM_UNUSED(backend_data);
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void VulkanRenderDrawData(ImDrawData* draw_data, VkCommandBuffer command_buffer, VkPipeline pipeline /* = VK_NULL_HANDLE */)
    {
        // For clarity:
        //  - A viewport describes the transformation from normalized screen coordinates to 
        //      pixels for frame buffer
        //  - A scissor gives the area where you can render (this might be useful for 
        //      picture-in-picture)
        //  - A surface is the area of the viewport that the render pass can modify
        //  - A frame buffer contains the information the render pass will draw

        int frame_buffer_width = (int)(draw_data->DisplaySize.x * draw_data->FramebufferScale.x);
        int frame_buffer_height = (int)(draw_data->DisplaySize.y * draw_data->FramebufferScale.y);
        if (frame_buffer_width <= 0 || frame_buffer_height <= 0)
            return;

        VulkanData* backend_data = VulkanGetBackendData();
        VulkanInitInfo* vulkan_init_info = &backend_data->vulkan_init_info;
        if (pipeline == VK_NULL_HANDLE)
            pipeline = backend_data->pipeline;

        VulkanViewportData* viewport_renderer_data = static_cast<VulkanViewportData*>(draw_data->OwnerViewport->RendererUserData);
        IM_ASSERT(viewport_renderer_data != NULL);

        VulkanWindowRenderBuffers* window_render_buffers = &viewport_renderer_data->window_render_buffers;
        if (window_render_buffers->frame_render_buffers == NULL)
        {
            window_render_buffers->index                = 0;
            window_render_buffers->count                = vulkan_init_info->image_count;
            window_render_buffers->frame_render_buffers = static_cast<VulkanFrameRenderBuffers*>(IM_ALLOC(sizeof(VulkanFrameRenderBuffers) * window_render_buffers->count));
            memset(window_render_buffers->frame_render_buffers, 0, sizeof(VulkanFrameRenderBuffers) * window_render_buffers->count);
        }
        IM_ASSERT(window_render_buffers->count == vulkan_init_info->image_count);
        window_render_buffers->index = (window_render_buffers->index + 1) % window_render_buffers->count;

        VulkanFrameRenderBuffers* frame_render_buffer = &window_render_buffers->frame_render_buffers[window_render_buffers->index];

        if (draw_data->TotalVtxCount > 0)
        {
            size_t vertex_size = draw_data->TotalVtxCount * sizeof(ImDrawVert);
            size_t index_size = draw_data->TotalIdxCount * sizeof(ImDrawIdx);
            if (frame_render_buffer->vertex_buffer == VK_NULL_HANDLE || frame_render_buffer->vertex_buffer_size < vertex_size)
                VulkanCreateOrResizeBuffer(frame_render_buffer->vertex_buffer, frame_render_buffer->vertex_buffer_memory, frame_render_buffer->vertex_buffer_size, vertex_size, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT);
            if (frame_render_buffer->index_buffer == VK_NULL_HANDLE || frame_render_buffer->index_buffer_size < index_size)
                VulkanCreateOrResizeBuffer(frame_render_buffer->index_buffer, frame_render_buffer->index_buffer_memory, frame_render_buffer->index_buffer_size, index_size, VK_BUFFER_USAGE_INDEX_BUFFER_BIT);

            ImDrawVert* vertex_buffer_destination = NULL;
            ImDrawIdx* index_buffer_destination = NULL;
            VkCheck(vkMapMemory(vulkan_init_info->device, frame_render_buffer->vertex_buffer_memory, 0, frame_render_buffer->vertex_buffer_size, 0, reinterpret_cast<void**>(&vertex_buffer_destination)));;
            VkCheck(vkMapMemory(vulkan_init_info->device, frame_render_buffer->index_buffer_memory, 0, frame_render_buffer->index_buffer_size, 0, reinterpret_cast<void**>(&index_buffer_destination)));

            for (int n = 0; n < draw_data->CmdListsCount; n++)
            {
                const ImDrawList* cmd_list = draw_data->CmdLists[n];
                memcpy(vertex_buffer_destination, cmd_list->VtxBuffer.Data, cmd_list->VtxBuffer.Size * sizeof(ImDrawVert));
                memcpy(index_buffer_destination, cmd_list->IdxBuffer.Data, cmd_list->IdxBuffer.Size * sizeof(ImDrawIdx));
                vertex_buffer_destination += cmd_list->VtxBuffer.Size;
                index_buffer_destination += cmd_list->IdxBuffer.Size;
            }
            VkMappedMemoryRange range[2] = {};
            range[0].sType  = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
            range[0].memory = frame_render_buffer->vertex_buffer_memory;
            range[0].size   = VK_WHOLE_SIZE;
            range[1].sType  = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
            range[1].memory = frame_render_buffer->index_buffer_memory;
            range[1].size   = VK_WHOLE_SIZE;
            VkCheck(vkFlushMappedMemoryRanges(vulkan_init_info->device, 2, range));
            vkUnmapMemory(vulkan_init_info->device, frame_render_buffer->vertex_buffer_memory);
            vkUnmapMemory(vulkan_init_info->device, frame_render_buffer->index_buffer_memory);
        }

        VulkanSetupRenderState(draw_data, pipeline, command_buffer, frame_render_buffer, frame_buffer_width, frame_buffer_height);

        // Will project scissor/clipping rectangles into framebuffer space
        ImVec2 clip_off = draw_data->DisplayPos;         // (0,0) unless using multi-viewports
        ImVec2 clip_scale = draw_data->FramebufferScale; // (1,1) unless using retina display which are often (2,2)

        int global_vertex_buffer_offset = 0;
        int global_index_buffer_offset = 0;
        for (int n = 0; n < draw_data->CmdListsCount; n++)
        {
            const ImDrawList* draw_comman_list = draw_data->CmdLists[n];
            for (int cmd_i = 0; cmd_i < draw_comman_list->CmdBuffer.Size; cmd_i++)
            {
                const ImDrawCmd* draw_command = &draw_comman_list->CmdBuffer[cmd_i];
                if (draw_command->UserCallback != NULL)
                {
                    // (ImDrawCallback_ResetRenderState is a special callback value used by the user to request the renderer to reset render state.)
                    if (draw_command->UserCallback == ImDrawCallback_ResetRenderState)
                        VulkanSetupRenderState(draw_data, pipeline, command_buffer, frame_render_buffer, frame_buffer_width, frame_buffer_height);
                    else
                        draw_command->UserCallback(draw_comman_list, draw_command);
                }
                else
                {
                    // Project scissor/clipping rectangles into framebuffer space
                    ImVec2 clip_min((draw_command->ClipRect.x - clip_off.x) * clip_scale.x, (draw_command->ClipRect.y - clip_off.y) * clip_scale.y);
                    ImVec2 clip_max((draw_command->ClipRect.z - clip_off.x) * clip_scale.x, (draw_command->ClipRect.w - clip_off.y) * clip_scale.y);

                    // Clamp to viewport as vkCmdSetScissor() won't accept values that are off bounds
                    if (clip_min.x < 0.0f) { clip_min.x = 0.0f; }
                    if (clip_min.y < 0.0f) { clip_min.y = 0.0f; }
                    if (clip_max.x > frame_buffer_width) { clip_max.x = (float)frame_buffer_width; }
                    if (clip_max.y > frame_buffer_height) { clip_max.y = (float)frame_buffer_height; }
                    if (clip_max.x <= clip_min.x || clip_max.y <= clip_min.y)
                        continue;

                    // Apply scissor/clipping rectangle
                    VkRect2D scissor;
                    scissor.offset.x        = static_cast<int32_t>(clip_min.x);
                    scissor.offset.y        = static_cast<int32_t>(clip_min.y);
                    scissor.extent.width    = static_cast<uint32_t>(clip_max.x - clip_min.x);
                    scissor.extent.height   = static_cast<uint32_t>(clip_max.y - clip_min.y);
                    vkCmdSetScissor(command_buffer, 0, 1, &scissor);

                    // Draw
                    vkCmdDrawIndexed(command_buffer, draw_command->ElemCount, 1, draw_command->IdxOffset + global_index_buffer_offset, draw_command->VtxOffset + global_vertex_buffer_offset, 0);
                }
            }
            global_index_buffer_offset += draw_comman_list->IdxBuffer.Size;
            global_vertex_buffer_offset += draw_comman_list->VtxBuffer.Size;
        }

        VkRect2D scissor = { { 0, 0 }, { static_cast<uint32_t>(frame_buffer_width), static_cast<uint32_t>(frame_buffer_height) } };
        vkCmdSetScissor(command_buffer, 0, 1, &scissor);
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    VkPresentModeKHR VulkanSelectPresentMode(VkPhysicalDevice physical_device, VkSurfaceKHR surface, const VkPresentModeKHR* request_modes, int request_modes_counts)
    {
        IM_ASSERT(request_modes != NULL);
        IM_ASSERT(request_modes_counts > 0);

        uint32_t available_modes_count = 0;
        vkGetPhysicalDeviceSurfacePresentModesKHR(physical_device, surface, &available_modes_count, NULL);

        ImVector<VkPresentModeKHR> available_modes;
        available_modes.resize(static_cast<int>(available_modes_count));

        vkGetPhysicalDeviceSurfacePresentModesKHR(physical_device, surface, &available_modes_count, available_modes.Data);

        for (int request_i = 0; request_i < request_modes_counts; request_i++)
            for (uint32_t mode_i = 0; mode_i < available_modes_count; mode_i++)
                if (request_modes[request_i] == available_modes[mode_i])
                    return request_modes[request_i];

        return VK_PRESENT_MODE_FIFO_KHR; // Always available
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    VkSurfaceFormatKHR VulkanSelectSurfaceFormat(VkPhysicalDevice physical_device, VkSurfaceKHR surface, const VkFormat* request_formats, int request_formats_count, VkColorSpaceKHR request_color_space)
    {
        IM_ASSERT(request_formats != NULL);
        IM_ASSERT(request_formats_count > 0);

        // Spec Format and View Format are expected to be the same unless VK_IMAGE_CREATE_MUTABLE_BIT was set at image creation
        uint32_t available_formats_count;
        vkGetPhysicalDeviceSurfaceFormatsKHR(physical_device, surface, &available_formats_count, NULL);
        ImVector<VkSurfaceFormatKHR> available_formats;
        available_formats.resize(static_cast<int>(available_formats_count));
        vkGetPhysicalDeviceSurfaceFormatsKHR(physical_device, surface, &available_formats_count, available_formats.Data);

        if (available_formats_count == 1)
        {
            if (available_formats[0].format == VK_FORMAT_UNDEFINED)
            {
                VkSurfaceFormatKHR returned_format;
                returned_format.format      = request_formats[0];
                returned_format.colorSpace  = request_color_space;
                return returned_format;
            }
            else return available_formats[0];
        }
        else
        {
            // First requested format found will be used
            for (int request_i = 0; request_i < request_formats_count; request_i++)
                for (uint32_t format_i = 0; format_i < available_formats_count; format_i++)
                    if (available_formats[format_i].format == request_formats[request_i] && available_formats[format_i].colorSpace == request_color_space)
                        return available_formats[format_i];

            return available_formats[0];
        }
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void VulkanSetMinImageCount(uint32_t min_image_count)
    {
        VulkanData* backend_data = VulkanGetBackendData();
        IM_ASSERT(min_image_count >= 2);
        if (backend_data->vulkan_init_info.min_image_count == min_image_count)
            return;

        // TODO: Address the concern here
        IM_ASSERT(0); // FIXME-VIEWPORT: Unsupported. Need to recreate all swap chains!
        VulkanInitInfo* vulkan_init_info = &backend_data->vulkan_init_info;
        VkCheck(vkDeviceWaitIdle(vulkan_init_info->device));
        VulkanDestroyAllViewportsRenderBuffers(vulkan_init_info->device, vulkan_init_info->allocator);

        backend_data->vulkan_init_info.min_image_count = min_image_count;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void VulkanShutdown()
    {
        VulkanData* backend_data = VulkanGetBackendData();
        IM_ASSERT(backend_data != NULL && "No Vulkan backend to shutdown");
        ImGuiIO& io = ImGui::GetIO();

        VulkanDestroyDeviceObjects();

        ImGuiViewport* main_viewport = ImGui::GetMainViewport();
        if (VulkanViewportData* viewport_data = static_cast<VulkanViewportData*>(main_viewport->RendererUserData))
            IM_DELETE(viewport_data);
        main_viewport->RendererUserData = NULL;

        VulkanShutdownPlatformInterface();

        io.BackendRendererName      = NULL;
        io.BackendRendererUserData  = NULL;
        IM_DELETE(backend_data);
    }


    // Begin implementation of static functions

    static void VulkanCreateOrResizeBuffer(VkBuffer& buffer, VkDeviceMemory& buffer_memory, VkDeviceSize& buffer_size, size_t new_size, VkBufferUsageFlagBits usage_flag_bits)
    {
        VulkanData* backend_data = VulkanGetBackendData();
        VulkanInitInfo* vulkan_init_info = &backend_data->vulkan_init_info;
        if (buffer != VK_NULL_HANDLE)
            vkDestroyBuffer(vulkan_init_info->device, buffer, vulkan_init_info->allocator);
        if (buffer_memory != VK_NULL_HANDLE)
            vkFreeMemory(vulkan_init_info->device, buffer_memory, vulkan_init_info->allocator);

        VkDeviceSize vertex_buffer_size_aligned = ((new_size - 1) / backend_data->buffer_memory_alignment + 1) * backend_data->buffer_memory_alignment;
        
        VkBufferCreateInfo buffer_info = {};
        buffer_info.sType       = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        buffer_info.size        = vertex_buffer_size_aligned;
        buffer_info.usage       = usage_flag_bits;
        buffer_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        VkCheck(vkCreateBuffer(vulkan_init_info->device, &buffer_info, vulkan_init_info->allocator, &buffer));

        VkMemoryRequirements memory_requirements;
        vkGetBufferMemoryRequirements(vulkan_init_info->device, buffer, &memory_requirements);
        backend_data->buffer_memory_alignment = (backend_data->buffer_memory_alignment > memory_requirements.alignment) ? backend_data->buffer_memory_alignment : memory_requirements.alignment;
        
        VkMemoryAllocateInfo alloc_info = {};
        alloc_info.sType            = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
        alloc_info.allocationSize   = memory_requirements.size;
        alloc_info.memoryTypeIndex  = VulkanMemoryType(VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, memory_requirements.memoryTypeBits);
        VkCheck(vkAllocateMemory(vulkan_init_info->device, &alloc_info, vulkan_init_info->allocator, &buffer_memory));

        VkCheck(vkBindBufferMemory(vulkan_init_info->device, buffer, buffer_memory, 0));
        buffer_size = memory_requirements.size;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static void VulkanCreateDescriptorSetLayout(VkDevice device, const VkAllocationCallbacks* allocator)
    {
        VulkanData* backend_data = VulkanGetBackendData();
        if (backend_data->descriptor_set_layout)
            return;

        VulkanCreateFontSampler(device, allocator);
        VkSampler sampler[1] = { backend_data->font_sampler };
        VkDescriptorSetLayoutBinding binding[1] = {};
        binding[0].descriptorType       = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        binding[0].descriptorCount      = 1;
        binding[0].stageFlags           = VK_SHADER_STAGE_FRAGMENT_BIT;
        binding[0].pImmutableSamplers   = sampler;
        
        VkDescriptorSetLayoutCreateInfo info = {};
        info.sType          = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
        info.bindingCount   = 1;
        info.pBindings      = binding;
        VkCheck(vkCreateDescriptorSetLayout(device, &info, allocator, &backend_data->descriptor_set_layout));
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static bool VulkanCreateDeviceObjects()
    {
        VulkanData* backend_data = VulkanGetBackendData();
        VulkanInitInfo* vulkan_init_info = &backend_data->vulkan_init_info;

        if (!backend_data->font_sampler)
        {
            VkSamplerCreateInfo info = {};
            info.sType          = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
            info.magFilter      = VK_FILTER_LINEAR;
            info.minFilter      = VK_FILTER_LINEAR;
            info.mipmapMode     = VK_SAMPLER_MIPMAP_MODE_LINEAR;
            info.addressModeU   = VK_SAMPLER_ADDRESS_MODE_REPEAT;
            info.addressModeV   = VK_SAMPLER_ADDRESS_MODE_REPEAT;
            info.addressModeW   = VK_SAMPLER_ADDRESS_MODE_REPEAT;
            info.minLod         = -1000;
            info.maxLod         = 1000;
            info.maxAnisotropy  = 1.0f;
            VkCheck(vkCreateSampler(vulkan_init_info->device, &info, vulkan_init_info->allocator, &backend_data->font_sampler));
        }

        if (!backend_data->descriptor_set_layout)
        {
            VkSampler sampler[1] = { backend_data->font_sampler };
            VkDescriptorSetLayoutBinding binding[1] = {};
            binding[0].descriptorType       = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
            binding[0].descriptorCount      = 1;
            binding[0].stageFlags           = VK_SHADER_STAGE_FRAGMENT_BIT;
            binding[0].pImmutableSamplers   = sampler;
            
            VkDescriptorSetLayoutCreateInfo info = {};
            info.sType          = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
            info.bindingCount   = 1;
            info.pBindings      = binding;
            VkCheck(vkCreateDescriptorSetLayout(vulkan_init_info->device, &info, vulkan_init_info->allocator, &backend_data->descriptor_set_layout));
        }

        // Create Descriptor Set:
        {
            VkDescriptorSetAllocateInfo allocation_info = {};
            allocation_info.sType               = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
            allocation_info.descriptorPool      = vulkan_init_info->descriptor_pool;
            allocation_info.descriptorSetCount  = 1;
            allocation_info.pSetLayouts         = &backend_data->descriptor_set_layout;
            VkCheck(vkAllocateDescriptorSets(vulkan_init_info->device, &allocation_info, &backend_data->descriptor_set));
        }

        if (!backend_data->pipeline_layout)
        {
            VkPushConstantRange push_constants[1] = {};
            push_constants[0].stageFlags    = VK_SHADER_STAGE_VERTEX_BIT;
            push_constants[0].offset        = sizeof(float) * 0;
            push_constants[0].size          = sizeof(float) * 4;

            VkDescriptorSetLayout set_layout[1] = { backend_data->descriptor_set_layout };
            
            VkPipelineLayoutCreateInfo layout_info = {};
            layout_info.sType                   = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
            layout_info.setLayoutCount          = 1;
            layout_info.pSetLayouts             = set_layout;
            layout_info.pushConstantRangeCount  = 1;
            layout_info.pPushConstantRanges     = push_constants;
            VkCheck(vkCreatePipelineLayout(vulkan_init_info->device, &layout_info, vulkan_init_info->allocator, &backend_data->pipeline_layout));
        }

        VulkanCreatePipeline(vulkan_init_info->device, vulkan_init_info->allocator, vulkan_init_info->pipeline_cache, backend_data->render_pass, vulkan_init_info->MSAA_samples, &backend_data->pipeline, backend_data->subpass);

        return true;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static void VulkanCreateFontSampler(VkDevice device, const VkAllocationCallbacks* allocator)
    {
        VulkanData* backend_data = VulkanGetBackendData();
        if (backend_data->font_sampler)
            return;

        VkSamplerCreateInfo info = {};
        info.sType          = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
        info.magFilter      = VK_FILTER_LINEAR;
        info.minFilter      = VK_FILTER_LINEAR;
        info.mipmapMode     = VK_SAMPLER_MIPMAP_MODE_LINEAR;
        info.addressModeU   = VK_SAMPLER_ADDRESS_MODE_REPEAT;
        info.addressModeV   = VK_SAMPLER_ADDRESS_MODE_REPEAT;
        info.addressModeW   = VK_SAMPLER_ADDRESS_MODE_REPEAT;
        info.minLod         = -1000;
        info.maxLod         = 1000;
        info.maxAnisotropy  = 1.0f;
        VkCheck(vkCreateSampler(device, &info, allocator, &backend_data->font_sampler));
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static void VulkanCreatePipeline(VkDevice device, const VkAllocationCallbacks* allocator, VkPipelineCache pipeline_cache, VkRenderPass render_pass, VkSampleCountFlagBits msaa_samples, VkPipeline* pipeline, uint32_t subpass)
    {
        VulkanData* backend_data = VulkanGetBackendData();
        VulkanCreateShaderModule(device, allocator);

        VkPipelineShaderStageCreateInfo stage[2] = {};
        stage[0].sType  = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        stage[0].stage  = VK_SHADER_STAGE_VERTEX_BIT;
        stage[0].module = backend_data->vertex_shader_module;
        stage[0].pName  = "main";
        stage[1].sType  = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        stage[1].stage  = VK_SHADER_STAGE_FRAGMENT_BIT;
        stage[1].module = backend_data->fragment_shader_module;
        stage[1].pName  = "main";

        VkVertexInputBindingDescription vertex_binding_description[1] = {};
        vertex_binding_description[0].stride    = sizeof(ImDrawVert);
        vertex_binding_description[0].inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

        VkVertexInputAttributeDescription attribute_description[3] = {};
        attribute_description[0].location   = 0;
        attribute_description[0].binding    = vertex_binding_description[0].binding;
        attribute_description[0].format     = VK_FORMAT_R32G32_SFLOAT;
        attribute_description[0].offset     = IM_OFFSETOF(ImDrawVert, pos);
        attribute_description[1].location   = 1;
        attribute_description[1].binding    = vertex_binding_description[0].binding;
        attribute_description[1].format     = VK_FORMAT_R32G32_SFLOAT;
        attribute_description[1].offset     = IM_OFFSETOF(ImDrawVert, uv);
        attribute_description[2].location   = 2;
        attribute_description[2].binding    = vertex_binding_description[0].binding;
        attribute_description[2].format     = VK_FORMAT_R8G8B8A8_UNORM;
        attribute_description[2].offset     = IM_OFFSETOF(ImDrawVert, col);

        VkPipelineVertexInputStateCreateInfo vertex_input_info = {};
        vertex_input_info.sType                             = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
        vertex_input_info.vertexBindingDescriptionCount     = 1;
        vertex_input_info.pVertexBindingDescriptions        = vertex_binding_description;
        vertex_input_info.vertexAttributeDescriptionCount   = 3;
        vertex_input_info.pVertexAttributeDescriptions      = attribute_description;

        VkPipelineInputAssemblyStateCreateInfo input_assembly_state_info = {};
        input_assembly_state_info.sType     = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
        input_assembly_state_info.topology  = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;

        VkPipelineViewportStateCreateInfo viewport_info = {};
        viewport_info.sType         = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
        viewport_info.viewportCount = 1;
        viewport_info.scissorCount  = 1;

        VkPipelineRasterizationStateCreateInfo rasterization_info = {};
        rasterization_info.sType        = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
        rasterization_info.polygonMode  = VK_POLYGON_MODE_FILL;
        rasterization_info.cullMode     = VK_CULL_MODE_NONE;
        rasterization_info.frontFace    = VK_FRONT_FACE_COUNTER_CLOCKWISE;
        rasterization_info.lineWidth    = 1.0f;

        VkPipelineMultisampleStateCreateInfo multisample_state_info = {};
        multisample_state_info.sType                = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
        multisample_state_info.rasterizationSamples = (msaa_samples != 0) ? msaa_samples : VK_SAMPLE_COUNT_1_BIT;

        VkPipelineColorBlendAttachmentState color_attachment[1] = {};
        color_attachment[0].blendEnable         = VK_TRUE;
        color_attachment[0].srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
        color_attachment[0].dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
        color_attachment[0].colorBlendOp        = VK_BLEND_OP_ADD;
        color_attachment[0].srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
        color_attachment[0].dstAlphaBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
        color_attachment[0].alphaBlendOp        = VK_BLEND_OP_ADD;
        color_attachment[0].colorWriteMask      = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;

        VkPipelineDepthStencilStateCreateInfo depth_stencil_info = {};
        depth_stencil_info.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;

        VkPipelineColorBlendStateCreateInfo blend_info = {};
        blend_info.sType            = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
        blend_info.attachmentCount  = 1;
        blend_info.pAttachments     = color_attachment;

        VkDynamicState dynamic_states[2] = { VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_SCISSOR };
        VkPipelineDynamicStateCreateInfo dynamic_state = {};
        dynamic_state.sType             = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
        dynamic_state.dynamicStateCount = static_cast<uint32_t>(IM_ARRAYSIZE(dynamic_states));
        dynamic_state.pDynamicStates    = dynamic_states;

        VulkanCreatePipelineLayout(device, allocator);

        VkGraphicsPipelineCreateInfo info = {};
        info.sType                  = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
        info.flags                  = backend_data->pipeline_create_flags;
        info.stageCount             = 2;
        info.pStages                = stage;
        info.pVertexInputState      = &vertex_input_info;
        info.pInputAssemblyState    = &input_assembly_state_info;
        info.pViewportState         = &viewport_info;
        info.pRasterizationState    = &rasterization_info;
        info.pMultisampleState      = &multisample_state_info;
        info.pDepthStencilState     = &depth_stencil_info;
        info.pColorBlendState       = &blend_info;
        info.pDynamicState          = &dynamic_state;
        info.layout                 = backend_data->pipeline_layout;
        info.renderPass             = render_pass;
        info.subpass                = subpass; 
        VkCheck(vkCreateGraphicsPipelines(device, pipeline_cache, 1, &info, allocator, pipeline));
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static void VulkanCreatePipelineLayout(VkDevice device, const VkAllocationCallbacks* allocator)
    {
        VulkanData* backend_data = VulkanGetBackendData();
        if (backend_data->pipeline_layout)
            return;

        // Constants: we are using 'vec2 offset' and 'vec2 scale' instead of a full 3d projection matrix
        VulkanCreateDescriptorSetLayout(device, allocator);
        VkPushConstantRange push_constants[1] = {};
        push_constants[0].stageFlags    = VK_SHADER_STAGE_VERTEX_BIT;
        push_constants[0].offset        = sizeof(float) * 0;
        push_constants[0].size          = sizeof(float) * 4;

        VkDescriptorSetLayout set_layout[1] = { backend_data->descriptor_set_layout };
        VkPipelineLayoutCreateInfo layout_info = {};
        layout_info.sType                   = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        layout_info.setLayoutCount          = 1;
        layout_info.pSetLayouts             = set_layout;
        layout_info.pushConstantRangeCount  = 1;
        layout_info.pPushConstantRanges     = push_constants;
        VkCheck(vkCreatePipelineLayout(device, &layout_info, allocator, &backend_data->pipeline_layout));
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static void VulkanCreateShaderModule(VkDevice device, const VkAllocationCallbacks* allocator)
    {
        VulkanData* backend_data = VulkanGetBackendData();
        if (backend_data->vertex_shader_module == VK_NULL_HANDLE)
        {
            VkShaderModuleCreateInfo vertex_shader_info = {};
            vertex_shader_info.sType    = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
            vertex_shader_info.codeSize = sizeof(__glsl_vertex_shader_spirv);
            vertex_shader_info.pCode    = const_cast<uint32_t*>(__glsl_vertex_shader_spirv);
            VkCheck(vkCreateShaderModule(device, &vertex_shader_info, allocator, &backend_data->vertex_shader_module));
        }
        if (backend_data->fragment_shader_module == VK_NULL_HANDLE)
        {
            VkShaderModuleCreateInfo fragment_shader_info = {};
            fragment_shader_info.sType      = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
            fragment_shader_info.codeSize   = sizeof(__glsl_fragment_shader_spirv);
            fragment_shader_info.pCode      = const_cast<uint32_t*>(__glsl_fragment_shader_spirv);
            VkCheck(vkCreateShaderModule(device, &fragment_shader_info, allocator, &backend_data->fragment_shader_module));
        }
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static void VulkanCreateWindow(ImGuiViewport* viewport)
    {
        VulkanData* backend_data = VulkanGetBackendData();
        VulkanInitInfo* vulkan_init_info = &backend_data->vulkan_init_info;
        VulkanViewportData* viewport_data = IM_NEW(VulkanViewportData)();
        VulkanWindow* window = &viewport_data->window;

        viewport->RendererUserData = viewport_data;

        ImGuiPlatformIO& platform_io = ImGui::GetPlatformIO();
        VkCheck(static_cast<VkResult>(platform_io.Platform_CreateVkSurface(
            viewport,
            reinterpret_cast<ImU64>(vulkan_init_info->instance),
            reinterpret_cast<const void*>(vulkan_init_info->allocator),
            reinterpret_cast<ImU64*>(&window->surface)
        )));

        VkBool32 b_wsi_support;
        vkGetPhysicalDeviceSurfaceSupportKHR(vulkan_init_info->physical_device, vulkan_init_info->queue_family, window->surface, &b_wsi_support);
        if (b_wsi_support != VK_TRUE)
        {
            IM_ASSERT(0); // Error: no WSI support on physical device
            return;
        }

        const VkFormat request_surface_image_format[] = { VK_FORMAT_B8G8R8A8_UNORM, VK_FORMAT_R8G8B8A8_UNORM, VK_FORMAT_B8G8R8_UNORM, VK_FORMAT_R8G8B8_UNORM };
        const VkColorSpaceKHR request_surface_color_space = VK_COLORSPACE_SRGB_NONLINEAR_KHR;
        window->surface_format = VulkanSelectSurfaceFormat(vulkan_init_info->physical_device, window->surface, request_surface_image_format, static_cast<size_t>(IM_ARRAYSIZE(request_surface_image_format)), request_surface_color_space);

        // FIXME-VULKAN: Even though mailbox seems to get us maximum framerate with a single window, it halves framerate with a second window etc. (w/ Nvidia and SDK 1.82.1)
        VkPresentModeKHR present_modes[] = { VK_PRESENT_MODE_MAILBOX_KHR, VK_PRESENT_MODE_IMMEDIATE_KHR, VK_PRESENT_MODE_FIFO_KHR };
        window->present_mode = VulkanSelectPresentMode(vulkan_init_info->physical_device, window->surface, &present_modes[0], IM_ARRAYSIZE(present_modes));

        window->b_clear_enable = (viewport->Flags & ImGuiViewportFlags_NoRendererClear) ? false : true;
        VulkanCreateOrResizeWindow(vulkan_init_info, window, static_cast<int>(viewport->Size.x), static_cast<int>(viewport->Size.y));
        viewport_data->b_window_owned = true;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static void VulkanCreateWindowCommandBuffers(VkPhysicalDevice physical_device, VkDevice device, VulkanWindow* window, uint32_t queue_family, const VkAllocationCallbacks* allocator)
    {
        IM_ASSERT(physical_device != VK_NULL_HANDLE && device != VK_NULL_HANDLE);
        (void)physical_device;
        (void)allocator;

        // Create Command Buffers
        VkResult err;
        for (uint32_t i = 0; i < window->image_count; i++)
        {
            VulkanFrame* frame_data = &window->frames[i];
            VulkanFrameSemaphores* frame_semaphore_data = &window->frame_semaphores[i];
            {
                VkCommandPoolCreateInfo info = {};
                info.sType              = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
                info.flags              = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
                info.queueFamilyIndex   = queue_family;
                VkCheck(vkCreateCommandPool(device, &info, allocator, &frame_data->command_pool));
            }
            {
                VkCommandBufferAllocateInfo info = {};
                info.sType              = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
                info.commandPool        = frame_data->command_pool;
                info.level              = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
                info.commandBufferCount = 1;
                VkCheck(vkAllocateCommandBuffers(device, &info, &frame_data->command_buffer));
            }
            {
                VkFenceCreateInfo info = {};
                info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
                info.flags = VK_FENCE_CREATE_SIGNALED_BIT;
                VkCheck(vkCreateFence(device, &info, allocator, &frame_data->fence));
            }
            {
                VkSemaphoreCreateInfo info = {};
                info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
                VkCheck(vkCreateSemaphore(device, &info, allocator, &frame_semaphore_data->image_acquired_semaphore));
                VkCheck(vkCreateSemaphore(device, &info, allocator, &frame_semaphore_data->render_completed_semaphore));
            }
        }
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static void VulkanCreateWindowSwapchain(VkPhysicalDevice physical_device, VkDevice device, VulkanWindow* window, const VkAllocationCallbacks* allocator, int window_height, int window_width, uint32_t min_image_count)
    {
        VkSwapchainKHR old_swapchain = window->swap_chain;
        window->swap_chain = VK_NULL_HANDLE;
        VkCheck(vkDeviceWaitIdle(device));


        for (uint32_t i = 0; i < window->image_count; i++)
        {
            VulkanDestroyFrame(device, &window->frames[i], allocator);
            VulkanDestroyFrameSemaphores(device, &window->frame_semaphores[i], allocator);
        }
        IM_FREE(window->frames);
        IM_FREE(window->frame_semaphores);
        window->frames              = NULL;
        window->frame_semaphores    = NULL;
        window->image_count         = 0;

        if (window->render_pass)
            vkDestroyRenderPass(device, window->render_pass, allocator);
        if (window->pipeline)
            vkDestroyPipeline(device, window->pipeline, allocator);

        if (min_image_count == 0)
            min_image_count = VulkanGetMinImageCountFromPresentMode(window->present_mode);

        // Create Swapchain
        {
            VkSwapchainCreateInfoKHR info = {};
            info.sType              = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
            info.surface            = window->surface;
            info.minImageCount      = min_image_count;
            info.imageFormat        = window->surface_format.format;
            info.imageColorSpace    = window->surface_format.colorSpace;
            info.imageArrayLayers   = 1;
            info.imageUsage         = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
            info.imageSharingMode   = VK_SHARING_MODE_EXCLUSIVE;
            info.preTransform       = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
            info.compositeAlpha     = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
            info.presentMode        = window->present_mode;
            info.clipped            = VK_TRUE;
            info.oldSwapchain       = old_swapchain;

            VkSurfaceCapabilitiesKHR cap;
            VkCheck(vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physical_device, window->surface, &cap));

            if (info.minImageCount < cap.minImageCount)
                info.minImageCount = cap.minImageCount;
            else if (cap.maxImageCount != 0 && info.minImageCount > cap.maxImageCount)
                info.minImageCount = cap.maxImageCount;

            if (cap.currentExtent.width == 0xffffffff)
            {
                info.imageExtent.width  = window->width = window_width;
                info.imageExtent.height = window->height = window_height;
            }
            else
            {
                info.imageExtent.width  = window->width = cap.currentExtent.width;
                info.imageExtent.height = window->height = cap.currentExtent.height;
            }
            VkCheck(vkCreateSwapchainKHR(device, &info, allocator, &window->swap_chain));
            VkCheck(vkGetSwapchainImagesKHR(device, window->swap_chain, &window->image_count, NULL));

            VkImage backbuffers[16] = {};
            IM_ASSERT(window->image_count >= min_image_count);
            IM_ASSERT(window->image_count < IM_ARRAYSIZE(backbuffers));
            VkCheck(vkGetSwapchainImagesKHR(device, window->swap_chain, &window->image_count, backbuffers));

            IM_ASSERT(window->frames == NULL);
            window->frames              = static_cast<VulkanFrame*>(IM_ALLOC(sizeof(VulkanFrame) * window->image_count));
            window->frame_semaphores    = static_cast<VulkanFrameSemaphores*>(IM_ALLOC(sizeof(VulkanFrameSemaphores) * window->image_count));
            memset(window->frames, 0, sizeof(window->frames[0]) * window->image_count);
            memset(window->frame_semaphores, 0, sizeof(window->frame_semaphores[0]) * window->image_count);
            for (uint32_t i = 0; i < window->image_count; i++)
                window->frames[i].back_buffer = backbuffers[i];
        }
        if (old_swapchain)
            vkDestroySwapchainKHR(device, old_swapchain, allocator);

        // Create the Render Pass
        {
            VkAttachmentDescription attachment = {};
            attachment.format           = window->surface_format.format;
            attachment.samples          = VK_SAMPLE_COUNT_1_BIT;
            attachment.loadOp           = window->b_clear_enable ? VK_ATTACHMENT_LOAD_OP_CLEAR : VK_ATTACHMENT_LOAD_OP_DONT_CARE;
            attachment.storeOp          = VK_ATTACHMENT_STORE_OP_STORE;
            attachment.stencilLoadOp    = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
            attachment.stencilStoreOp   = VK_ATTACHMENT_STORE_OP_DONT_CARE;
            attachment.initialLayout    = VK_IMAGE_LAYOUT_UNDEFINED;
            attachment.finalLayout      = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

            VkAttachmentReference color_attachment = {};
            color_attachment.attachment = 0;
            color_attachment.layout     = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

            VkSubpassDescription subpass_description = {};
            subpass_description.pipelineBindPoint       = VK_PIPELINE_BIND_POINT_GRAPHICS;
            subpass_description.colorAttachmentCount    = 1;
            subpass_description.pColorAttachments       = &color_attachment;

            VkSubpassDependency subpass_dependency = {};
            subpass_dependency.srcSubpass       = VK_SUBPASS_EXTERNAL;
            subpass_dependency.dstSubpass       = 0;
            subpass_dependency.srcStageMask     = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
            subpass_dependency.dstStageMask     = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
            subpass_dependency.srcAccessMask    = 0;
            subpass_dependency.dstAccessMask    = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

            VkRenderPassCreateInfo info = {};
            info.sType              = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
            info.attachmentCount    = 1;
            info.pAttachments       = &attachment;
            info.subpassCount       = 1;
            info.pSubpasses         = &subpass_description;
            info.dependencyCount    = 1;
            info.pDependencies      = &subpass_dependency;
            VkCheck(vkCreateRenderPass(device, &info, allocator, &window->render_pass));
        }

        // Create image views
        {
            VkImageViewCreateInfo info = {};
            info.sType          = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
            info.viewType       = VK_IMAGE_VIEW_TYPE_2D;
            info.format         = window->surface_format.format;
            info.components.r   = VK_COMPONENT_SWIZZLE_R;
            info.components.g   = VK_COMPONENT_SWIZZLE_G;
            info.components.b   = VK_COMPONENT_SWIZZLE_B;
            info.components.a   = VK_COMPONENT_SWIZZLE_A;

            VkImageSubresourceRange image_range = { VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 };
            info.subresourceRange = image_range;
            for (uint32_t i = 0; i < window->image_count; i++)
            {
                VulkanFrame* frame_data = &window->frames[i];
                info.image = frame_data->back_buffer;
                VkCheck(vkCreateImageView(device, &info, allocator, &frame_data->back_buffer_view));
            }
        }

        // Create Framebuffer
        {
            VkImageView attachment[1];
            VkFramebufferCreateInfo info = {};
            info.sType              = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
            info.renderPass         = window->render_pass;
            info.attachmentCount    = 1;
            info.pAttachments       = attachment;
            info.width              = window->width;
            info.height             = window->height;
            info.layers             = 1;
            for (uint32_t i = 0; i < window->image_count; i++)
            {
                VulkanFrame* frame_data = &window->frames[i];
                attachment[0] = frame_data->back_buffer_view;
                VkCheck(vkCreateFramebuffer(device, &info, allocator, &frame_data->frame_buffer));
            }
        }
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static void VulkanDestroyAllViewportsRenderBuffers(VkDevice device, const VkAllocationCallbacks* allocator)
    {
        ImGuiPlatformIO& platform_io = ImGui::GetPlatformIO();
        for (int n = 0; n < platform_io.Viewports.Size; n++)
            if (VulkanViewportData* viewport_data = (VulkanViewportData*)platform_io.Viewports[n]->RendererUserData)
                VulkanDestroyWindowRenderBuffers(device, &viewport_data->window_render_buffers, allocator);
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static void VulkanDestroyDeviceObjects()
    {
        VulkanData* backend_data = VulkanGetBackendData();
        VulkanInitInfo* vulkan_init_info = &backend_data->vulkan_init_info;
        VulkanDestroyAllViewportsRenderBuffers(vulkan_init_info->device, vulkan_init_info->allocator);
        VulkanDestroyFontUploadObjects();

        if (backend_data->vertex_shader_module)
        {
            vkDestroyShaderModule(vulkan_init_info->device, backend_data->vertex_shader_module, vulkan_init_info->allocator);
            backend_data->vertex_shader_module = VK_NULL_HANDLE;
        }
        if (backend_data->fragment_shader_module)
        {
            vkDestroyShaderModule(vulkan_init_info->device, backend_data->fragment_shader_module, vulkan_init_info->allocator);
            backend_data->fragment_shader_module = VK_NULL_HANDLE;
        }
        if (backend_data->font_view)
        {
            vkDestroyImageView(vulkan_init_info->device, backend_data->font_view, vulkan_init_info->allocator);
            backend_data->font_view = VK_NULL_HANDLE;
        }
        if (backend_data->font_image)
        {
            vkDestroyImage(vulkan_init_info->device, backend_data->font_image, vulkan_init_info->allocator);
            backend_data->font_image = VK_NULL_HANDLE;
        }
        if (backend_data->font_memory)
        {
            vkFreeMemory(vulkan_init_info->device, backend_data->font_memory, vulkan_init_info->allocator);
            backend_data->font_memory = VK_NULL_HANDLE;
        }
        if (backend_data->font_sampler)
        {
            vkDestroySampler(vulkan_init_info->device, backend_data->font_sampler, vulkan_init_info->allocator);
            backend_data->font_sampler = VK_NULL_HANDLE;
        }
        if (backend_data->descriptor_set_layout)
        {
            vkDestroyDescriptorSetLayout(vulkan_init_info->device, backend_data->descriptor_set_layout, vulkan_init_info->allocator);
            backend_data->descriptor_set_layout = VK_NULL_HANDLE;
        }
        if (backend_data->pipeline_layout)
        {
            vkDestroyPipelineLayout(vulkan_init_info->device, backend_data->pipeline_layout, vulkan_init_info->allocator);
            backend_data->pipeline_layout = VK_NULL_HANDLE;
        }
        if (backend_data->pipeline)
        {
            vkDestroyPipeline(vulkan_init_info->device, backend_data->pipeline, vulkan_init_info->allocator);
            backend_data->pipeline = VK_NULL_HANDLE;
        }
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static void VulkanDestroyFrame(VkDevice device, VulkanFrame* frame_data, const VkAllocationCallbacks* allocator)
    {
        vkDestroyFence(device, frame_data->fence, allocator);
        vkFreeCommandBuffers(device, frame_data->command_pool, 1, &frame_data->command_buffer);
        vkDestroyCommandPool(device, frame_data->command_pool, allocator);
        frame_data->fence           = VK_NULL_HANDLE;
        frame_data->command_buffer  = VK_NULL_HANDLE;
        frame_data->command_pool    = VK_NULL_HANDLE;

        vkDestroyImageView(device, frame_data->back_buffer_view, allocator);
        vkDestroyFramebuffer(device, frame_data->frame_buffer, allocator);
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static void VulkanDestroyFrameRenderBuffers(VkDevice device, VulkanFrameRenderBuffers* frame_render_buffers, const VkAllocationCallbacks* allocator)
    {
        if (frame_render_buffers->vertex_buffer)
        {
            vkDestroyBuffer(device, frame_render_buffers->vertex_buffer, allocator);
            frame_render_buffers->vertex_buffer = VK_NULL_HANDLE;
        }
        if (frame_render_buffers->vertex_buffer_memory)
        {
            vkFreeMemory(device, frame_render_buffers->vertex_buffer_memory, allocator);
            frame_render_buffers->vertex_buffer_memory = VK_NULL_HANDLE;
        }
        if (frame_render_buffers->index_buffer)
        {
            vkDestroyBuffer(device, frame_render_buffers->index_buffer, allocator);
            frame_render_buffers->index_buffer = VK_NULL_HANDLE;
        }
        if (frame_render_buffers->index_buffer_memory)
        {
            vkFreeMemory(device, frame_render_buffers->index_buffer_memory, allocator);
            frame_render_buffers->index_buffer_memory = VK_NULL_HANDLE;
        }
        frame_render_buffers->vertex_buffer_size = 0;
        frame_render_buffers->index_buffer_size = 0;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static void VulkanDestroyFrameSemaphores(VkDevice device, VulkanFrameSemaphores* frame_render_semaphore, const VkAllocationCallbacks* allocator)
    {
        vkDestroySemaphore(device, frame_render_semaphore->image_acquired_semaphore, allocator);
        vkDestroySemaphore(device, frame_render_semaphore->render_completed_semaphore, allocator);
        frame_render_semaphore->image_acquired_semaphore = frame_render_semaphore->render_completed_semaphore = VK_NULL_HANDLE;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static void VulkanDestroyWindow(ImGuiViewport* viewport)
    {
        // The main viewport (owned by the application) will always have RendererUserData == NULL since we didn't create the data for it.
        VulkanData* backend_data = VulkanGetBackendData();
        if (VulkanViewportData* viewport_data = static_cast<VulkanViewportData*>(viewport->RendererUserData))
        {
            VulkanInitInfo* vulkan_init_info = &backend_data->vulkan_init_info;
            if (viewport_data->b_window_owned)
                VulkanDestroyWindow(vulkan_init_info->instance, vulkan_init_info->device, &viewport_data->window, vulkan_init_info->allocator);
            VulkanDestroyWindowRenderBuffers(vulkan_init_info->device, &viewport_data->window_render_buffers, vulkan_init_info->allocator);
            IM_DELETE(viewport_data);
        }
        viewport->RendererUserData = NULL;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static void VulkanDestroyWindow(VkInstance instance, VkDevice device, VulkanWindow* window, const VkAllocationCallbacks* allocator)
    {
        vkDeviceWaitIdle(device);

        for (uint32_t i = 0; i < window->image_count; i++)
        {
            VulkanDestroyFrame(device, &window->frames[i], allocator);
            VulkanDestroyFrameSemaphores(device, &window->frame_semaphores[i], allocator);
        }
        IM_FREE(window->frames);
        IM_FREE(window->frame_semaphores);
        window->frames              = NULL;
        window->frame_semaphores    = NULL;
        vkDestroyPipeline(device, window->pipeline, allocator);
        vkDestroyRenderPass(device, window->render_pass, allocator);
        vkDestroySwapchainKHR(device, window->swap_chain, allocator);
        vkDestroySurfaceKHR(instance, window->surface, allocator);

        *window = VulkanWindow();
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static void VulkanDestroyWindowRenderBuffers(VkDevice device, VulkanWindowRenderBuffers* window_render_buffers, const VkAllocationCallbacks* allocator)
    {
        for (uint32_t n = 0; n < window_render_buffers->count; n++)
            VulkanDestroyFrameRenderBuffers(device, &window_render_buffers->frame_render_buffers[n], allocator);
        IM_FREE(window_render_buffers->frame_render_buffers);
        window_render_buffers->frame_render_buffers = NULL;
        window_render_buffers->index = 0;
        window_render_buffers->count = 0;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static VulkanData* VulkanGetBackendData()
    {
        return ImGui::GetCurrentContext() ? static_cast<VulkanData*>(ImGui::GetIO().BackendRendererUserData) : NULL;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static void VulkanInitPlatformInterface()
    {
        ImGuiPlatformIO& platform_io = ImGui::GetPlatformIO();
        if (ImGui::GetIO().ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
            IM_ASSERT(platform_io.Platform_CreateVkSurface != NULL && "No VkSurface, please create");
        platform_io.Renderer_CreateWindow   = VulkanCreateWindow;
        platform_io.Renderer_DestroyWindow  = VulkanDestroyWindow;
        platform_io.Renderer_SetWindowSize  = VulkanSetWindowSize;
        platform_io.Renderer_RenderWindow   = VulkanRenderWindow;
        platform_io.Renderer_SwapBuffers    = VulkanSwapBuffers;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static uint32_t VulkanMemoryType(VkMemoryPropertyFlags property_flags, uint32_t type_bits)
    {
        VulkanData* backend_data = VulkanGetBackendData();
        VulkanInitInfo* vulkan_init_info = &backend_data->vulkan_init_info;
        VkPhysicalDeviceMemoryProperties prop;
        vkGetPhysicalDeviceMemoryProperties(vulkan_init_info->physical_device, &prop);
        for (uint32_t i = 0; i < prop.memoryTypeCount; i++)
            if ((prop.memoryTypes[i].propertyFlags & property_flags) == property_flags && type_bits & (1 << i))
                return i;
        return 0xFFFFFFFF; // Unable to find memoryType
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static void VulkanRenderWindow(ImGuiViewport* viewport, void*)
    {
        VulkanData* backend_data = VulkanGetBackendData();
        VulkanViewportData* viewport_data = static_cast<VulkanViewportData*>(viewport->RendererUserData);
        VulkanWindow* window = &viewport_data->window;
        VulkanInitInfo* vulkan_init_info = &backend_data->vulkan_init_info;
        VkResult err;

        VulkanFrame* frame_data = &window->frames[window->frame_index];
        VulkanFrameSemaphores* frame_semaphore_data = &window->frame_semaphores[window->semaphore_index];
        {
            {
                VkCheck(vkAcquireNextImageKHR(vulkan_init_info->device, window->swap_chain, UINT64_MAX, frame_semaphore_data->image_acquired_semaphore, VK_NULL_HANDLE, &window->frame_index));
                frame_data = &window->frames[window->frame_index];
            }

            for (;;)
            {
                VkResult err = vkWaitForFences(vulkan_init_info->device, 1, &frame_data->fence, VK_TRUE, 100);
                if (err == VK_SUCCESS) break;
                if (err == VK_TIMEOUT) continue;
                VkCheck(err);
            }

            {
                VkCheck(vkResetCommandPool(vulkan_init_info->device, frame_data->command_pool, 0));
                VkCommandBufferBeginInfo info = {};
                info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
                info.flags |= VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
                VkCheck(vkBeginCommandBuffer(frame_data->command_buffer, &info));
            }

            {
                ImVec4 clear_color = ImVec4(0.0f, 0.0f, 0.0f, 1.0f);
                memcpy(&window->clear_value.color.float32[0], &clear_color, 4 * sizeof(float));

                VkRenderPassBeginInfo info = {};
                info.sType                      = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
                info.renderPass                 = window->render_pass;
                info.framebuffer                = frame_data->frame_buffer;
                info.renderArea.extent.width    = window->width;
                info.renderArea.extent.height   = window->height;
                info.clearValueCount            = (viewport->Flags & ImGuiViewportFlags_NoRendererClear) ? 0 : 1;
                info.pClearValues               = (viewport->Flags & ImGuiViewportFlags_NoRendererClear) ? NULL : &window->clear_value;
                vkCmdBeginRenderPass(frame_data->command_buffer, &info, VK_SUBPASS_CONTENTS_INLINE);
            }
        }

        VulkanRenderDrawData(viewport->DrawData, frame_data->command_buffer, window->pipeline);
        {
            vkCmdEndRenderPass(frame_data->command_buffer);
            {
                VkPipelineStageFlags wait_stage = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
                VkSubmitInfo info = {};
                info.sType                  = VK_STRUCTURE_TYPE_SUBMIT_INFO;
                info.waitSemaphoreCount     = 1;
                info.pWaitSemaphores        = &frame_semaphore_data->image_acquired_semaphore;
                info.pWaitDstStageMask      = &wait_stage;
                info.commandBufferCount     = 1;
                info.pCommandBuffers        = &frame_data->command_buffer;
                info.signalSemaphoreCount   = 1;
                info.pSignalSemaphores      = &frame_semaphore_data->render_completed_semaphore;

                VkCheck(vkEndCommandBuffer(frame_data->command_buffer));
                VkCheck(vkResetFences(vulkan_init_info->device, 1, &frame_data->fence));
                VkCheck(vkQueueSubmit(vulkan_init_info->queue, 1, &info, frame_data->fence));
            }
        }
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static void VulkanSetWindowSize(ImGuiViewport* viewport, ImVec2 size)
    {
        VulkanData* backend_data = VulkanGetBackendData();
        VulkanViewportData* viewport_data = (VulkanViewportData*)viewport->RendererUserData;
        if (viewport_data == NULL) // This is NULL for the main viewport (which is left to the user/app to handle)
            return;
        VulkanInitInfo* vulkan_init_info = &backend_data->vulkan_init_info;
        viewport_data->window.b_clear_enable = (viewport->Flags & ImGuiViewportFlags_NoRendererClear) ? false : true;
        VulkanCreateOrResizeWindow(vulkan_init_info, &viewport_data->window, static_cast<int>(size.x), static_cast<int>(size.y));
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static void VulkanSetupRenderState(ImDrawData* draw_data, VkPipeline pipeline, VkCommandBuffer command_buffer, VulkanFrameRenderBuffers* frame_render_buffers, int viewport_width, int viewport_height)
    {
        VulkanData* backend_data = VulkanGetBackendData();

        {
            vkCmdBindPipeline(command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);
            VkDescriptorSet desc_set[1] = { backend_data->descriptor_set };
            vkCmdBindDescriptorSets(command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, backend_data->pipeline_layout, 0, 1, desc_set, 0, NULL);
        }

        // Bind Vertex And Index Buffer:
        if (draw_data->TotalVtxCount > 0)
        {
            VkBuffer vertex_buffers[1] = { frame_render_buffers->vertex_buffer };
            VkDeviceSize vertex_offset[1] = { 0 };
            vkCmdBindVertexBuffers(command_buffer, 0, 1, vertex_buffers, vertex_offset);
            vkCmdBindIndexBuffer(command_buffer, frame_render_buffers->index_buffer, 0, sizeof(ImDrawIdx) == 2 ? VK_INDEX_TYPE_UINT16 : VK_INDEX_TYPE_UINT32);
        }

        // Setup viewport:
        {
            VkViewport viewport;
            viewport.x          = 0;
            viewport.y          = 0;
            viewport.width      = static_cast<float>(viewport_width);
            viewport.height     = static_cast<float>(viewport_height);
            viewport.minDepth   = 0.0f;
            viewport.maxDepth   = 1.0f;
            vkCmdSetViewport(command_buffer, 0, 1, &viewport);
        }

        // Setup scale and translation:
        // Our visible imgui space lies from draw_data->DisplayPps (top left) to draw_data->DisplayPos+data_data->DisplaySize (bottom right). DisplayPos is (0,0) for single viewport apps.
        {
            float scale[2];
            scale[0] = 2.0f / draw_data->DisplaySize.x;
            scale[1] = 2.0f / draw_data->DisplaySize.y;
            float translate[2];
            translate[0] = -1.0f - draw_data->DisplayPos.x * scale[0];
            translate[1] = -1.0f - draw_data->DisplayPos.y * scale[1];
            vkCmdPushConstants(command_buffer, backend_data->pipeline_layout, VK_SHADER_STAGE_VERTEX_BIT, sizeof(float) * 0, sizeof(float) * 2, scale);
            vkCmdPushConstants(command_buffer, backend_data->pipeline_layout, VK_SHADER_STAGE_VERTEX_BIT, sizeof(float) * 2, sizeof(float) * 2, translate);
        }
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static void VulkanShutdownPlatformInterface()
    {
        ImGui::DestroyPlatformWindows();
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static void VulkanSwapBuffers(ImGuiViewport* viewport, void*)
    {
        VulkanData* backend_data = VulkanGetBackendData();
        VulkanViewportData* viewport_data = (VulkanViewportData*)viewport->RendererUserData;
        VulkanWindow* window = &viewport_data->window;
        VulkanInitInfo* vulkan_init_info = &backend_data->vulkan_init_info;

        uint32_t present_index = window->frame_index;

        VulkanFrameSemaphores* frame_semaphore_data = &window->frame_semaphores[window->semaphore_index];
        VkPresentInfoKHR info = {};
        info.sType              = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
        info.waitSemaphoreCount = 1;
        info.pWaitSemaphores    = &frame_semaphore_data->render_completed_semaphore;
        info.swapchainCount     = 1;
        info.pSwapchains        = &window->swap_chain;
        info.pImageIndices      = &present_index;
        VkResult err = vkQueuePresentKHR(vulkan_init_info->queue, &info);
        if (err == VK_ERROR_OUT_OF_DATE_KHR || err == VK_SUBOPTIMAL_KHR)
            VulkanCreateOrResizeWindow(vulkan_init_info, &viewport_data->window, static_cast<int>(viewport->Size.x), static_cast<int>(viewport->Size.y));
        else
            VkCheck(err);

        window->frame_index     = (window->frame_index + 1) % window->image_count;
        window->semaphore_index = (window->semaphore_index + 1) % window->image_count;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

}