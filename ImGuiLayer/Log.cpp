//
// Author: Kevin Ingles
// File: Log.cpp
// Description: Translation unit for Log.hpp

#include <Log.hpp>
#include <fstream>
#include <cstring>

namespace vizqgp::ui
{
    static std::vector<LogMessage>* Log{};

    LogMessage::LogMessage(const std::string& message_, LogMessageType type_)
    {
        message = new char[message_.length()+1];  // +1 to account of `\0` character
        strcpy(message, message_.data());
        type = type_;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    LogMessage::LogMessage(const char* message_, LogMessageType type_)
    {
        message = new char[strlen(message_) + 1]; // +1 to account for `\0` character
        strcpy(message, message_);
        type = type_;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void CreateLog()
    {
        Log = new std::vector<LogMessage>();
        LogLog("Initializing log...please check log.log for errors.\n");
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

   [[nodiscard]] std::vector<LogMessage>& GetLog()
    {
        return *Log;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void DestroyLog()
    {
        PrintLog("log.log");
        delete Log;
    }
    
     // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void LogDebug(const std::string& message)
    {
        Log->push_back(std::move(LogMessage(message, LogMessageType::Debug)));
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void LogError(const std::string& message)
    {
        Log->push_back(std::move(LogMessage(message, LogMessageType::Error)));
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void LogLog(const std::string& message)
    {
        Log->push_back(std::move(LogMessage(message, LogMessageType::Log)));
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void LogSuccess(const std::string& message)
    {
        Log->push_back(std::move(LogMessage(message, LogMessageType::Success)));
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void LogWarning(const std::string& message)
    {
        Log->push_back(std::move(LogMessage(message, LogMessageType::Warning)));
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void LogDebug(const char* message)
    {
        Log->push_back(std::move(LogMessage(message, LogMessageType::Debug)));
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void LogError(const char* message)
    {
        Log->push_back(std::move(LogMessage(message, LogMessageType::Error)));
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void LogLog(const char* message)
    {
        Log->push_back(std::move(LogMessage(message, LogMessageType::Log)));
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void LogSuccess(const char* message)
    {
        Log->push_back(std::move(LogMessage(message, LogMessageType::Success)));
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void LogWarning(const char* message)
    {
        Log->push_back(std::move(LogMessage(message, LogMessageType::Warning)));
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void PrintLog(const char* file_name)
    {
        std::fstream fout(file_name, std::fstream::out);
        for (auto const& entry : *Log)
            fout << entry.message;
        fout.close();
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....
}