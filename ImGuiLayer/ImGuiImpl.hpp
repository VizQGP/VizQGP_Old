//
// Author: Kevin Ingles
// File: ImGuiImpl.hpp
// Description: ImGui backend, since the stalk one has vulkan validation errors whenever a docking window is moved out
//              Most of code is copied from the backend itself, but some modification form #PR???? have been integrated
//              Proper credit is given where it is deserved.

#ifndef VIZQGG_IMGUI_IMPL_HPP
#define VIZQGP_IMGUI_IMPL_HPP

#include <imgui.h>
#include <SDL.h>
#include <vulkan/vulkan.h>

// forward declaration for SDL

namespace vizqgp::ui
{
    // ImGui SDL backend
    bool SDLInit(SDL_Window* window);
    void SDLNewFrame();
    bool SDLProcessEvent(const SDL_Event* event);
    void SDLShutdown();

    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////

    // ImGui Vulkan backend structs
    struct VulkanInitInfo;
    struct VulkanFrame;
    struct VulkanFrameRenderBuffers;
    struct VulkanFrameSemaphores;
    struct VulkanWindow;
    struct VulkanWindowRenderBuffers;

    bool                VulkanCreateFontsTexture(VkCommandBuffer command_buffer);
    void                VulkanCreateOrResizeWindow(VulkanInitInfo* info, VulkanWindow* window, int width, int height);
    void                VulkanDestroyFontUploadObjects();
    void                VulkanDestroyWindow(VulkanInitInfo* info, VulkanWindow* window);
    int                 VulkanGetMinImageCountFromPresentMode(VkPresentModeKHR present_mode);
    bool                VulkanInit(VulkanInitInfo& info, VkRenderPass& render_pass);
    void                VulkanNewFrame();
    void                VulkanRenderDrawData(ImDrawData* draw_data, VkCommandBuffer command_buffer, VkPipeline pipeline = VK_NULL_HANDLE);
    VkPresentModeKHR    VulkanSelectPresentMode(VkPhysicalDevice physical_device, VkSurfaceKHR surface, const VkPresentModeKHR* request_modes, int request_modes_counts);
    VkSurfaceFormatKHR  VulkanSelectSurfaceFormat(VkPhysicalDevice physical_device, VkSurfaceKHR surface, const VkFormat* request_formats, int request_formats_count, VkColorSpaceKHR request_color_space);
    void                VulkanSetMinImageCount(uint32_t min_image_count);
    void                VulkanShutdown();



    struct VulkanInitInfo
    {
        VkInstance                      instance;
        VkPhysicalDevice                physical_device;
        VkDevice                        device;
        uint32_t                        queue_family;
        VkQueue                         queue;
        VkPipelineCache                 pipeline_cache;
        VkDescriptorPool                descriptor_pool;
        uint32_t                        subpass;
        uint32_t                        min_image_count;
        uint32_t                        image_count;
        VkSampleCountFlagBits           MSAA_samples;
        const VkAllocationCallbacks*    allocator{ nullptr };
        void                            (*VkCheckResults)(VkResult err);
    };

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    struct VulkanFrame
    {
        VkCommandPool   command_pool;
        VkCommandBuffer command_buffer;
        VkFence         fence;
        VkImage         back_buffer;
        VkImageView     back_buffer_view;
        VkFramebuffer   frame_buffer;
    };

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    struct VulkanFrameRenderBuffers
    {
        VkDeviceMemory  vertex_buffer_memory;
        VkDeviceMemory  index_buffer_memory;
        VkDeviceSize    vertex_buffer_size;
        VkDeviceSize    index_buffer_size;
        VkBuffer        vertex_buffer;
        VkBuffer        index_buffer;
    };

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    struct VulkanFrameSemaphores
    {
        VkSemaphore image_acquired_semaphore;
        VkSemaphore render_completed_semaphore;
    };

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    struct VulkanWindow
    {
        int                     width;
        int                     height;
        VkSwapchainKHR          swap_chain;
        VkSurfaceKHR            surface;
        VkSurfaceFormatKHR      surface_format;
        VkPresentModeKHR        present_mode;
        VkRenderPass            render_pass;
        VkPipeline              pipeline;
        bool                    b_clear_enable;
        VkClearValue            clear_value;
        uint32_t                frame_index;
        uint32_t                image_count;
        uint32_t                semaphore_index;
        VulkanFrame* frames;
        VulkanFrameSemaphores* frame_semaphores;

        VulkanWindow()
        {
            memset(this, 0, sizeof(*this));
            present_mode = VK_PRESENT_MODE_MAX_ENUM_KHR;
            b_clear_enable = true;
        }
    };

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    struct VulkanWindowRenderBuffers
    {
        uint32_t                    index;
        uint32_t                    count;
        VulkanFrameRenderBuffers* frame_render_buffers;
    };

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....
}

#endif