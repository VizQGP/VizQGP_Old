//
// Author: Kevin Ingles
// File: ImGui.cpp
// Description: Implementation of ImGuiLayer.hpp

#include "ImGuiLayer.hpp"

#include <chrono>
#include <fmt/format.h>
#include <fmt/ostream.h>
#include <Log.hpp>
#include <Print.hpp>
#include <ProjectPaths.h>
#include <thread>
#include <vector>
#include <vulkan/vulkan.h>

static void VkCheck(VkResult err)
{
if (err)
    LogError(fmt::format("ImGuiLayer detected Vulkan Error: {}", err));
}

namespace vizqgp::ui
{
    ImGuiLayer::ImGuiLayer(VulkanInitInfo& vulkan_init_info, Window& window, VkRenderPass& render_pass)
    {
        m_vulkan_init_info = vulkan_init_info;

        std::vector<VkDescriptorPoolSize> pool_sizes =
        {
            { VK_DESCRIPTOR_TYPE_SAMPLER, 1000 },
            { VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1000 },
            { VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, 1000 },
            { VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 1000 },
            { VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER, 1000 },
            { VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER, 1000 },
            { VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1000 },
            { VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1000 },
            { VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, 1000 },
            { VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC, 1000 },
            { VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT, 1000 }
        };

        VkDescriptorPoolCreateInfo pool_info{};
        pool_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
        pool_info.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
        pool_info.maxSets = 1000;
        pool_info.poolSizeCount = pool_sizes.size();
        pool_info.pPoolSizes = pool_sizes.data();

        VkDescriptorPool imgui_pool;
        VkCheck(vkCreateDescriptorPool(m_vulkan_init_info.device, &pool_info, nullptr, &imgui_pool));
        m_vulkan_init_info.descriptor_pool = imgui_pool;

        // Remove window boarders and frame borders
        // Rounded windows
        ImGui::CreateContext();
        
        ImGui::GetIO().Fonts->AddFontFromFileTTF((fonts_dir / "caskaydia_cove_nerd_mono.ttf").string().data(), 15.0f);
        ImGui::GetIO().ConfigFlags |= ImGuiConfigFlags_::ImGuiConfigFlags_DockingEnable;
        // FIXME: Enabling multiple viewports essentially allows ImGui to create windows outside the main surface which leads to validation errors from Vulkan
        //        Need to find a way to enable this and fix the validation errors.
        // ImGui::GetIO().ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;                     // TO enable multiple viewports, in particular so we can see the clear colors

        ImGui::GetStyle().WindowRounding = 7.0f;
        ImGui::GetStyle().WindowBorderSize = 0.0f;
        ImGui::GetStyle().Colors[ImGuiCol_TitleBgActive] = ImGui::GetStyle().Colors[ImGuiCol_TitleBg];
        ImGui::SetNextWindowBgAlpha(1.0);

        ImGui::GetMainViewport()->Flags |= ImGuiViewportFlags_NoFocusOnAppearing;
        ImGui::GetMainViewport()->Flags |= ImGuiViewportFlags_NoFocusOnClick;

        IM_ASSERT(SDLInit(window.window) && "Failed to initialize SDL backend");
        IM_ASSERT(VulkanInit(m_vulkan_init_info, render_pass) && "Faied to initialize Vulkan backend");
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    ImGuiLayer::~ImGuiLayer()
    {
        Print("Destroying ImGuiLayer");
        vkDestroyDescriptorPool(m_vulkan_init_info.device, m_vulkan_init_info.descriptor_pool, nullptr);
        VulkanShutdown();
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void ImGuiLayer::BeginFrame()
    {
        VulkanNewFrame();
        SDLNewFrame();
        ImGui::NewFrame();
        
        // ImGui::ShowDemoWindow();
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void ImGuiLayer::DestroyFontTexturesObjects()
    {
        VulkanDestroyFontUploadObjects();
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static float log_window_height_in_percent = 0.2f; // TODO: Make log window resizable
    std::vector<VkExtent2D> ImGuiLayer::GetImGuiViewportRectangles()
    {
        std::vector<VkExtent2D> viewport_sizes{};
        viewport_sizes.push_back(VkExtent2D(ImGui::GetIO().DisplaySize.x, log_window_height_in_percent * ImGui::GetIO().DisplaySize.y));

        return viewport_sizes;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    static size_t number_log_entries = 0;
    void ImGuiLayer::DrawLoggingConsole()
    {
        // Add flags:
        //  Set item spacing
        ImVec2 current_position = ImGui::GetMainViewport()->Pos;
        ImVec2 current_window_size = ImGui::GetIO().DisplaySize;

        ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, { 5.0f, 1.0f });
        
        ImVec2 new_window_position = { current_position.x, current_position.y + (1.0f - log_window_height_in_percent) * current_window_size.y };
        ImVec2 new_window_size = { current_window_size.x, log_window_height_in_percent * current_window_size.y };
       
        ImGui::SetNextWindowPos(new_window_position);
        ImGui::SetNextWindowBgAlpha(1.0);
        ImGui::SetNextWindowSize(new_window_size);

        ImGuiWindowFlags imgui_window_flags = ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoCollapse;
        imgui_window_flags |= ImGuiWindowFlags_HorizontalScrollbar | ImGuiWindowFlags_NoFocusOnAppearing;
        ImGui::Begin("VizQGP: Output", static_cast<bool *>(0), imgui_window_flags);

        // TODO: Have the Log.hpp/cpp take care of the the text coloration 
        for (const auto& entry : GetLog())
        {
            switch (entry.type)
            {
                case LogMessageType::Log:
                {
                    ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.753, 0.753, 0.753, 1.0));
                    ImGui::Text(log_prefix);
                    ImGui::SameLine();
                    ImGui::Text(entry.Message());
                    ImGui::PopStyleColor();
                    break;
                }
                case LogMessageType::Debug:
                {
                    ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(1.0, 1.0, 0.0, 1.0));
                    ImGui::Text(debug_prefix);
                    ImGui::SameLine();
                    ImGui::Text(entry.Message());
                    ImGui::PopStyleColor();
                    break;
                }
                case LogMessageType::Error:
                {
                    ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(1.0, 0.0, 0.0, 1.0));
                    ImGui::Text(error_prefix);
                    ImGui::SameLine();
                    ImGui::Text(entry.Message());
                    ImGui::PopStyleColor();
                    break;
                }
                case LogMessageType::Warning:
                {
                    ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(1.0, 0.509, 0.0, 1.0));
                    ImGui::Text(warning_prefix);
                    ImGui::SameLine();
                    ImGui::Text(entry.Message());
                    ImGui::PopStyleColor();
                    break;
                }
                case LogMessageType::Success:
                {
                    ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.0, 1.0, 0.0, 1.0));
                    ImGui::Text(success_prefix);
                    ImGui::SameLine();
                    ImGui::Text(entry.Message());
                    ImGui::PopStyleColor();
                    break;
                }
                default:
                {
                    ImGui::Text("Log message needs qualifier:");
                    ImGui::SameLine();
                    ImGui::Text(entry.Message());
                    break;
                }
            }
        }
        if (number_log_entries != GetLog().size())
        {
            ImGui::SetScrollHereY(0.999f);
            number_log_entries = GetLog().size();
        }
        ImGui::End();
         ImGui::PopStyleVar();
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void ImGuiLayer::EndFrame()
    {
        ImGui::EndFrame();
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void ImGuiLayer::ProcessEvents(const SDL_Event* event)
    {
        SDLProcessEvent(event);
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void ImGuiLayer::RenderFrame(VkCommandBuffer command_buffer)
    {
        ImGui::Render();
        if (ImGui::GetIO().ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
        {
            ImGui::UpdatePlatformWindows();
            ImGui::RenderPlatformWindowsDefault();
        }
        VulkanRenderDrawData(ImGui::GetDrawData(), command_buffer);
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void ImGuiLayer::SetImGuiIOConfigFlags(ImGuiConfigFlags flags)
    {
        ImGui::GetIO().ConfigFlags |= flags;
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....

    void ImGuiLayer::UploadFontTextures(VkCommandBuffer command_buffer)
    {
        VulkanCreateFontsTexture(command_buffer);
    }

    // .....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....oooOO0OOooo.....
}