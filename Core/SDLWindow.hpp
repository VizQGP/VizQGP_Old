//
// Author: Kevin Ingles
// File: SDLWindow.hpp
// Description: SDL window wrapper for rendering engine

#ifndef VIZQGP_WINDOW_HPP
#define VIZQGP_WINDOW_HPP

//#define SDL_MAIN_HANDLED
#include <SDL.h>
#undef main
#include <SDL_vulkan.h>

namespace vizqgp
{
    // TODO: add support for multiple windows
    struct Window
    {
        Window() = default;
        Window(const char* window_name, uint32_t width, uint32_t height, SDL_WindowFlags flags);
        ~Window();

        void ProcessEvent(SDL_Event* event);


        uint32_t width{ 1600 };
        uint32_t height{ 900 };

        SDL_Window* window{ nullptr };
        uint32_t    window_id{ 0 };
    };
}

#endif