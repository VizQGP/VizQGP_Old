//
// Author: Kevin Ingles
// File: SDLWindow.cpp
// Description: Implementation for SDL window wrapper

#include "SDLWindow.hpp"


namespace vizqgp
{
    Window::Window(const char* window_name, uint32_t width_, uint32_t height_, SDL_WindowFlags flags)
    {
        width  = width_;
        height = height_;
        SDL_Init(SDL_INIT_VIDEO);
        window = SDL_CreateWindow(window_name, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, flags);
    }
    Window::~Window()
    {
        SDL_DestroyWindow(window);
    }
    void Window::ProcessEvent(SDL_Event* event)
    {
        switch (event->window.event)
        {
        //  case SDL_WINDOWEVENT_RESIZED:
        //      break;
        //  case SDL_WINDOWEVENT_SIZE_CHANGED:
        //      OnWindowResize(sdl_event.window.data1, sdl_event.window.data2); break;
        default:
            break;
        } // End switch (sdl_event.window.event)
    }
}