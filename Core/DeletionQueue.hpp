
// Author: Kevin Ingles
// File: DeletionQueue.hpp
// Description: Data structure that stores functions used to call destructors or garabage collectors in a specific order.

#ifndef DELETION_QUEUE_HPP
#define DELETION_QUEUE_HPP

#include <functional>
#include <deque>

struct DeletionQueue
{
    std::deque<std::function<void()>> deletors;

    void PushFunction(std::function<void()>&& func)
    {
        deletors.push_back(func);
    }

    void Flush()
    {
        for (auto it = deletors.rbegin(); it != deletors.rend(); it++)
            (*it)();
        
        deletors.clear();
    }
};

#endif