//
// Author: Kevin Ingles
// File: Units.hpp
// Description: The scales that are relevant in physical processes can vary. We want to be able to render the correct length scale for our desired process
//              This API provides the plumbing to ensure that the steps the camera takes, etc are all commensurate with the length scale in question

// Camera class should not be modified. Rather, the scale the best describes the system should be set and this becomes the default unit for the simulation DEFAULT_UNIT, and 
// a step forward for the camera will be in terms of DEFAULT_UNITs

#ifndef VIZQGP_UNITS_HPP
#define VIZQGP_UNITS_HPP

#include <Log.hpp>

// TODO: Construct composite units
// TODO: Add conversation routines for natural units between mass and space

namespace vizqgp::detail
{
    enum class UnitsSystem { SI, Natural };
    enum class SIUnits { meters, seconds, kilograms, Coloumb, Gauss };
    enum class NaturalUnits { eV };
    struct UnitPrefix
    {
        static double yotta = 1e24;
        static double zetta = 1e21;
        static double exa   = 1e18;
        static double peta  = 1e15;
        static double tera  = 1e12;
        static double giga  = 1e9;
        static double mega  = 1e6;
        static double kilo  = 1e3;
        static double base  = 1;
        static double milli = 1e-3;
        static double micro = 1e-6;
        static double nano  = 1e-9;
        static double pico  = 1e-12;
        static double femto = 1e-15;
        static double atto  = 1e-18;
        static double zepto = 1e-21;
        static double yocto = 1e-24;
    };

    class CurrentUnitSystem
    {
    public:
        CurrentUnitSystem() = delete;
        CurrentUnitSystem(UnitsSystem units_system) : m_units_system{ units_system } {}

        void SetScales(double meters_scale, double seconds_scale, double kilograms_scale, double Coloumb_scale = 0.1 * UnitPrefix::atto, double Gauss_scale = 1.0)
        {
            m_default_scale_meter       = meters_scale;
            m_default_scale_second      = seconds_scale;
            m_default_scale_kilogram    = kilograms_scale;
            m_default_scale_Coloumb     = Coloumb_scale;
            m_default_scale_Gauss       = Gauss_scale;
        }

        void SetScale(SIUnits unit, double scale)
        {
            switch (unit)
            {
            case SIUnits::meters:
                m_default_scale_meter = scale;
                return;
            case SIUnits::seconds:
                m_default_scale_second = scale;
                return;
            case SIUnits::kilograms:
                m_default_scale_kilogram = scale;
                return;
            case SIUnits::Coloumb:
                m_default_scale_Coloumb = scale;
                return;
            case SIUnits::Gauss:
                m_default_scale_Gauss = scale;
                return;
            default:
                LogWarning("Tried to call undefined SI unit");
                return;
            }
        }

        // This should be a prefix or multiple of a prefix from the UnitPrefix struct
        void SetScale(double natural_units_scale)
        {
            m_default_natural_units_scale = prefix;

            m_default_scale_meter       = 0.0;
            m_default_scale_second      = 0.0;
            m_default_scale_kilogram    = 0.0;
            m_default_scale_Coloumb     = 0.0;
            m_default_scale_Gauss       = 0.0;
        }

        constexpr bool CheckUnits()
        {
            switch (m_units_system)
            {
                case UnitsSystem::Natural:
                {
                    if (m_default_natural_units_scale <= 0)
                        return false;
                    else return true;
                }
                case UnitsSystem::SI:
                {
                    if (    m_default_scale_meter       <= 0
                        ||  m_default_scale_second      <= 0
                        ||  m_default_scale_kilogram    <= 0
                        ||  m_default_scale_Coloumb     <= 0
                        ||  m_default_scale_Gauss       <= 0)
                    {
                        return false;
                    }
                    else return true;
                }
            }
        }
    private:
        UnitsSystem m_units_system;

        double m_default_scale_meter;
        double m_default_scale_second;
        double m_default_scale_kilogram;
        double m_default_scale_Coloumb;
        double m_default_scale_Gauss;

        double m_default_natural_units_scale = 0;
    };

}

#endif 