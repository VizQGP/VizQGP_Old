//
// Author: Kevin Ingles
// File: Log.hpp
// Description: Provides basic functionality to output information to any output stream
//              Should be the file to include if iostream is desired

#ifndef PRINT_HPP
#define PRINT_HPP

#include <fmt/core.h>
#include <fmt/format.h>
#include <iostream>
#include <type_traits>

// Specialization for above when OStream == std::ostream's cout
template<class...Args>
void Print(Args&&...args) noexcept
{
    ((std::cout << std::forward<Args>(args) << " "), ...);
    std::cout << '\n';
}

template<typename OStream, class...Args>
OStream& PrintT(OStream& out, Args&&...args) noexcept
{
    ((out << std::forward<Args>(args) << " "), ...);
    out << '\n';
    return out;
}

#endif