#
# Author: Kevin Ingles
# File: ConfigProjectPaths.cmake
# Description: Configure the file ProjectPaths.h.in to return the relevant paths for the project. These are absolute paths.

set(THIS_PROJECT_DIR_STR "${PROJECT_SOURCE_DIR}")
set(THIS_SHADER_DIR_STR "${PROJECT_SOURCE_DIR}/Shaders")
set(THIS_FONT_FAMILY_DIR_STR "${PROJECT_SOURCE_DIR}/Utility/Fonts")
set(THIS_DEFAULT_MESH_DIR_STR "${PROJECT_SOURCE_DIR}/Utility/DefaultAssests")

configure_file(
	"${PROJECT_SOURCE_DIR}/cmake/ProjectPaths.h.in" 
	"${PROJECT_SOURCE_DIR}/Core/ProjectPaths.h" 
	@ONLY
)