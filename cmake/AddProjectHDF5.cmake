#
# Author: Kevin Ingles
# File: AddProjectDHF5.cmake
# Description: This files includes HDF5 into our local project without installing it into the system path


# Add HDF5 dependence through ExternalProject_Add
# Taken from https://github.com/bast/hdf5-cmake-example/blob/master/CMakeLists.txt
# Required by HDF5 top cmake file
set(HDF5_EXTERNALLY_CONFIGURED 1)
set(HDF5_EXTERNAL_LIB_PREFIX "VizQGP")

# To help configre, compile and link
include(ExternalProject)
set(
    ExternalProjectCMakeArgs 
        "-DHDF5_BUILD_CPP_LIB=ON"
        "-DHDF5_GENERATE_HEADERS=ON"
)

ExternalProject_Add(
    HDF5_Local
    PREFIX "HDF5"
    GIT_REPOSITORY "https://github.com/HDFGroup/hdf5.git"
    GIT_TAG "origin/hdf5_1_12"
    CMAKE_ARGS ${ExternalProjectCMakeArgs}
    SOURCE_DIR "HDF5/HDF5_Local"
    STAMP_DIR  "HDF5/HDF5_Stamp"
    BINARY_DIR "HDF5/HDF5_Build"
    BUILD_ALWAYS ON
    INSTALL_COMMAND ""
)

include_directories(
    "${PROJECT_BINARY_DIR}/HDF5/HDF5_Local/src"
    "${PROJECT_BINARY_DIR}/HDF5/HDF5_Local/c++/src"
    "${PROJECT_BINARY_DIR}/HDF5/HDF5_Build/src"
)

# Looking in the bin directory these were the closests that I found
set(
    _HDF5_LIBS
    "${PROJECT_BINARY_DIR}/HDF5/HDF5_Build/bin/libhdf5_cpp_D.lib"
    "${PROJECT_BINARY_DIR}/HDF5/HDF5_Build/bin/libhdf5_hl_cpp_D.lib"
    "${PROJECT_BINARY_DIR}/HDF5/HDF5_Build/bin/libhdf5_D.lib"
    "${PROJECT_BINARY_DIR}/HDF5/HDF5_Build/bin/libhdf5_hl_D.lib"
)