#
# Author: Kevin Ingles
# File: CompileGLSLShaders.cmake
# Description: Compiles GLSL shaders into SPIR-V format for Vulkan engine

# 
# Credit: https://github.com/vblanco20-1/vulkan-guide/blob/starting-point/CMakeLists.txt
#

find_program(GLSL_VALIDATOR glslangValidator HINTS /usr/bin /usr/local/bin $ENV{VULKAN_SDK}/Bin/ $ENV{VULKAN_SDK}/Bin32/)

## find all the shader files under the shaders folder
file(GLOB_RECURSE GLSL_SOURCE_FILES
    "${PROJECT_SOURCE_DIR}/Shaders/*.frag"
    "${PROJECT_SOURCE_DIR}/Shaders/*.vert"
    "${PROJECT_SOURCE_DIR}/Shaders/*.comp"
    )
	
message(STATUS "GLSL Source Files ${GLSL_SOURCE_FILES}")
# file(GLOB_RECURSE GLSL_SOURCE_FILES
    # "${PROJECT_SOURCE_DIR}/*.frag"
    # "${PROJECT_SOURCE_DIR}/*.vert"
    # "${PROJECT_SOURCE_DIR}/*.comp"
    # )

## iterate each shader
foreach(GLSL ${GLSL_SOURCE_FILES})
  get_filename_component(FILE_NAME ${GLSL} NAME)
  set(SPIRV "${PROJECT_SOURCE_DIR}/Shaders/${FILE_NAME}.spv")
  message(STATUS ${GLSL})
  message(STATUS ${SPIRV})
  add_custom_command(
    OUTPUT ${SPIRV}
    COMMAND ${GLSL_VALIDATOR} -V ${GLSL} -o ${SPIRV}
    # COMMAND ECHO "HELLO"
    DEPENDS ${GLSL})
  list(APPEND SPIRV_BINARY_FILES ${SPIRV})
endforeach(GLSL)

add_custom_target(
    ShadersBin 
    DEPENDS ${SPIRV_BINARY_FILES}
    )