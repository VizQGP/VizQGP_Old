﻿// 
// Author: Kevin Ingles
// File: EntryPoint.cpp
// Description: Defines the entry point for the application.

#include <Print.hpp>
#include <Log.hpp>
#include <RenderEngine.hpp>
#include <type_traits>

template<typename T>
void PrintType(const T& v)
{
	Print(typeid(v).name());
}

int main()
{
	Print("Hello CMake!");

	vizqgp::ui::CreateLog();
	LogDebug("Still cannot append destruction\n");
	LogWarning("Using fmt::format has unpredictable behavior because `Log` does not own const char*");
	vizqgp::RenderEngine engine;
	engine.Run();
	vizqgp::ui::DestroyLog();
	return 0;
}